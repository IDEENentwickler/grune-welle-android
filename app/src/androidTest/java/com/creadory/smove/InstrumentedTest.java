package com.creadory.smove;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.creadory.smove.core.trafficlights.Phase;
import com.creadory.smove.core.trafficlights.TimeRange;
import com.creadory.smove.core.trafficlights.TrafficLightController;
import com.creadory.smove.core.trafficlights.TrafficLightSignalState;
import com.creadory.smove.data.local.DatabaseHelper;
import com.creadory.smove.util.ParserUtil;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class InstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.creadory.smove", appContext.getPackageName());
    }

    @Test
    public void calcTrafficLightSignalState() throws Exception {
        TrafficLightController trafficLightController = new TrafficLightController(new DatabaseHelper(InstrumentationRegistry.getTargetContext()));
        trafficLightController.initTrafficLights();

        TrafficLightSignalState state = trafficLightController.getTrafficLightSignalState(
                ParserUtil.parseDate("10:58:45_01/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                trafficLightController.getTrafficLightSignal(23290, 23278));
        assertEquals(state.getState(), Phase.State.NOT_GREEN);
        assertEquals(state.getTimeLeft(), 23);
        assertEquals(state.getPhaseStartTime(), 28);
        assertEquals(state.getPhaseEndTime(), 75);
        assertEquals(state.getSecondInInterval(), 5);

        state = trafficLightController.getTrafficLightSignalState(
                ParserUtil.parseDate("10:59:08_01/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                trafficLightController.getTrafficLightSignal(23290, 23278));
        assertEquals(state.getState(), Phase.State.GREEN);
        assertEquals(state.getTimeLeft(), 47);
        assertEquals(state.getPhaseStartTime(), 28);
        assertEquals(state.getPhaseEndTime(), 75);
        assertEquals(state.getSecondInInterval(), 28);

        state = trafficLightController.getTrafficLightSignalState(
                ParserUtil.parseDate("10:59:15_01/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                trafficLightController.getTrafficLightSignal(28548, 3944));
        assertEquals(state.getState(), Phase.State.NOT_GREEN);
        assertEquals(state.getTimeLeft(), 19);
        assertEquals(state.getPhaseStartTime(), 34);
        assertEquals(state.getPhaseEndTime(), 50);
        assertEquals(state.getSecondInInterval(), 15);

        state = trafficLightController.getTrafficLightSignalState(
                ParserUtil.parseDate("18:40:29_01/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                trafficLightController.getTrafficLightSignal(28548, 3944));
        assertEquals(state.getState(), Phase.State.NOT_GREEN);
        assertEquals(state.getTimeLeft(), 42);
        assertEquals(state.getPhaseStartTime(), 41);
        assertEquals(state.getPhaseEndTime(), 60);
        assertEquals(state.getSecondInInterval(), 69);

        state = trafficLightController.getTrafficLightSignalState(
                ParserUtil.parseDate("18:40:39_01/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                trafficLightController.getTrafficLightSignal(28546, 3944));
        assertEquals(state.getState(), Phase.State.GREEN);
        assertEquals(state.getTimeLeft(), 22);
        assertEquals(state.getPhaseStartTime(), 0);
        assertEquals(state.getPhaseEndTime(), 31);
        assertEquals(state.getSecondInInterval(), 9);

        //OFF STATE
        state = trafficLightController.getTrafficLightSignalState(
                ParserUtil.parseDate("04:40:40_03/11/2017", "HH:mm:ss_MM/dd/yyyy"),
                trafficLightController.getTrafficLightSignal(33888, 33889));
        assertEquals(state.getState(), Phase.State.OFF);
        assertEquals(state.getTimeLeft(), 4760);

        state = trafficLightController.getTrafficLightSignalState(
                ParserUtil.parseDate("23:30:40_03/11/2017", "HH:mm:ss_MM/dd/yyyy"),
                trafficLightController.getTrafficLightSignal(33888, 33889));
        assertEquals(state.getState(), Phase.State.OFF);
        assertEquals(state.getTimeLeft(), 23360);

        state = trafficLightController.getTrafficLightSignalState(
                ParserUtil.parseDate("23:30:40_03/12/2017", "HH:mm:ss_MM/dd/yyyy"),
                trafficLightController.getTrafficLightSignal(33888, 33889));
        assertEquals(state.getState(), Phase.State.OFF);
        assertEquals(state.getTimeLeft(), 1760);
    }

    @Test
    public void calcTimeRangesForGreen() throws Exception {
        TrafficLightController trafficLightController = new TrafficLightController(new DatabaseHelper(InstrumentationRegistry.getTargetContext()));
        trafficLightController.initTrafficLights();

        //06112, Signalplan 4, K1K2K3, 0-...-7-g-60-...-90
        ArrayList<TimeRange> timeRanges = trafficLightController.getTimeRangesForGreen(    //0 in -> 0-...-7
                trafficLightController.getTrafficLightSignal(23290, 23278),
                ParserUtil.parseDate("14:00:00_03/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                0, 2);
        assertEquals(timeRanges.get(0).start, 7);
        assertEquals(timeRanges.get(0).end, 60);
        assertEquals(timeRanges.get(1).start, 97);
        assertEquals(timeRanges.get(1).end, 150);

        timeRanges = trafficLightController.getTimeRangesForGreen(                         //70 in -> 60-...-90
                trafficLightController.getTrafficLightSignal(23290, 23278),
                ParserUtil.parseDate("14:01:10_03/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                0, 2);
        assertEquals(timeRanges.get(0).start, 27);
        assertEquals(timeRanges.get(0).end, 80);
        assertEquals(timeRanges.get(1).start, 117);
        assertEquals(timeRanges.get(1).end, 170);

        timeRanges = trafficLightController.getTimeRangesForGreen(                         //40 in -> 7-g-60
                trafficLightController.getTrafficLightSignal(23290, 23278),
                ParserUtil.parseDate("14:00:40_03/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                0, 2);
        assertEquals(timeRanges.get(0).start, 0);
        assertEquals(timeRanges.get(0).end, 20);
        assertEquals(timeRanges.get(1).start, 57);
        assertEquals(timeRanges.get(1).end, 110);

        timeRanges = trafficLightController.getTimeRangesForGreen(                         //40 in -> 7-g-60 and timeToTls = 40
                trafficLightController.getTrafficLightSignal(23290, 23278),
                ParserUtil.parseDate("14:00:00_03/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                40, 2);
        assertEquals(timeRanges.get(0).start, 7);
        assertEquals(timeRanges.get(0).end, 60);
        assertEquals(timeRanges.get(1).start, 97);
        assertEquals(timeRanges.get(1).end, 150);

        timeRanges = trafficLightController.getTimeRangesForGreen(                         //40 in -> 7-g-60 and timeToTls = 30
                trafficLightController.getTrafficLightSignal(23290, 23278),
                ParserUtil.parseDate("14:00:10_03/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                30, 2);
        assertEquals(timeRanges.get(0).start, 0);
        assertEquals(timeRanges.get(0).end, 50);
        assertEquals(timeRanges.get(1).start, 87);
        assertEquals(timeRanges.get(1).end, 140);

        //06112, Signalplan 4, K4,K5,K6, 0-g-17-...-50-g-90
        timeRanges = trafficLightController.getTimeRangesForGreen(    //5 in -> 0-g-17
                trafficLightController.getTrafficLightSignal(201, 23279),
                ParserUtil.parseDate("14:00:05_03/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                0, 2);
        assertEquals(timeRanges.get(0).start, 0);
        assertEquals(timeRanges.get(0).end, 12);
        assertEquals(timeRanges.get(1).start, 45);
        assertEquals(timeRanges.get(1).end, 102);

        timeRanges = trafficLightController.getTimeRangesForGreen(    //30 in -> 17-...-50
                trafficLightController.getTrafficLightSignal(201, 23279),
                ParserUtil.parseDate("14:00:30_03/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                0, 2);
        assertEquals(timeRanges.get(0).start, 20);
        assertEquals(timeRanges.get(0).end, 77);
        assertEquals(timeRanges.get(1).start, 110);
        assertEquals(timeRanges.get(1).end, 167);

        timeRanges = trafficLightController.getTimeRangesForGreen(    //80 in -> 50-g-90
                trafficLightController.getTrafficLightSignal(201, 23279),
                ParserUtil.parseDate("14:01:20_03/31/2017", "HH:mm:ss_MM/dd/yyyy"),
                0, 2);
        assertEquals(timeRanges.get(0).start, 0);
        assertEquals(timeRanges.get(0).end, 27);
        assertEquals(timeRanges.get(1).start, 60);
        assertEquals(timeRanges.get(1).end, 117);
    }
}
