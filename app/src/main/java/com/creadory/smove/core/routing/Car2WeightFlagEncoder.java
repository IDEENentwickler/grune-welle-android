package com.creadory.smove.core.routing;

import com.graphhopper.reader.ReaderWay;
import com.graphhopper.routing.util.CarFlagEncoder;
import com.graphhopper.routing.util.EncodedDoubleValue;
import com.graphhopper.util.BitUtil;

/**
 * Created by Tim on 25.10.2016.
 */
public class Car2WeightFlagEncoder extends CarFlagEncoder {
    public static final String NAME = "car2";

    private EncodedDoubleValue reverseSpeedEncoder;

    public Car2WeightFlagEncoder(int speedBits, double speedFactor, int maxTurnCosts) {
        super(speedBits, speedFactor, maxTurnCosts);

        //override default speed values
        defaultSpeedMap.put("track", 5);
    }

    @Override
    public int getVersion()
    {
        return 1;
    }

    @Override
    public int defineWayBits( int index, int shift )
    {
        shift = super.defineWayBits(index, shift);
        reverseSpeedEncoder = new EncodedDoubleValue("Reverse Speed", shift, speedBits, speedFactor,
                defaultSpeedMap.get("secondary"), maxPossibleSpeed);
        shift += reverseSpeedEncoder.getBits();
        return shift;
    }

    @Override
    public double getReverseSpeed( long flags )
    {
        double speedVal = reverseSpeedEncoder.getDoubleValue(flags);
        if (speedVal < 0)
            throw new IllegalStateException("Reverse speed was negative!? " + speedVal);

        return speedVal;
    }

    @Override
    public long setReverseSpeed( long flags, double speed )
    {
        if (speed < 0)
            throw new IllegalArgumentException("Speed cannot be negative: " + speed + ", flags:" + BitUtil.LITTLE.toBitString(flags));

        if (speed < speedFactor / 2)        //speedFactor == speedEncoder.factor
            return setLowSpeed(flags, speed, true);

        if (speed > getMaxSpeed())
            speed = getMaxSpeed();

        return reverseSpeedEncoder.setDoubleValue(flags, speed);
    }

    @Override
    public long handleWayTags( ReaderWay way, long allowed, long priorityFromRelation )
    {
        if (!isAccept(allowed)) {
            return 0;
        }

        long flags = 0;
        if (!isFerry(allowed)){
            // get assumed speed from highway type
            double speed = getSpeed(way);
            speed = applyMaxSpeed(way, speed);

            // limit speed to max 30 km/h if bad surface
            if (speed > 30 && way.hasTag("surface", badSurfaceSpeedMap)){
                speed = 30;
            }

            boolean isRoundabout = way.hasTag("junction", "roundabout");
            if (isRoundabout) {
                flags = setBool(0, K_ROUNDABOUT, true);
            }

            if (way.hasTag("oneway", oneways) || isRoundabout){
                if (way.hasTag("oneway", "-1")){
                    flags = setReverseSpeed(flags, speed);
                    flags |= backwardBit;
                } else{
                    flags = setSpeed(flags, speed);
                    flags |= forwardBit;
                }
            } else{
                flags = setSpeed(flags, speed);
                flags = setReverseSpeed(flags, speed);
                flags |= directionBitMask;
            }
        } else {
            double ferrySpeed = getFerrySpeed(way, defaultSpeedMap.get("living_street"), defaultSpeedMap.get("service"), defaultSpeedMap.get("residential"));
            flags = setSpeed(flags, ferrySpeed);
            flags = setReverseSpeed(flags, ferrySpeed);
            flags |= directionBitMask;
        }

        return flags;
    }

    @Override
    protected double getSpeed(ReaderWay way) {
        double speed = super.getSpeed(way);

        if(way.getTag("highway").equals("service") &&
                way.hasTag("est_width") && Double.parseDouble(way.getTag("est_width")) <= 3){
            speed = 0;
        }
        return speed;
    }

    @Override
    protected double applyMaxSpeed(ReaderWay way, double speed) {
        double maxSpeed = getMaxSpeed(way);
        // We obay speed limits
        if (maxSpeed >= 0){
            // return maxSpeed, if available
            return maxSpeed;
        }
        return speed;
    }

    @Override
    protected long setLowSpeed( long flags, double speed, boolean reverse )
    {
        if (reverse)
            return setBool(reverseSpeedEncoder.setDoubleValue(flags, 0), K_BACKWARD, false);

        return setBool(speedEncoder.setDoubleValue(flags, 0), K_FORWARD, false);
    }

    @Override
    public long flagsDefault( boolean forward, boolean backward )
    {
        long flags = super.flagsDefault(forward, backward);
        if (backward)
            return reverseSpeedEncoder.setDefaultValue(flags);

        return flags;
    }

    @Override
    public long setProperties( double speed, boolean forward, boolean backward )
    {
        long flags = super.setProperties(speed, forward, backward);
        if (backward)
            return setReverseSpeed(flags, speed);

        return flags;
    }

    @Override
    public long reverseFlags( long flags )
    {
        // swap access
        flags = super.reverseFlags(flags);

        // swap speeds
        double otherValue = reverseSpeedEncoder.getDoubleValue(flags);
        flags = setReverseSpeed(flags, speedEncoder.getDoubleValue(flags));
        return setSpeed(flags, otherValue);
    }

    @Override
    public String toString()
    {
        return NAME;
    }
}
