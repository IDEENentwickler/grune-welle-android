package com.creadory.smove.core.trafficlights;


import com.google.common.base.CharMatcher;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.THashMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by Tim on 26.01.2017.
 */
public class SignalPlan {
    private static final CharMatcher digitsMatcher = CharMatcher.inRange('0','9').precomputed();
    private static final CharMatcher letterMatcher = CharMatcher.JAVA_LETTER.precomputed();

    private int number; //1-4; if signalPlan OFF: -1
    private int interval;
    private TIntObjectMap<Phase> kTlsPlanEntries;
    private TIntObjectMap<Phase> klTlsPlanEntries;
    private THashMap<String, Phase> kxTlsPlanEntries;

    public SignalPlan(int number, int interval) {
        this.number = number;
        this.interval = interval;
    }

    public void addSignals(String signalNames, String signalString){
        String[] signals = signalString.split("-");
        String startTime = "-1";
        String endTime = "-1";
        for(int i = 1; i < signals.length; i+=2){   //go through all letters in signalString, e.g. 0-r-17-ry-18-g-78-y-81-r-90
            if(signals[i].equalsIgnoreCase("g")){   //set start and end time if green 'g' found
                startTime = signals[i-1];
                endTime = endTime.equalsIgnoreCase("-1") ? signals[i+1] : endTime;  //override phaseEndTime in case of splitted green, e.g. 0-g-13-y-16-r-85-ry-86-g-90
            }
        }
        Phase phase = new Phase(Integer.parseInt(startTime), Integer.parseInt(endTime), Phase.State.GREEN);

        String[] names = signalNames.split("[kK]");
        for(int i = 1; i < names.length; i++){    //go thorugh all elemts in core.db.trafficlights light signal String, e.g. K9K10K11K12K13K14KL30KAL7KAL77
            String digits = digitsMatcher.retainFrom(names[i]);
            String letter = letterMatcher.retainFrom(names[i]);

            if(letter.isEmpty()){   //all forward K.. core.db.trafficlights light signals
                add_K_TlsPlanEntry(Integer.parseInt(digits), phase);
            }else if(letter.equalsIgnoreCase("L")){ //KL (left turn) core.db.trafficlights light signals
                add_KL_TlsPlanEntry(Integer.parseInt(digits), phase);
            }else{  //other K.. signals
                add_KX_TlsPlanEntry(names[i], phase);
            }
        }
    }

    private void add_K_TlsPlanEntry(int nameDigit, Phase phase){
        if(kTlsPlanEntries == null){
            kTlsPlanEntries = new TIntObjectHashMap<Phase>();
        }
        kTlsPlanEntries.put(nameDigit, phase);
    }

    private void add_KL_TlsPlanEntry(int nameDigit, Phase phase){
        if(klTlsPlanEntries == null){
            klTlsPlanEntries = new TIntObjectHashMap<Phase>();
        }
        klTlsPlanEntries.put(nameDigit, phase);
    }

    private void add_KX_TlsPlanEntry(String name, Phase phase){
        if(kxTlsPlanEntries == null){
            kxTlsPlanEntries = new THashMap<String, Phase>();
        }
        kxTlsPlanEntries.put(name, phase);
    }

    public boolean isOff(){
        return number == -1;
    }

    public Phase getPhase(int signalNameDigit){
        return kTlsPlanEntries.get(signalNameDigit);
    }

    public int getNumber() {
        return number;
    }

    public int getInterval() {
        return interval;
    }

    @Override
    public String toString() {
        return "SignalPlan{" +
                "number=" + number +
                ", interval=" + interval +
                ", kTlsPlanEntries=" + kTlsPlanEntries +
                ", klTlsPlanEntries=" + klTlsPlanEntries +
                ", kxTlsPlanEntries=" + kxTlsPlanEntries +
                '}';
    }
}
