package com.creadory.smove.core.routing;

import com.graphhopper.routing.AlgorithmOptions;
import com.graphhopper.routing.RoutingAlgorithm;
import com.graphhopper.routing.RoutingAlgorithmFactorySimple;
import com.graphhopper.storage.Graph;

import static com.creadory.smove.AppConstants.Algorithms.ASTAR_TRAFFIC_LIGHT;
import static com.creadory.smove.AppConstants.Algorithms.DIJKSTRA_TRAFFIC_LIGHT;

/**
 * Created by Tim on 01.02.2017.
 */
public class TrafficLightRoutingAlgorithmFactory extends RoutingAlgorithmFactorySimple {

    @Override
    public RoutingAlgorithm createAlgo(Graph g, AlgorithmOptions opts) {
        if (DIJKSTRA_TRAFFIC_LIGHT.equalsIgnoreCase(opts.getAlgorithm())) {
            RoutingAlgorithm ra = new TrafficLightDijkstra(g, opts.getWeighting(), opts.getTraversalMode());
            ra.setMaxVisitedNodes(opts.getMaxVisitedNodes());
            return ra;
        }else if (ASTAR_TRAFFIC_LIGHT.equalsIgnoreCase(opts.getAlgorithm())) {
            TrafficLightAStar ra = new TrafficLightAStar(g, opts.getWeighting(), opts.getTraversalMode());
            ra.setApproximation(getApproximation(ASTAR_TRAFFIC_LIGHT, opts, g.getNodeAccess()));
            ra.setMaxVisitedNodes(opts.getMaxVisitedNodes());
            return ra;
        } else {
            return super.createAlgo(g, opts);
        }
    }
}
