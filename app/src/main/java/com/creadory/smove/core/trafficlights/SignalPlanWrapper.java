package com.creadory.smove.core.trafficlights;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Tim on 26.01.2017.
 */
public class SignalPlanWrapper {
    private Calendar startTime;
    private Calendar endTime;
    private SignalPlan signalPlan;

    public SignalPlanWrapper(Calendar startTime, Calendar endTime, SignalPlan signalPlan) {
        this.startTime = startTime;
        this.signalPlan = signalPlan;
        this.endTime = endTime;
    }

    public Calendar getStartTime() {
        return startTime;
    }

    public Calendar getEndTime() {
        return endTime;
    }

    public SignalPlan getSignalPlan() {
        return signalPlan;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS", Locale.GERMANY);

        return "SignalPlanWrapper{" +
                "phaseStartTime=" + dateFormat.format(startTime.getTime()) +
                "phaseEndTime=" + dateFormat.format(endTime.getTime()) +
                ", signalPlan=" + signalPlan +
                '}';
    }
}
