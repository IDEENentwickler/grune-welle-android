package com.creadory.smove.core.routing;

import com.creadory.smove.core.routing.weighting.TrafficLightWeighting;
import com.creadory.smove.AppConstants;
import com.graphhopper.routing.AbstractRoutingAlgorithm;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.QueryGraph;
import com.graphhopper.routing.util.TraversalMode;
import com.graphhopper.routing.weighting.BeelineWeightApproximator;
import com.graphhopper.routing.weighting.WeightApproximator;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.SPTEntry;
import com.graphhopper.util.DistancePlaneProjection;
import com.graphhopper.util.EdgeExplorer;
import com.graphhopper.util.EdgeIterator;

import java.util.PriorityQueue;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by Tim on 30.01.2017.
 */
public class TrafficLightAStar extends AbstractRoutingAlgorithm {
    private WeightApproximator weightApprox;
    private int visitedCount;
    private TIntObjectMap<AStarEntry> fromMap;
    private PriorityQueue<AStarEntry> prioQueueOpenSet;
    private AStarEntry currEdge;
    private int to1 = -1;

    public TrafficLightAStar(Graph graph, Weighting weighting, TraversalMode tMode) {
        super(graph, weighting, tMode);
        if(!TrafficLightWeighting.class.isAssignableFrom(weighting.getClass())){
            throw new RuntimeException("TrafficLightAStar works only with TrafficLightWeighting, current weighting: " + weighting.getClass());
        }
        int size = Math.min(Math.max(200, graph.getNodes() / 10), 2000);
        initCollections(size);
        BeelineWeightApproximator defaultApprox = new BeelineWeightApproximator(nodeAccess, weighting);
        defaultApprox.setDistanceCalc(new DistancePlaneProjection());
        setApproximation(defaultApprox);
    }

    /**
     * @param approx defines how distance to goal Node is approximated
     */
    public TrafficLightAStar setApproximation(WeightApproximator approx) {
        weightApprox = approx;
        return this;
    }

    protected void initCollections(int size) {
        fromMap = new TIntObjectHashMap<AStarEntry>();
        prioQueueOpenSet = new PriorityQueue<AStarEntry>(size);
    }

    @Override
    public Path calcPath(int from, int to) {
        if(TrafficLightWeighting.class.isAssignableFrom(weighting.getClass())){
            ((TrafficLightWeighting) weighting).resetForNextPathCalc();
            if(graph instanceof QueryGraph && ((QueryGraph) graph).isVirtualNode(from)){
                EdgeIterator edgeIterator = graph.createEdgeExplorer().setBaseNode(from);
                while(edgeIterator.next()){
                    ((TrafficLightWeighting) weighting).addVirtualNodeRef(edgeIterator.getBaseNode(), edgeIterator.getAdjNode());
                }
            }
        }

        checkAlreadyRun();
        to1 = to;

        weightApprox.setGoalNode(to);
        double weightToGoal = weightApprox.approximate(from);
        currEdge = new AStarEntry(EdgeIterator.NO_EDGE, from, 0 + weightToGoal, 0);
        if (!traversalMode.isEdgeBased()) {
            fromMap.put(from, currEdge);
        }
        return runAlgo();
    }

    private Path runAlgo() {
        double currWeightToGoal, estimationFullWeight;
        EdgeExplorer explorer = outEdgeExplorer;
        while (true) {
            int currVertex = currEdge.adjNode;
            visitedCount++;
            if (isMaxVisitedNodesExceeded())
                return createEmptyPath();

            if (finished())
                break;

            EdgeIterator iter = explorer.setBaseNode(currVertex);
            while (iter.next()) {
                if (!accept(iter, currEdge.edge))
                    continue;

                int neighborNode = iter.getAdjNode();
                int traversalId = traversalMode.createTraversalId(iter, false);
                double alreadyVisitedWeight = ((TrafficLightWeighting)weighting).calcWeight(iter, false, currEdge.edge, currEdge.getWeightOfVisitedPath(), graph.getNodeAccess())
                        + currEdge.getWeightOfVisitedPath();
                if (Double.isInfinite(alreadyVisitedWeight))
                    continue;

                AStarEntry ase = fromMap.get(traversalId);
                if (ase == null || ase.weightOfVisitedPath > alreadyVisitedWeight) {
                    currWeightToGoal = weightApprox.approximate(neighborNode);
                    estimationFullWeight = alreadyVisitedWeight + currWeightToGoal;
                    if (ase == null) {
                        ase = new AStarEntry(iter.getEdge(), neighborNode, estimationFullWeight, alreadyVisitedWeight);
                        fromMap.put(traversalId, ase);
                    } else {
                        assert (ase.weight > 0.9999999 * estimationFullWeight) : "Inconsistent distance estimate "
                                + ase.weight + " vs " + estimationFullWeight + " (" + ase.weight / estimationFullWeight + "), and:"
                                + ase.weightOfVisitedPath + " vs " + alreadyVisitedWeight + " (" + ase.weightOfVisitedPath / alreadyVisitedWeight + ")";
                        prioQueueOpenSet.remove(ase);
                        ase.edge = iter.getEdge();
                        ase.weight = estimationFullWeight;
                        ase.weightOfVisitedPath = alreadyVisitedWeight;
                    }

                    ase.parent = currEdge;
                    prioQueueOpenSet.add(ase);

                    updateBestPath(iter, ase, traversalId);
                }
            }

            if (prioQueueOpenSet.isEmpty())
                return createEmptyPath();

            currEdge = prioQueueOpenSet.poll();
            if (currEdge == null)
                throw new AssertionError("Empty edge cannot happen");
        }

        return extractPath();
    }

    @Override
    protected Path extractPath() {
        return new Path(graph, weighting).
                setWeight(currEdge.weight).setSPTEntry(currEdge).extract();
    }

    @Override
    protected SPTEntry createSPTEntry(int node, double weight) {
        throw new IllegalStateException("use AStarEdge constructor directly");
    }

    @Override
    protected boolean finished() {
        return currEdge.adjNode == to1;
    }

    @Override
    public int getVisitedNodes() {
        return visitedCount;
    }

    @Override
    public String getName() {
        return AppConstants.Algorithms.ASTAR_TRAFFIC_LIGHT;
    }

    public static class AStarEntry extends SPTEntry {
        double weightOfVisitedPath;

        public AStarEntry(int edgeId, int adjNode, double weightForHeap, double weightOfVisitedPath) {
            super(edgeId, adjNode, weightForHeap);
            this.weightOfVisitedPath = weightOfVisitedPath;
        }

        @Override
        public final double getWeightOfVisitedPath() {
            return weightOfVisitedPath;
        }
    }
}
