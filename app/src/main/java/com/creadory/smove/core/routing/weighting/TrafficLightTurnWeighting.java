package com.creadory.smove.core.routing.weighting;

import com.creadory.smove.core.trafficlights.Phase;
import com.creadory.smove.core.trafficlights.TrafficLightController;
import com.creadory.smove.core.trafficlights.TrafficLightSignal;
import com.creadory.smove.core.trafficlights.TrafficLightSignalState;
import com.creadory.smove.core.util.time.ExactTime;
import com.graphhopper.routing.weighting.TurnWeighting;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.storage.TurnCostExtension;
import com.graphhopper.util.DistanceCalcEarth;
import com.graphhopper.util.EdgeIteratorState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Created by Tim on 25.01.2017.
 */
public class TrafficLightTurnWeighting extends TurnWeighting implements TrafficLightWeighting{
    protected final static double SPEED_CONV = 3.6;
    private Logger logger = LoggerFactory.getLogger(getClass());
    private TrafficLightController trafficLightController;
    private TIntObjectMap<TIntIntMap> tlsWaitingTimes;     //to store TLSleftTimes/WaitingTimes for edges
    private TIntObjectMap<TIntSet> virtualNodeRefs;
    private ExactTime exactTime;
    private Calendar calCurrentTime;

    public TrafficLightTurnWeighting(Weighting superWeighting, TurnCostExtension turnCostExt, TrafficLightController trafficLightController, ExactTime exactTime) {
        super(superWeighting, turnCostExt);
        this.trafficLightController = trafficLightController;
        this.tlsWaitingTimes = new TIntObjectHashMap<TIntIntMap>();
        this.virtualNodeRefs = new TIntObjectHashMap<TIntSet>();
        this.exactTime = exactTime;
    }

    public double calcWeight(EdgeIteratorState edge, boolean reverse, int prevOrNextEdgeId, double currentWeight, NodeAccess nodeAccess) {
        double time =  super.calcWeight(edge, reverse, prevOrNextEdgeId);   //time needed to cross current edge, based on edge speed and headingPenalty
        if(time == Double.POSITIVE_INFINITY || trafficLightController == null || exactTime == null){
            return time;
        }

        int baseNode = edge.getBaseNode();
        int adjNode = edge.getAdjNode();
        if(currentWeight == 0){    //only if first node, get real base or adj node, depending if reverse or not
            if(reverse){
                adjNode = getOtherRealNode(adjNode, baseNode);
            }else {
                baseNode = getOtherRealNode(baseNode, adjNode);
            }
        }

        //get Tls on current edge, if existing, depending if reverse or not
        TrafficLightSignal tlSignal;
        if(reverse){
            tlSignal = trafficLightController.getTrafficLightSignal(adjNode, baseNode);
        }else{
            tlSignal = trafficLightController.getTrafficLightSignal(baseNode, adjNode);
        }

        //if tlSignal exists on current edge, calc additional time needed based on left waiting time (if phase=red, when reaching Tls))
        if(tlSignal != null){
            double speed = reverse ? super.getFlagEncoder().getReverseSpeed(edge.getFlags()) : super.getFlagEncoder().getSpeed(edge.getFlags());
            double dist = tlSignal.getDistancePositionToAdjNode() == -1 ? edge.getDistance() : tlSignal.getDistancePositionToAdjNode();

            //only if first node is virtual node, calc dist between tls and virtual node(=current position)
            if(currentWeight == 0 && virtualNodeRefs.containsKey(edge.getBaseNode())) {
                int towerNode = reverse ? edge.getAdjNode() : edge.getBaseNode();
                dist = new DistanceCalcEarth().calcDist(nodeAccess.getLat(towerNode), nodeAccess.getLon(towerNode),
                        tlSignal.getPosition().lat, tlSignal.getPosition().lon);
            }

            double timeToTls = dist / speed * SPEED_CONV;
            Calendar calPlusWeight = Calendar.getInstance(calCurrentTime.getTimeZone());
            calPlusWeight.setTime(calCurrentTime.getTime());

            calPlusWeight.add(Calendar.SECOND,(int)(currentWeight + timeToTls));
            TrafficLightSignalState state = trafficLightController.getTrafficLightSignalState(calPlusWeight, tlSignal);
            if(state.getState() == Phase.State.NOT_GREEN){
                addTlsWaitingTime(edge, reverse, (int)state.getTimeLeft()); //only cast to int, if state == Phase.State.NOT_GREEN or Phase.State.GREEN
                time += state.getTimeLeft();
            }
        }
        return time;
    }

    @Override
    public long calcMillis(EdgeIteratorState edgeState, boolean reverse, int prevOrNextEdgeId) {
        return super.calcMillis(edgeState, reverse, prevOrNextEdgeId) + (getTlsWaitingTime(edgeState, reverse) * 1000);
    }

    public void resetForNextPathCalc(){
        setCurrentTimeForCalcWeighting(exactTime.getCurrentTimeCalendar());
        tlsWaitingTimes.clear();
        virtualNodeRefs.clear();
    }

    public void addVirtualNodeRef(int virtualBaseNode, int adjNode){
        TIntSet secondLevelSet = virtualNodeRefs.get(virtualBaseNode);
        if(secondLevelSet == null){
            secondLevelSet = new TIntHashSet();
            virtualNodeRefs.put(virtualBaseNode, secondLevelSet);
        }
        secondLevelSet.add(adjNode);
    }

    private void setCurrentTimeForCalcWeighting(Calendar calendar){
        calCurrentTime = Calendar.getInstance(calendar.getTimeZone());
        calCurrentTime.setTime(calendar.getTime());
    }

    private int getOtherRealNode(int virtualBaseNode, int realNode){
        TIntSet secondLevelSet = virtualNodeRefs.get(virtualBaseNode);
        if(secondLevelSet != null){
            TIntIterator iter = secondLevelSet.iterator();
            while(iter.hasNext()){
                int node = iter.next();
                if(node != realNode){
                    return node;
                }
            }
        }
        return virtualBaseNode;
    }

    private void addTlsWaitingTime(EdgeIteratorState edge, boolean reverse, int time) {
        int firstNode = reverse ? edge.getAdjNode() : edge.getBaseNode();
        int secondNode = reverse ? edge.getBaseNode() : edge.getAdjNode();

        TIntIntMap secondLevelMap = tlsWaitingTimes.get(firstNode);
        if(secondLevelMap == null){
            secondLevelMap = new TIntIntHashMap();
            tlsWaitingTimes.put(firstNode, secondLevelMap);
        }
        secondLevelMap.put(secondNode, time);
    }

    private int getTlsWaitingTime(EdgeIteratorState edge, boolean reverse){
        int firstNode = reverse ? edge.getAdjNode() : edge.getBaseNode();
        int secondNode = reverse ? edge.getBaseNode() : edge.getAdjNode();

        TIntIntMap secondLevelMap = tlsWaitingTimes.get(firstNode);
        if(secondLevelMap != null && secondLevelMap.containsKey(secondNode)){
            logger.debug(edge.getAdjNode() + "; time: " + secondLevelMap.get(secondNode));
            return secondLevelMap.get(secondNode);
        }
        return 0;
    }

    @Override
    public String getName() {
        return "trafficlight|" + super.getName();
    }
}
