package com.creadory.smove.core.util.time;

import android.util.Log;

import com.creadory.smove.util.ParserUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Tim on 03.02.2017.
 */

public class DebugTime implements ExactTime {
    public static Calendar cal = ParserUtil.parseDate("18:40:19_01/31/2017", "HH:mm:ss_MM/dd/yyyy");

    private Calendar calendar;

    public DebugTime(Calendar calendar) {
        this.calendar = calendar;
        Log.d("DebugTime", cal.getTime().toString());
    }

    @Override
    public Calendar getCurrentTimeCalendar() {
        return calendar;
    }

    @Override
    public Date getCurrentTimeDate() {
        return calendar.getTime();
    }

    @Override
    public boolean hasExactTime() {
        return true;
    }
}
