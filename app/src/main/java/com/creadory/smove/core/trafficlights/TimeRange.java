package com.creadory.smove.core.trafficlights;

/**
 * Created by Tim on 31.03.2017.
 */

public class TimeRange {
    public final long start;
    public final long end;

    public TimeRange(long start, long end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "TimeRange{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
