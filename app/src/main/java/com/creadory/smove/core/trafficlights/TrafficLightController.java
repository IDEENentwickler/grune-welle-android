package com.creadory.smove.core.trafficlights;

import android.util.Log;

import com.creadory.smove.data.local.DatabaseHelper;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.DistanceCalcEarth;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.THashMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by Tim on 27.01.2017.
 */
public class TrafficLightController {
    private final static String TAG = "TrafficLightController";
    private THashMap<String, TrafficLight> trafficLights;
    private TIntObjectMap<TIntObjectMap<TrafficLightSignal>> allTrafficLightSignals;
    private DatabaseHelper databaseHelper;

    public TrafficLightController(DatabaseHelper databaseHelper) {
        this.trafficLights = new THashMap<String, TrafficLight>();
        this.allTrafficLightSignals = new TIntObjectHashMap<TIntObjectMap<TrafficLightSignal>>();
        this.databaseHelper = databaseHelper;
    }

    /**
     * Create TrafficLights inclusive TrafficLightSignals and SignalPlans from SQLite database
     */
    public void initTrafficLights(){
        boolean success;
        int tlWithSchedule = 0;
        int tlWithTlSignals = 0;

        //create TrafficLight and add to map of trafficLights (TrafficLightController.trafficLights)
        success = databaseHelper.fillWithTrafficLights(trafficLights);
        Log.d(TAG, "[" + success + "] Fetching TrafficLights [" + trafficLights.size() + "] from DB -> " +
                "Creating realated WeeklySchedule and TrafficLightSignals from DB ...");

        for(Map.Entry<String, TrafficLight> tlEntry : trafficLights.entrySet()) {
            //create SignalPlans and add to TrafficLight
            TIntObjectMap<SignalPlan> signalPlans = databaseHelper.getSignalPlans(tlEntry.getValue().getName());

            //create weeklySchedule including signalPlans and add to available (FST) TrafficLight
            if(!signalPlans.isEmpty()) {
                success = databaseHelper.fillTrafficLightWithWeeklySchedule(tlEntry.getValue(), signalPlans);
                tlWithSchedule += success ? 1 : 0;
            }

            //create TrafficLightSignals and add to related TrafficLight and to map of all trafficLightSignals (TrafficLightController.trafficLightSignals)
            success = databaseHelper.fillTrafficLightWithTrafficLightSignals(tlEntry.getValue());
            tlWithTlSignals += success ? 1 : 0;

            //add all TrafficLightSignals in one map to find it faster during routing
            TIntObjectIterator<TIntObjectMap<TrafficLightSignal>> iter = tlEntry.getValue().getTrafficLightSignals().iterator();
            while(iter.hasNext()){
                iter.advance();

                TIntObjectMap<TrafficLightSignal> secondLevelMap;
                if (allTrafficLightSignals.containsKey(iter.key())) {
                    secondLevelMap = allTrafficLightSignals.get(iter.key());
                } else {
                    secondLevelMap = new TIntObjectHashMap<TrafficLightSignal>();
                    allTrafficLightSignals.put(iter.key(), secondLevelMap);
                }
                secondLevelMap.putAll(iter.value());
            }
        }
        Log.d(TAG, "Done creating realated WeeklySchedule[" + tlWithSchedule + "/" + trafficLights.size() +
                "] and TrafficLightSignals[" + tlWithTlSignals + "/" + trafficLights.size() + "] from DB.");
    }

    /**
     * Calc distance between TrafficLightSignal Position <-and-> TrafficLightSignal AdjNode of all available TrafficLights
     */
    public void calculateAllDistancesOfTlsToAdjNode(NodeAccess nodeAccess){
        DistanceCalc distanceCalc = new DistanceCalcEarth();
        TIntObjectIterator<TIntObjectMap<TrafficLightSignal>> iter = getAllTrafficLightSignals().iterator();
        while(iter.hasNext()){
            iter.advance();

            TIntObjectIterator<TrafficLightSignal> iterSecond = iter.value().iterator();
            while(iterSecond.hasNext()){
                iterSecond.advance();
                TrafficLightSignal tls = iterSecond.value();
                double distance = distanceCalc.calcDist(tls.getPosition().lat, tls.getPosition().lon,
                        nodeAccess.getLat(tls.getAdjNode()), nodeAccess.getLon(tls.getAdjNode()));
                tls.setDistancePositionToAdjNode(distance);
            }
        }
    }

    /**
     * Get TrafficLightSignal wich stands on edge or null. TrafficLightSignal facing direction baseNode -> adjNode
     *
     * @param adjNode adjNode of TrafficLightSignal
     * @param baseNode baseNode of TrafficLightSignal
     */
    public TrafficLightSignal getTrafficLightSignal(int adjNode, int baseNode){
        TIntObjectMap<TrafficLightSignal> foundEdges = getTrafficLightSignal(adjNode);
        if(foundEdges != null){
            return foundEdges.get(baseNode);
        }else{
            return null;
        }
    }

    /**
     * Get TrafficLightSignals wich have the given adjNode or null.
     */
    public TIntObjectMap<TrafficLightSignal> getTrafficLightSignal(int adjNode){
        return allTrafficLightSignals.get(adjNode);
    }

    public TIntObjectMap<TIntObjectMap<TrafficLightSignal>> getAllTrafficLightSignals() {
        return allTrafficLightSignals;
    }

    public THashMap<String, TrafficLight> getTrafficLights() {
        return trafficLights;
    }

    /**
     * Get TrafficLightSignalState at a specific time (weekday, hour, minute)
     */
    public TrafficLightSignalState getTrafficLightSignalState(Calendar currentDate, TrafficLightSignal trafficLightSignal){
        SignalPlanWrapper signalPlanWrapper = trafficLightSignal.getTrafficLight().getSignalPlanByDate(currentDate);
        if(signalPlanWrapper != null) {
            if(!signalPlanWrapper.getSignalPlan().isOff()) {
                Phase phase = getPhase(signalPlanWrapper, trafficLightSignal);
                if(phase == null){  //if no phase found -> return unknown phase
                    return new TrafficLightSignalState(Phase.State.UNKNOWN, -1);
                }

                int interval = signalPlanWrapper.getSignalPlan().getInterval();
                int secondInInterval = getSecondInInterval(signalPlanWrapper.getStartTime(), currentDate, interval);

                Phase.State state = Phase.State.GREEN;
                int timeLeft = -1;
                if (phase.isSplitted()) {    //if splitted green phase, e.g. 0-g-20-...-40-g-80
                    if(secondInInterval >= phase.start() || secondInInterval < phase.end()) {   //in green phase
                        timeLeft = secondInInterval >= phase.start() ? (interval - secondInInterval) + (phase.end() - 0) : (phase.end() - 0) - secondInInterval;
                    }else { //not in green phase
                        state = Phase.State.NOT_GREEN;
                        timeLeft = (phase.start() - 0) - secondInInterval;
                    }
                } else{                     //if normal green phase, e.g. 0-...-20-g-40-...-80
                    if (secondInInterval >= phase.start() && secondInInterval < phase.end()) {  //in green phase
                        timeLeft = (phase.end() - 0) - secondInInterval;
                    } else {  //if not in green phase
                        state = Phase.State.NOT_GREEN;
                        timeLeft = secondInInterval >= phase.end() ? (interval - secondInInterval) + (phase.start() - 0) : (phase.start() - 0) - secondInInterval;
                    }
                }

                return new TrafficLightSignalState(state, timeLeft, phase.start(), phase.end(), secondInInterval);
            }else{
                return new TrafficLightSignalState(Phase.State.OFF, trafficLightSignal.getTrafficLight().getTimeInSecondsTillSignalPlanEnd(signalPlanWrapper, currentDate));
            }
        }else{
            return new TrafficLightSignalState(Phase.State.UNKNOWN, -1);
        }
    }

    public ArrayList<TimeRange> getTimeRangesForGreen(TrafficLightSignal trafficLightSignal, Calendar currentDate, double secondsToTls, int numberOfNextRanges) {
        ArrayList<TimeRange> timeRanges = new ArrayList<>();
        int roundedSecondsToTls = (int) Math.round(secondsToTls);
        currentDate.add(Calendar.SECOND, roundedSecondsToTls);

        SignalPlanWrapper signalPlanWrapper = trafficLightSignal.getTrafficLight().getSignalPlanByDate(currentDate);
        if (signalPlanWrapper != null && !signalPlanWrapper.getSignalPlan().isOff()) {
            Phase phase = getPhase(signalPlanWrapper, trafficLightSignal);
            if (phase != null) {  //if phase found
                int interval = signalPlanWrapper.getSignalPlan().getInterval();
                int secondInInterval = getSecondInInterval(signalPlanWrapper.getStartTime(), currentDate, interval);

                int timeLeft = 0;
                if (phase.isSplitted()) {    //if splitted green phase, e.g. 0-g-20-...-40-g-80
                    int phaseDuration = (interval - phase.start()) +  phase.end();
                    int startIndex = 0;
                    int endIndex = numberOfNextRanges;

                    if (secondInInterval >= phase.start() || secondInInterval < phase.end()) {   //in green phase
                        if(secondInInterval >= phase.start()){    //40-g-80
                            startIndex = 1;
                            timeLeft = (interval - secondInInterval) + phase.end();
                        }else{                                  //0-g-20
                            endIndex--;
                            timeLeft = phase.end() - secondInInterval;
                        }
                        int rangeEnd = roundedSecondsToTls + timeLeft;
                        int rangeStart = (rangeEnd - phaseDuration) > 0 ? (rangeEnd - phaseDuration) : 0;
                        timeRanges.add(new TimeRange(rangeStart, rangeEnd));
                    } else {    //not in green phase
                        //do nothing
                    }

                    for (int i = startIndex; i < endIndex; i++) {
                        int rangeStart = phase.start() + (i * interval) - secondInInterval;
                        timeRanges.add(new TimeRange(roundedSecondsToTls + rangeStart, roundedSecondsToTls + rangeStart + phaseDuration));
                    }
                } else {                     //if normal green phase, e.g. 0-...-20-g-40-...-80
                    int phaseDuration = phase.end() - phase.start();
                    int startIndex = 1;
                    int endIndex = numberOfNextRanges;

                    if (secondInInterval >= phase.start() && secondInInterval < phase.end()) {  //in green phase
                        timeLeft = phase.end() - secondInInterval;

                        int rangeEnd = roundedSecondsToTls + timeLeft;
                        int rangeStart = (rangeEnd - phaseDuration) > 0 ? (rangeEnd - phaseDuration) : 0;
                        timeRanges.add(new TimeRange(rangeStart, rangeEnd));
                    } else {    //not in green phase
                        if(secondInInterval >= phase.end()){    //40-...-80
                            startIndex = 2;
                            endIndex++;
                            timeLeft = (interval - secondInInterval) + phase.start();
                        }else{                                  //0-...-20
                            timeLeft = phase.start() - secondInInterval;
                        }
                        timeRanges.add(new TimeRange(roundedSecondsToTls + timeLeft, roundedSecondsToTls + timeLeft + phaseDuration));
                    }

                    for (int i = startIndex; i < endIndex; i++) {
                        int rangeStart = phase.start() + (i * interval) - secondInInterval;
                        timeRanges.add(new TimeRange(roundedSecondsToTls + rangeStart, roundedSecondsToTls + rangeStart + phaseDuration));
                    }
                }
            }
        }

        return timeRanges;
    }

    private Phase getPhase(SignalPlanWrapper signalPlanWrapper, TrafficLightSignal trafficLightSignal){
        Phase phase = signalPlanWrapper.getSignalPlan().getPhase(trafficLightSignal.getFirst_K_TlsNameDigit());
        if(phase == null){  //just in case, First_K_TlsNameDigit can not be found in current signal plan
            phase = signalPlanWrapper.getSignalPlan().getPhase(trafficLightSignal.getLast_K_TlsNameDigit());
            //if even Last_K_TlsNameDigit can not be found in current signal plan -> phase still null
        }
        return phase;   //returns null if no TlsName of trafficLightSignal found in signalPlan
    }

    private int getSecondInInterval(Calendar signalPlanStartTime, Calendar currentTime, int interval){
        int hourSignalPlan = signalPlanStartTime.get(Calendar.HOUR_OF_DAY);
        int minuteSignalPlan = signalPlanStartTime.get(Calendar.MINUTE);

        int hourCurrentTime = currentTime.get(Calendar.HOUR_OF_DAY);
        int minuteCurrentTime = currentTime.get(Calendar.MINUTE);
        int secondCurrentTime = currentTime.get(Calendar.SECOND);

        int hourDiff = 0;
        int minuteDiff = 0;

        if(hourCurrentTime >= hourSignalPlan){  //interval started previously at the same day
            hourDiff = hourCurrentTime - hourSignalPlan;
        }else{                                  //interval started one day before/yesterday
            hourDiff = (24 - hourSignalPlan) + hourCurrentTime;
        }

        minuteDiff = minuteCurrentTime - minuteSignalPlan;
        if(minuteDiff < 0){
            hourDiff = hourDiff - 1;
            minuteDiff = 60 + minuteDiff;
        }

        int currentSec = ((hourDiff * 60 * 60) + (minuteDiff * 60) + secondCurrentTime) % interval;
//        Log.d("TrafficLight", "Time Diffs: " + hourDiff + ":" + minuteDiff + ":" + secondCurrentTime);
//        Log.d("TrafficLight", "currentSec: " + currentSec);
        return currentSec;
    }
}
