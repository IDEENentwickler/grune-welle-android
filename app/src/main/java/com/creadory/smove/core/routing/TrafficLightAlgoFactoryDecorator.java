package com.creadory.smove.core.routing;

import com.graphhopper.routing.RoutingAlgorithmFactory;
import com.graphhopper.routing.RoutingAlgorithmFactoryDecorator;
import com.graphhopper.routing.util.HintsMap;

/**
 * Created by Tim on 01.02.2017.
 */
public class TrafficLightAlgoFactoryDecorator implements RoutingAlgorithmFactoryDecorator {
    private boolean enabled = true;

    @Override
    public RoutingAlgorithmFactory getDecoratedAlgorithmFactory(RoutingAlgorithmFactory algoFactory, HintsMap optionsMap) {
        return new TrafficLightRoutingAlgorithmFactory();
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public final void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
