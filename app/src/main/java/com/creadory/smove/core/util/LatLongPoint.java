package com.creadory.smove.core.util;

import com.graphhopper.util.shapes.GHPoint;

/**
 *
 * @author Peter Karich
 */
public class LatLongPoint {

    public final double lat;
    public final double lon;

    public LatLongPoint(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LatLongPoint latLongPoint = (LatLongPoint) o;

        if (Double.compare(latLongPoint.lat, lat) != 0) return false;
        return Double.compare(latLongPoint.lon, lon) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(lat);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lon);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public GHPoint toGHPoint() {
        return new GHPoint(lat, lon);
    }

    @Override
    public String toString() {
        return lat + ", " + lon;
    }
}
