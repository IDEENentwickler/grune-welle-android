package com.creadory.smove.core.routing;

import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.HintsMap;
import com.graphhopper.routing.util.TraversalMode;
import com.graphhopper.routing.weighting.FastestWeighting;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.TurnCostExtension;
import com.creadory.smove.core.routing.weighting.TrafficLightFastestWeighting;
import com.creadory.smove.core.routing.weighting.TrafficLightTurnWeighting;
import com.creadory.smove.core.trafficlights.TrafficLightController;
import com.creadory.smove.core.util.time.ExactTime;

/**
 * Created by Tim on 03.02.2017.
 */
public class TrafficLightGraphHopper extends GraphHopper {
    private TrafficLightController trafficLightController;
    private ExactTime exactTime;

    public TrafficLightGraphHopper(){
        super();
    }

    public TrafficLightGraphHopper(TrafficLightController trafficLightController, ExactTime exactTime) {
        this();
        this.trafficLightController = trafficLightController;
        this.exactTime = exactTime;
    }

    public Weighting createWeighting(HintsMap hintsMap, FlagEncoder encoder) {
        String weighting = hintsMap.getWeighting().toLowerCase();
        if ("trafficlight".equalsIgnoreCase(weighting) || weighting.isEmpty()) {
            return new TrafficLightFastestWeighting(encoder, hintsMap, trafficLightController, exactTime);
        }
        return super.createWeighting(hintsMap, encoder);
    }

    @Override
    public Weighting createTurnWeighting(Graph graph, Weighting weighting, TraversalMode tMode) {
        FlagEncoder encoder = weighting.getFlagEncoder();
        if (FastestWeighting.class.isAssignableFrom(weighting.getClass()) && encoder.supports(TrafficLightTurnWeighting.class) && !tMode.equals(TraversalMode.NODE_BASED)) {
            return new TrafficLightTurnWeighting(weighting, (TurnCostExtension) graph.getExtension(), trafficLightController, exactTime);
        }
        return super.createTurnWeighting(graph, weighting, tMode);
    }

    public void setTrafficLightController(TrafficLightController trafficLightController) {
        this.trafficLightController = trafficLightController;
    }

    public void setExactTime(ExactTime exactTime) {
        this.exactTime = exactTime;
    }
}
