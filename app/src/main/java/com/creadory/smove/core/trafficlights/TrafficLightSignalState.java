package com.creadory.smove.core.trafficlights;

/**
 * Created by Tim on 27.01.2017.
 */
public class TrafficLightSignalState {
    private Phase.State phaseState;  //current phaseState
    private long timeLeft;  //time left in current phaseState, in seconds

    private int phaseStartTime;
    private int phaseEndTime;
    private int secondInInterval;

    public TrafficLightSignalState(Phase.State phaseState, long timeLeft, int phaseStartTime, int phaseEndTime, int secondInInterval) {
        this.phaseState = phaseState;
        this.timeLeft = timeLeft;
        this.phaseStartTime = phaseStartTime;
        this.phaseEndTime = phaseEndTime;
        this.secondInInterval = secondInInterval;
    }

    public TrafficLightSignalState(Phase.State phaseState, long timeLeft) {
        this.phaseState = phaseState;
        this.timeLeft = timeLeft;
    }

    public Phase.State getState() {
        return phaseState;
    }

    public long getTimeLeft() {
        return timeLeft;
    }

    public int getPhaseEndTime() {
        return phaseEndTime;
    }

    public int getPhaseStartTime() {
        return phaseStartTime;
    }

    public int getSecondInInterval() {
        return secondInInterval;
    }

    @Override
    public String toString() {
        return "TrafficLightSignalState{" +
                "phaseState=" + phaseState +
                ", timeLeft=" + timeLeft +
                ", phaseStartTime=" + phaseStartTime +
                ", phaseEndTime=" + phaseEndTime +
                ", secondInInterval=" + secondInInterval +
                '}';
    }
}
