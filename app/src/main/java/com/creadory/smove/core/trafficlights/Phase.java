package com.creadory.smove.core.trafficlights;

/**
 * Created by Tim on 26.01.2017.
 */
public class Phase {
    public enum State {
        GREEN, NOT_GREEN, UNKNOWN, OFF
    }

    private int startTime;
    private int endTime;
    private boolean splitted;
    private State state;

    public Phase(int startTime, int endTime, State state) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.state = state;

        if(startTime < endTime){    //e.g. 0-...-20-g-40-...-80
            splitted = false;
        }else{                      //e.g. 0-g-20-...-40-g-80
            splitted = true;
        }
    }

    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public int start() {
        return startTime;
    }

    public int end() {
        return endTime;
    }

    public boolean isSplitted() {
        return splitted;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Phase{" +
                "phaseStartTime=" + startTime +
                ", phaseEndTime=" + endTime +
                ", state=" + state +
                '}';
    }
}
