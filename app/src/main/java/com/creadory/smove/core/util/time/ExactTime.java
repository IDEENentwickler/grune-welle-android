package com.creadory.smove.core.util.time;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Tim on 02.02.2017.
 */
public interface ExactTime {
    public Calendar getCurrentTimeCalendar();

    public Date getCurrentTimeDate();

    public boolean hasExactTime();
}
