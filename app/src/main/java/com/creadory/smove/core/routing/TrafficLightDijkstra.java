package com.creadory.smove.core.routing;

import com.creadory.smove.core.routing.weighting.TrafficLightWeighting;
import com.graphhopper.routing.Dijkstra;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.QueryGraph;
import com.graphhopper.routing.util.TraversalMode;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.SPTEntry;
import com.graphhopper.util.EdgeExplorer;
import com.graphhopper.util.EdgeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Tim on 30.01.2017.
 */
public class TrafficLightDijkstra extends Dijkstra {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public TrafficLightDijkstra(Graph graph, Weighting weighting, TraversalMode tMode) {
        super(graph, weighting, tMode);
        if(!TrafficLightWeighting.class.isAssignableFrom(weighting.getClass())){
            throw new RuntimeException("TrafficLightDijkstra works only with TrafficLightWeighting, current weighting: " + weighting.getClass());
        }
    }

    @Override
    public Path calcPath(int from, int to) {
        if(TrafficLightWeighting.class.isAssignableFrom(weighting.getClass())){
            ((TrafficLightWeighting) weighting).resetForNextPathCalc();
            if(graph instanceof QueryGraph && ((QueryGraph) graph).isVirtualNode(from)){
                EdgeIterator edgeIterator = graph.createEdgeExplorer().setBaseNode(from);
                while(edgeIterator.next()){
                    ((TrafficLightWeighting) weighting).addVirtualNodeRef(edgeIterator.getBaseNode(), edgeIterator.getAdjNode());
                }
            }
        }
        return super.calcPath(from, to);
    }

    @Override
    protected void runAlgo() {
        EdgeExplorer explorer = outEdgeExplorer;
        while (true) {
            if (isMaxVisitedNodesExceeded() || finished())
                break;

            int startNode = currEdge.adjNode;
            EdgeIterator iter = explorer.setBaseNode(startNode);
            while (iter.next()) {
                if (!accept(iter, currEdge.edge))
                    continue;

                int traversalId = traversalMode.createTraversalId(iter, false);
                double tmpWeight = ((TrafficLightWeighting)weighting).calcWeight(iter, false, currEdge.edge, currEdge.getWeightOfVisitedPath(), graph.getNodeAccess()) + currEdge.weight;

                if (Double.isInfinite(tmpWeight))
                    continue;

                SPTEntry nEdge = fromMap.get(traversalId);
                if (nEdge == null) {
                    nEdge = new SPTEntry(iter.getEdge(), iter.getAdjNode(), tmpWeight);
                    nEdge.parent = currEdge;
                    fromMap.put(traversalId, nEdge);
                    fromHeap.add(nEdge);
                } else if (nEdge.weight > tmpWeight) {
                    fromHeap.remove(nEdge);
                    nEdge.edge = iter.getEdge();
                    nEdge.weight = tmpWeight;
                    nEdge.parent = currEdge;
                    fromHeap.add(nEdge);
                } else
                    continue;

                updateBestPath(iter, nEdge, traversalId);
            }

            if (fromHeap.isEmpty())
                break;

            currEdge = fromHeap.poll();
            if (currEdge == null)
                throw new AssertionError("Empty edge cannot happen");
        }
    }
}
