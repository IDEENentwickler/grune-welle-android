package com.creadory.smove.core.routing.weighting;

import com.graphhopper.storage.NodeAccess;
import com.graphhopper.util.EdgeIteratorState;

/**
 * Created by Tim on 03.02.2017.
 */
public interface TrafficLightWeighting {
    public double calcWeight(EdgeIteratorState edge, boolean reverse, int prevOrNextEdgeId, double currentWeight, NodeAccess nodeAccess);

    public void addVirtualNodeRef(int virtualBaseNode, int adjNode);

    public void resetForNextPathCalc();
}
