package com.creadory.smove.core.trafficlights;

import com.creadory.smove.core.util.LatLongPoint;
import com.google.common.base.CharMatcher;

import gnu.trove.set.hash.THashSet;
import gnu.trove.stack.TIntStack;
import gnu.trove.stack.array.TIntArrayStack;

/**
 * Created by Tim on 27.01.2017.
 */
public class TrafficLightSignal {
    private static final CharMatcher digitsMatcher = CharMatcher.inRange('0', '9').precomputed();
    private static final CharMatcher letterMatcher = CharMatcher.JAVA_LETTER.precomputed();

    private TrafficLight trafficLight;
    private LatLongPoint position;
    private int baseNode;
    private int adjNode;
    private TIntStack kTlsNames;
    private TIntStack klTlsNames;
    private THashSet<String> kxTlsNames;
    private double distancePositionToAdjNode = -1;

    public TrafficLightSignal(TrafficLight trafficLight, LatLongPoint position, int baseNode, int adjNode) {
        this.trafficLight = trafficLight;
        this.position = position;
        this.baseNode = baseNode;
        this.adjNode = adjNode;
    }

    public void setSignalNames(String signalNames) {
        String[] names = signalNames.split("[kK]");
        for (int i = names.length-1; i > 0; i--) {    //go thorugh all elemts in traffic light signal String, e.g. K9K10K11K12K13K14KL30KAL7KAL77
            String digits = digitsMatcher.retainFrom(names[i]);
            String letter = letterMatcher.retainFrom(names[i]);

            if (letter.isEmpty()) {   //all forward K.. traffic light signals
                add_K_TlsName(Integer.parseInt(digits));
            } else if (letter.equalsIgnoreCase("L")) { //KL (left turn) traffic light signals
                add_KL_TlsName(Integer.parseInt(digits));
            } else {  //other K.. signals
                add_KX_TlsName(names[i]);
            }
        }
    }

    private void add_K_TlsName(int nameDigit) {
        if (kTlsNames == null) {
            kTlsNames = new TIntArrayStack();
        }
        kTlsNames.push(nameDigit);
    }

    private void add_KL_TlsName(int nameDigit) {
        if (klTlsNames == null) {
            klTlsNames = new TIntArrayStack();
        }
        klTlsNames.push(nameDigit);
    }

    private void add_KX_TlsName(String name) {
        if (kxTlsNames == null) {
            kxTlsNames = new THashSet<String>();
        }
        kxTlsNames.add(name);
    }

    public void setDistancePositionToAdjNode(double distancePositionToAdjNode) {
        this.distancePositionToAdjNode = distancePositionToAdjNode;
    }

    public double getDistancePositionToAdjNode() {
        return distancePositionToAdjNode;
    }

    public int getFirst_K_TlsNameDigit(){
        return kTlsNames.peek();
    }

    public int getLast_K_TlsNameDigit(){
        int[] kTlsNamesArray = kTlsNames.toArray();
        return kTlsNamesArray[kTlsNamesArray.length-1];
    }

    public TrafficLight getTrafficLight() {
        return trafficLight;
    }

    public LatLongPoint getPosition() {
        return position;
    }

    public int getBaseNode() {
        return baseNode;
    }

    public int getAdjNode() {
        return adjNode;
    }

    @Override
    public String toString() {
        return "TrafficLightSignal{" +
                "trafficLight=" + trafficLight +
                ", position=" + position +
                ", baseNode=" + baseNode +
                ", adjNode=" + adjNode +
                ", kTlsNames=" + kTlsNames +
                ", klTlsNames=" + klTlsNames +
                ", kxTlsNames=" + kxTlsNames +
                '}';
    }
}
