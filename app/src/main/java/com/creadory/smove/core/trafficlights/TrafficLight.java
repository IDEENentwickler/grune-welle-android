package com.creadory.smove.core.trafficlights;

import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.util.ParserUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * Created by Tim on 26.01.2017.
 */
public class TrafficLight {
    private static final Calendar primeDateMidnight = ParserUtil.parseDate("23:59", "HH:mm");
    private Logger logger = LoggerFactory.getLogger(getClass());
    private String name;
    private int nameExtension;
    private LatLongPoint position;
    private TIntObjectMap<TIntObjectMap<TrafficLightSignal>> trafficLightSignals;
    private TIntObjectMap<TreeMap<Calendar, SignalPlanWrapper>> weeklySchedule;

    public TrafficLight(String name, int nameExtension, LatLongPoint position) {
        this.name = name;
        this.nameExtension = nameExtension;
        this.position = position;
        this.trafficLightSignals = new TIntObjectHashMap<TIntObjectMap<TrafficLightSignal>>();
        this.weeklySchedule = new TIntObjectHashMap<TreeMap<Calendar, SignalPlanWrapper>>();
    }

    public void addTrafficLightSignal(TrafficLightSignal trafficLightSignal) {
        TIntObjectMap<TrafficLightSignal> secondLevelMap;
        if (trafficLightSignals.containsKey(trafficLightSignal.getAdjNode())) {
            secondLevelMap = trafficLightSignals.get(trafficLightSignal.getAdjNode());
        } else {
            secondLevelMap = new TIntObjectHashMap<TrafficLightSignal>();
            trafficLightSignals.put(trafficLightSignal.getAdjNode(), secondLevelMap);
        }
        secondLevelMap.put(trafficLightSignal.getBaseNode(), trafficLightSignal);
    }

    public void addWeekDaySchedule(String weekDays, String startTimeString, String endTimeString, SignalPlan signalPlan) {
        String[] days = weekDays.split(",");
        Calendar startTime = ParserUtil.parseDate(startTimeString, "HH:mm");
        Calendar endTime = ParserUtil.parseDate(endTimeString, "HH:mm");
        if (startTime == null || endTime == null) {
            return;
        }

        TreeMap<Calendar, SignalPlanWrapper> dailySchedule;
        int day = convertToDayOfWeek(Integer.parseInt(days[0]));
        if (weeklySchedule.containsKey(day)) {
            dailySchedule = weeklySchedule.get(day);
        } else {
            dailySchedule = new TreeMap<Calendar, SignalPlanWrapper>();
            for (int i = 0; i < days.length; i++) {
                weeklySchedule.put(convertToDayOfWeek(Integer.parseInt(days[i])), dailySchedule);
            }
        }
        dailySchedule.put(startTime, new SignalPlanWrapper(startTime, endTime, signalPlan));
    }

    public SignalPlanWrapper getSignalPlanByDate(Calendar date) {
        TreeMap<Calendar, SignalPlanWrapper> dailySchedule = weeklySchedule.get(date.get(Calendar.DAY_OF_WEEK));
        if (dailySchedule != null) {
            date.set(primeDateMidnight.get(Calendar.YEAR), primeDateMidnight.get(Calendar.MONTH), primeDateMidnight.get(Calendar.DATE));
            Map.Entry<Calendar, SignalPlanWrapper> entry = dailySchedule.floorEntry(date);
            if (entry == null) {  //if currentTime < signalPlanStartTime, e.g. 05:00 < 08:30, set get last signalPlanStartTime of privious day
                entry = dailySchedule.floorEntry(primeDateMidnight);
            }

            return entry.getValue();
        } else {
            return null;
        }
    }

    public long getTimeInSecondsTillSignalPlanEnd(SignalPlanWrapper signalPlanWrapper, Calendar date){
        long seconds = 0;
        date.set(primeDateMidnight.get(Calendar.YEAR), primeDateMidnight.get(Calendar.MONTH), primeDateMidnight.get(Calendar.DATE));
        if(date.before(signalPlanWrapper.getEndTime())){    //e.g. date(04:00) before phaseEndTime(6:00)
            seconds = (signalPlanWrapper.getEndTime().getTimeInMillis() - date.getTimeInMillis()) / 1000;
        }else if(date.after(signalPlanWrapper.getStartTime())){     //e.g. date(23:00) after phaseStartTime(22:00) and not before phaseEndTime(6:00)
            int offset = signalPlanWrapper.getEndTime().get(Calendar.ZONE_OFFSET) + signalPlanWrapper.getEndTime().get(Calendar.DST_OFFSET);
            seconds = ((primeDateMidnight.getTimeInMillis() + (60*1000) - date.getTimeInMillis()) +     //difference 23:00-00:00
                    (signalPlanWrapper.getEndTime().getTimeInMillis() + offset)) / 1000;                //plus difference 00:00-06:00
        }else if(signalPlanWrapper.getStartTime().equals(signalPlanWrapper.getEndTime())){     //e.g. phaseStartTime(00:00) == phaseEndTime(00:00), signalPlan used for hole day
            seconds = ((primeDateMidnight.getTimeInMillis() + (60*1000) - date.getTimeInMillis()) / 1000);     //time to 00:00
        }
        return seconds;
    }

    private int convertToDayOfWeek(int weekDay) {
        switch (weekDay) {
            case 1:
                return Calendar.MONDAY;
            case 2:
                return Calendar.TUESDAY;
            case 3:
                return Calendar.WEDNESDAY;
            case 4:
                return Calendar.THURSDAY;
            case 5:
                return Calendar.FRIDAY;
            case 6:
                return Calendar.SATURDAY;
            case 7:
            case 0:
                return Calendar.SUNDAY;
            default:
                return -1;
        }
    }

    public String getName() {
        return name;
    }

    public boolean hasNameExtension() {
        return nameExtension != 0;
    }

    public int getNameExtension() {
        return nameExtension;
    }

    public LatLongPoint getPosition() {
        return position;
    }

    public TIntObjectMap<TIntObjectMap<TrafficLightSignal>> getTrafficLightSignals() {
        return trafficLightSignals;
    }

    public boolean hasWeeklySchedule() {
        return !weeklySchedule.isEmpty();
    }

    @Override
    public String toString() {
        String weekSchedule = "";
        TIntObjectIterator<TreeMap<Calendar, SignalPlanWrapper>> iter = weeklySchedule.iterator();
        while (iter.hasNext()) {
            iter.advance();
            weekSchedule += ">" + DateFormatSymbols.getInstance().getShortWeekdays()[iter.key()] + "\n";
            for (Map.Entry<Calendar, SignalPlanWrapper> entry : iter.value().entrySet()) {
                weekSchedule += " [" + entry.getKey().getTime().toString() + "] " + entry.getValue().getSignalPlan().toString() + "\n";
            }
        }
        return "TrafficLight{" +
                "name='" + name + '\'' +
                ", nameExtension=" + nameExtension +
                ", position=" + position +
                ", weeklySchedule=\n" + weekSchedule +
                '}';
    }
}
