package com.creadory.smove.data.local;

import com.creadory.smove.data.DataManager;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Tim on 14.03.2017.
 */

@Singleton
public class StateHelper implements DataManager.StateHelperInt{
    public enum State {
        START, SEARCH, SEARCH_RESULT, ROUTER_RESULT, NAVIGATION
    }

    private State mCurrentState;

    @Inject
    public StateHelper() {
        mCurrentState = State.START;
    }

    @Override
    public State getCurrentState() {
        return mCurrentState;
    }

    @Override
    public void setCurrentState(State currentState) {
        this.mCurrentState = currentState;
    }
}
