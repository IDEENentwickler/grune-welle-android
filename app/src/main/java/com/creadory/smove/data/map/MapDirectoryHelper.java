package com.creadory.smove.data.map;

import android.content.Context;
import android.os.Build;
import android.os.Environment;

import com.creadory.smove.AppConstants;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.injection.ApplicationContext;
import com.creadory.smove.util.LogUtil;

import org.oscim.core.Tile;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Tim on 01.03.2017.
 */

@Singleton
public class MapDirectoryHelper implements DataManager.MapDirectoryHelperInt{
    private static final String TAG = "MapDirectoryHelper";

    private File mMapsFolder;
    private File mAreaFolder;
    private String mCurrentArea = AppConstants.MAP_DATA.DEFAULT_AREA; //initial value

    @Inject
    public MapDirectoryHelper(@ApplicationContext Context context, LogUtil logUtil) {
        // get folder of map and graph data
        Tile.SIZE = Tile.calculateTileSize(context.getResources().getDisplayMetrics().scaledDensity);

        boolean greaterOrEqKitkat = Build.VERSION.SDK_INT >= 19;
        if (greaterOrEqKitkat) {
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                logUtil.logUser(TAG, "GraphHopper is not usable without an external storage!");
                return;
            }
            mMapsFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), AppConstants.MAP_DATA.DIRECTORY);
        } else {
            mMapsFolder = new File(Environment.getExternalStorageDirectory(), AppConstants.MAP_DATA.DIRECTORY);
        }

        if (!mMapsFolder.exists()) {
            mMapsFolder.mkdirs();
        }

        mAreaFolder = new File(mMapsFolder, mCurrentArea + "-gh");
    }

    /**
     * returns mapsFolder + AppConstants.MAP_DATA.DEFAULT_AREA + "-gh"
     */
    @Override
    public File getAreaFolder() {
        return mAreaFolder;
    }

    @Override
    public File getMapsFolder() {
        return mMapsFolder;
    }

    @Override
    public String getCurrentArea() {
        return mCurrentArea;
    }
}
