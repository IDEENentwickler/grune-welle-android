package com.creadory.smove.data.map.util;

import com.graphhopper.GHResponse;
import com.graphhopper.routing.Path;

import java.util.List;

/**
 * Created by Tim on 11.03.2017.
 */

public class CustomGHResponse extends GHResponse {
    private List<Path> mPaths;
    private long mFaster = 0;

    public List<Path> getPaths() {
        return mPaths;
    }

    public void setPaths(List<Path> paths) {
        this.mPaths = paths;
    }

    public long getFaster() {
        return mFaster;
    }

    public void setFaster(long faster) {
        this.mFaster = faster;
    }
}
