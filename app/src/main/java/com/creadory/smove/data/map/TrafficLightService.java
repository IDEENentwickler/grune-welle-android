package com.creadory.smove.data.map;

import android.util.Log;

import com.creadory.smove.core.routing.Car2WeightFlagEncoder;
import com.creadory.smove.core.trafficlights.TrafficLightController;
import com.creadory.smove.core.trafficlights.TrafficLightSignal;
import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.data.local.DatabaseHelper;
import com.creadory.smove.data.map.util.TrafficLightSignalWrapper;
import com.creadory.smove.util.LogUtil;
import com.creadory.smove.util.RxEventBus;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.storage.NodeAccess;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.EdgeExplorer;
import com.graphhopper.util.EdgeIterator;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.Helper;
import com.graphhopper.util.shapes.GHPoint;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import gnu.trove.list.TIntList;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Tim on 08.03.2017.
 */

@Singleton
public class TrafficLightService implements DataManager.TrafficLightServiceInt {
    private static final String TAG = "TrafficLightService";

    private final static double SPEED_CONV = 3.6;

    private LogUtil mLogUtil;
    private RxEventBus mRxEventBus;
    private TrafficLightController mTrafficLightController;
    private DatabaseHelper mDatabaseHelper;
    private volatile boolean prepareInProgress = false;
    private DistanceCalc mDistCalc;

    @Inject
    public TrafficLightService(DatabaseHelper databaseHelper, RxEventBus rxEventBus, LogUtil logUtil) {
        mLogUtil = logUtil;
        mDatabaseHelper = databaseHelper;
        mRxEventBus = rxEventBus;
        mDistCalc = Helper.DIST_EARTH;
    }

    @Override
    public boolean isReady() {
        // only return true if already loaded
        if (mTrafficLightController != null)
            return true;

        if (prepareInProgress) {
            mLogUtil.logUser(TAG, "Preparation still in progress");
            return false;
        }
        mLogUtil.logUser(TAG, "Prepare finished but TrafficLightService not ready.");
        return false;
    }

    @Override
    public Single<TrafficLightController> getInitObservable() {
        return Single.create(new SingleOnSubscribe<TrafficLightController>() {
            @Override
            public void subscribe(SingleEmitter<TrafficLightController> emitter) throws Exception {
                prepareInProgress = true;
                try {
                    TrafficLightController tmpTrafficLightController = new TrafficLightController(mDatabaseHelper);
                    tmpTrafficLightController.initTrafficLights();
                    Log.i(TAG, "Finished to initiate TrafficLightController.");
                    mTrafficLightController = tmpTrafficLightController;
                    emitter.onSuccess(mTrafficLightController);
                } catch (Exception e) {
                    Log.i(TAG, "An error happened while initiating TrafficLightController: " + e.getMessage());
                    emitter.onError(e);
                }
                prepareInProgress = false;
                mRxEventBus.post("done");
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public TrafficLightController getTrafficLightController() {
        return mTrafficLightController;
    }

    //go through all pathNodes and get all traffic light signals on path
    @Override
    public List<TrafficLightSignal> getAllTrafficLightSignalsOnPath(LatLongPoint position, TIntList pathNodes, GraphHopper hopper) {
        List<TrafficLightSignal> trafficLightSignals = new ArrayList<TrafficLightSignal>();
        QueryResult queryResult = hopper.getLocationIndex().findClosest(position.lat, position.lon, EdgeFilter.ALL_EDGES);
        int baseNode = queryResult.getClosestEdge().getBaseNode();
        int adjNode = queryResult.getClosestEdge().getAdjNode();

        if (pathNodes.contains(baseNode) || pathNodes.contains(adjNode)) {    //in the middle of path
            int pathIndexBaseNode = pathNodes.indexOf(baseNode);
            int pathIndexAdjNode = pathNodes.indexOf(adjNode);

            int nextIndex;
            int prevNode, nextNode;
            if (pathIndexBaseNode > pathIndexAdjNode) {   //path direction: adjNode->baseNode
                nextIndex = pathIndexBaseNode;
                nextNode = baseNode;
                prevNode = adjNode;
            } else {                                      //path direction: baseNode->adjNode
                nextIndex = pathIndexAdjNode;
                nextNode = adjNode;
                prevNode = baseNode;
            }

            while (true) {
                TrafficLightSignal trafficLightSignal = mTrafficLightController.getTrafficLightSignal(prevNode, nextNode);
                if (trafficLightSignal != null) {
                    trafficLightSignals.add(trafficLightSignal);
                }

                nextIndex++;
                if (nextIndex < pathNodes.size()) {
                    prevNode = nextNode;
                    nextNode = pathNodes.get(nextIndex);
                } else {
                    break;
                }
            }
        }

        return trafficLightSignals;
    }

    //get only next traffic light signal, don't go through all pathNodes
    @Override
    public TrafficLightSignalWrapper getNextTrafficLightSignalOnPath(LatLongPoint position, TIntList pathNodes, GraphHopper hopper) {
        QueryResult queryResult = hopper.getLocationIndex().findClosest(position.lat, position.lon, EdgeFilter.ALL_EDGES);
        if (queryResult.isValid()) {
            GHPoint snappedPosition = queryResult.getSnappedPoint();
            EdgeIteratorState closestEdge = queryResult.getClosestEdge();
            int baseNode = closestEdge.getBaseNode();
            int adjNode = closestEdge.getAdjNode();

            if (pathNodes.contains(baseNode) || pathNodes.contains(adjNode)) {    //in the middle of path
                int pathIndexBaseNode = pathNodes.indexOf(baseNode);
                int pathIndexAdjNode = pathNodes.indexOf(adjNode);

                int nextNodeIndex;
                int prevNode, nextNode;
                if (pathIndexBaseNode > pathIndexAdjNode) {   //path direction: adjNode->baseNode
                    nextNodeIndex = pathIndexBaseNode;
                    nextNode = baseNode;
                    prevNode = adjNode;
                } else {                                      //path direction: baseNode->adjNode
                    nextNodeIndex = pathIndexAdjNode;
                    nextNode = adjNode;
                    prevNode = baseNode;
                }

                NodeAccess nodeAccess = hopper.getGraphHopperStorage().getNodeAccess();
                FlagEncoder flagEncoder = hopper.getEncodingManager().getEncoder(Car2WeightFlagEncoder.NAME);
                EdgeExplorer edgeExplorer = hopper.getGraphHopperStorage().createEdgeExplorer(EdgeFilter.ALL_EDGES);

                double distanceToTls = 0;
                double timeToTls = 0;
                boolean currentMaxSpeedChanged = false;

                //check for first edge
                EdgeIteratorState edge = getEdge(prevNode, nextNode, edgeExplorer);
                if(edge == null){   //no more traffic light on path
                    return null;
                }
                double edgeSpeed = flagEncoder.getSpeed(edge.getFlags());
                double currentMaxSpeed = edgeSpeed;
                double edgeDistance = mDistCalc.calcDist(nodeAccess.getLat(nextNode), nodeAccess.getLon(nextNode),
                        snappedPosition.lat, snappedPosition.lon);
                double distanceWithCurrentMaxSpeed = edgeDistance;

                TrafficLightSignal trafficLightSignal = mTrafficLightController.getTrafficLightSignal(prevNode, nextNode);
                if (trafficLightSignal != null &&
                        edgeDistance > mDistCalc.calcDist(nodeAccess.getLat(nextNode), nodeAccess.getLon(nextNode),
                                trafficLightSignal.getPosition().lat, trafficLightSignal.getPosition().lon)) {   //if valid tls on first edge

                    distanceToTls = mDistCalc.calcDist(snappedPosition.lat, snappedPosition.lon,
                            trafficLightSignal.getPosition().lat, trafficLightSignal.getPosition().lon);
                    distanceWithCurrentMaxSpeed = distanceToTls;
                    timeToTls = getTimeNeeded(distanceToTls, edgeSpeed);

                    return new TrafficLightSignalWrapper(trafficLightSignal, distanceToTls, timeToTls, currentMaxSpeed, distanceWithCurrentMaxSpeed);
                } else {    //go through path (next edges) until first tls is found -> return tls, distanceToTls and timeToTls
                    distanceToTls = edgeDistance;
                    timeToTls = getTimeNeeded(edgeDistance, edgeSpeed);

                    for (int i = nextNodeIndex + 1; i < pathNodes.size(); i++) {
                        prevNode = nextNode;
                        nextNode = pathNodes.get(i);

                        edge = getEdge(prevNode, nextNode, edgeExplorer);
                        if(edge == null){   //no more traffic light on path
                            return null;
                        }
                        edgeSpeed = flagEncoder.getSpeed(edge.getFlags());

                        trafficLightSignal = mTrafficLightController.getTrafficLightSignal(prevNode, nextNode);
                        if (trafficLightSignal != null) {
                            edgeDistance = mDistCalc.calcDist(nodeAccess.getLat(prevNode), nodeAccess.getLon(prevNode),
                                    trafficLightSignal.getPosition().lat, trafficLightSignal.getPosition().lon);

                            distanceToTls += edgeDistance;
                            timeToTls += getTimeNeeded(edgeDistance, edgeSpeed);
                            if (!currentMaxSpeedChanged && currentMaxSpeed == edgeSpeed) {
                                distanceWithCurrentMaxSpeed += distanceToTls;
                            }

                            return new TrafficLightSignalWrapper(trafficLightSignal, distanceToTls, timeToTls, currentMaxSpeed, distanceWithCurrentMaxSpeed);
                        }

                        edgeDistance = edge.getDistance();

                        distanceToTls += edgeDistance;
                        timeToTls += getTimeNeeded(edgeDistance, edgeSpeed);
                        if (!currentMaxSpeedChanged && currentMaxSpeed == edgeSpeed) {
                            distanceWithCurrentMaxSpeed += edgeDistance;
                        } else {
                            currentMaxSpeedChanged = true;
                        }
                    }
                }
            }
        }

        return null;
    }

    private EdgeIteratorState getEdge(int prevNode, int nextNode, EdgeExplorer edgeExplorer) {
        EdgeIterator edgeIterator = edgeExplorer.setBaseNode(prevNode);
        do{
            if (edgeIterator.getAdjNode() == nextNode) {
                return edgeIterator;
            }
        }while (edgeIterator.next());
        return null;
    }

    private double getTimeNeeded(double distance, double speed) {
        return distance / speed * SPEED_CONV;
    }
}
