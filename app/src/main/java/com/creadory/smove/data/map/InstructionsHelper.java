package com.creadory.smove.data.map;

import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.data.map.util.InstructionWrapper;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.Helper;
import com.graphhopper.util.Instruction;
import com.graphhopper.util.InstructionList;
import com.graphhopper.util.PointList;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Tim on 18.03.2017.
 */

@Singleton
public class InstructionsHelper implements DataManager.InstructionsHelperInt{
    private static final String TAG = "PositioningHelper";
    private DistanceCalc mDistCalc;

    @Inject
    public InstructionsHelper() {
        mDistCalc = Helper.DIST_EARTH;
    }

    /**
     * Original in com.graphhopper.util.InstructionList
     *
     * This method is useful for navigation devices to find the next instruction for the specified
     * coordinate (e.g. the current position).
     * <p>
     *
     * @param maxDistance the maximum acceptable distance to the instruction (in meter)
     * @return InstructionWrapper or null if too far away.
     */
    @Override
    public InstructionWrapper find(InstructionList instructionList, double lat, double lon, double maxDistance) {
        // handle special cases
        if (instructionList.getSize() == 0) {
            return null;
        }

        double distanceToNextInstruction = instructionList.get(0).getDistance();
        double distanceToFinish = 0;
        long timeToFinish = 0;

        PointList points = instructionList.get(0).getPoints();
        double prevLat = points.getLatitude(0);
        double prevLon = points.getLongitude(0);
        DistanceCalc distCalc = Helper.DIST_EARTH;
        double foundMinDistance = distCalc.calcNormalizedDist(lat, lon, prevLat, prevLon);
        int foundInstruction = 0;

        // Search the closest edge to the query point
        if (instructionList.getSize() > 1) {
            for (int instructionIndex = 0; instructionIndex < instructionList.getSize(); instructionIndex++) {
                Instruction currInstruction = instructionList.get(instructionIndex);
                points = currInstruction.getPoints();

                boolean instructionWithMinDistance = false;
                double passedDistance = 0;
                for (int pointIndex = 0; pointIndex < points.size(); pointIndex++) {
                    double currLat = points.getLatitude(pointIndex);
                    double currLon = points.getLongitude(pointIndex);

                    if (!(instructionIndex == 0 && pointIndex == 0)) {
                        // calculate the distance from the point to the edge
                        double distance;
                        int index = instructionIndex;
                        if (distCalc.validEdgeDistance(lat, lon, currLat, currLon, prevLat, prevLon)) {
                            distance = distCalc.calcNormalizedEdgeDistance(lat, lon, currLat, currLon, prevLat, prevLon);
                            if (pointIndex > 0) {
                                index++;
                            }
                        } else {
                            distance = distCalc.calcNormalizedDist(lat, lon, currLat, currLon);
                            if (pointIndex > 0) {
                                index++;
                            }
                        }

                        if (distance < foundMinDistance) {
                            foundMinDistance = distance;
                            foundInstruction = index;

                            if(index > instructionIndex) {
                                distanceToNextInstruction = currInstruction.getDistance() - (passedDistance + distCalc.calcDist(prevLat, prevLon, lat, lon));
                                distanceToNextInstruction = distanceToNextInstruction < 0 ? 0 : distanceToNextInstruction;
                                instructionWithMinDistance = true;
                            }
                        }

                        if(pointIndex > 0) {
                            passedDistance += distCalc.calcDist(prevLat, prevLon, currLat, currLon);
                        }
                    }
                    prevLat = currLat;
                    prevLon = currLon;
                }

                if(instructionWithMinDistance){
                    distanceToFinish = distanceToNextInstruction;
                    timeToFinish = (long)((distanceToFinish * currInstruction.getTime()) / currInstruction.getDistance());
                }else{
                    distanceToFinish += currInstruction.getDistance();
                    timeToFinish += currInstruction.getTime();
                }
            }
        }

        if (distCalc.calcDenormalizedDist(foundMinDistance) > maxDistance) {
            return null;
        }

        // special case finish condition
        if (foundInstruction == instructionList.getSize()) {
            foundInstruction--;
        }

        return new InstructionWrapper(instructionList.get(foundInstruction), timeToFinish, distanceToFinish, distanceToNextInstruction, new LatLongPoint(lat, lon));
    }

}
