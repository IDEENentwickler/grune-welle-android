package com.creadory.smove.data.map.util;

import com.creadory.smove.core.util.LatLongPoint;
import com.graphhopper.util.PointList;
import com.graphhopper.util.shapes.GHPoint;

/**
 * Created by Tim on 13.03.2017.
 */

public class PathPosition {
    public final GHPoint matchedPosition;
    public final PointList leftPathPoints;
    public final LatLongPoint prevPathPoint;
    public final float bearing;
    public final double distanceToPath;

    public PathPosition(GHPoint matchedPosition, PointList leftPathPoints, LatLongPoint prevPathPoint, float bearing, double distanceToPath) {
        this.bearing = bearing;
        this.matchedPosition = matchedPosition;
        this.leftPathPoints = leftPathPoints;
        this.prevPathPoint = prevPathPoint;
        this.distanceToPath = distanceToPath;
    }

}
