package com.creadory.smove.data.map.util;

import com.creadory.smove.core.trafficlights.TrafficLightSignal;

/**
 * Created by Tim on 31.03.2017.
 */

public class TrafficLightSignalWrapper {
    public final TrafficLightSignal tls;
    public final double distanceToTls;
    public final double timeToTls;
    public final double currentMaxSpeed;
    public final double distanceWithCurrentMaxSpeed;

    public TrafficLightSignalWrapper(TrafficLightSignal tls, double distanceToTls, double timeToTls, double currentMaxSpeed, double distanceWithCurrentMaxSpeed) {
        this.distanceToTls = distanceToTls;
        this.tls = tls;
        this.timeToTls = timeToTls;
        this.currentMaxSpeed = currentMaxSpeed;
        this.distanceWithCurrentMaxSpeed = distanceWithCurrentMaxSpeed;
    }

    @Override
    public String toString() {
        return "TrafficLightSignalWrapper{" +
                "tls=" + tls +
                ", distanceToTls=" + distanceToTls +
                ", timeToTls=" + timeToTls +
                ", currentMaxSpeed=" + currentMaxSpeed +
                ", distanceWithCurrentMaxSpeed=" + distanceWithCurrentMaxSpeed +
                '}';
    }
}
