package com.creadory.smove.data.map.util;

import java.util.LinkedList;

public class LimitedList<T> extends LinkedList<T> {
    int capacity;

    public LimitedList(int capacity) {
        this.capacity = capacity;
    }

    public boolean addOnlyNew(T element) {
        if (contains(element)) return false;
        return add(element);
    }

    @Override
    public boolean add(T element) {
        if (isFull()) {
            remove(0);
        }
        super.add(element);
        return true;
    }

    public boolean isFull() {
        return size() >= capacity;
    }
}
