package com.creadory.smove.data.map.util;

import com.creadory.smove.core.util.LatLongPoint;
import com.graphhopper.util.Instruction;

/**
 * Created by Tim on 18.03.2017.
 */

public class InstructionWrapper {
    public final Instruction nextInstruction;
    public final double distanceToNextInstruction;
    public final double distanceToFinish;
    public final long timeToFinish;
    public final LatLongPoint userPosition;

    public InstructionWrapper(Instruction nextInstruction, long timeToFinish, double distanceToFinish, double distanceToNextInstruction, LatLongPoint userPosition) {
        this.timeToFinish = timeToFinish;
        this.distanceToFinish = distanceToFinish;
        this.distanceToNextInstruction = distanceToNextInstruction;
        this.nextInstruction = nextInstruction;
        this.userPosition = userPosition;
    }

    @Override
    public String toString() {
        return "InstructionWrapper{" +
                "distanceToFinish=" + distanceToFinish +
                ", nextInstruction=" + nextInstruction +
                ", distanceToNextInstruction=" + distanceToNextInstruction +
                ", timeToFinish=" + timeToFinish +
                ", userPosition=" + userPosition +
                '}';
    }
}
