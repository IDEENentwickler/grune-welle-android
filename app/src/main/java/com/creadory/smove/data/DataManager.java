package com.creadory.smove.data;

import android.location.Location;
import android.util.Log;

import com.creadory.smove.core.routing.TrafficLightAlgoFactoryDecorator;
import com.creadory.smove.core.routing.TrafficLightGraphHopper;
import com.creadory.smove.core.trafficlights.TrafficLightController;
import com.creadory.smove.core.trafficlights.TrafficLightSignal;
import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.core.util.time.ExactTime;
import com.creadory.smove.data.local.PreferencesHelper;
import com.creadory.smove.data.local.StateHelper;
import com.creadory.smove.data.map.InstructionsHelper;
import com.creadory.smove.data.map.LocationService;
import com.creadory.smove.data.map.MapDirectoryHelper;
import com.creadory.smove.data.map.PositioningHelper;
import com.creadory.smove.data.map.RoutingService;
import com.creadory.smove.data.map.TrafficLightService;
import com.creadory.smove.data.map.util.CustomGHResponse;
import com.creadory.smove.data.map.util.InstructionWrapper;
import com.creadory.smove.data.map.util.LocationAndSpeed;
import com.creadory.smove.data.map.util.PathPosition;
import com.creadory.smove.data.map.util.TrafficLightSignalWrapper;
import com.creadory.smove.data.remote.GeocodingService;
import com.creadory.smove.data.remote.NtpTimeService;
import com.creadory.smove.data.remote.TrafficFlowService;
import com.creadory.smove.data.remote.util.NamedLatLong;
import com.graphhopper.GraphHopper;
import com.graphhopper.util.InstructionList;
import com.graphhopper.util.PointList;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.inject.Inject;
import javax.inject.Singleton;

import gnu.trove.list.TIntList;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class DataManager {
    private static final String TAG = "DataManager";

    private final StateHelper mStateHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final LocationService mLocationService;
    private final PositioningHelper mPositioningHelper;
    private final InstructionsHelper mInstructionsHelper;
    private final GeocodingService mGeocodingService;
    private final MapDirectoryHelper mMapDirectoryHelper;
    private final NtpTimeService mNtpTimeService;
    private final RoutingService mRoutingService;
    private final TrafficLightService mTrafficLightService;
    private final TrafficFlowService mTrafficFlowService;

    @Inject
    public DataManager(StateHelper stateHelper,
                       PreferencesHelper preferencesHelper,
                       LocationService locationService,
                       PositioningHelper positioningHelper,
                       InstructionsHelper instructionsHelper,
                       GeocodingService geocodingService,
                       MapDirectoryHelper mapDirectoryHelper,
                       NtpTimeService ntpTimeService,
                       RoutingService routingService,
                       TrafficLightService trafficLightService,
                       TrafficFlowService trafficFlowService) {
        mPreferencesHelper = preferencesHelper;
        mGeocodingService = geocodingService;
        mMapDirectoryHelper = mapDirectoryHelper;
        mLocationService = locationService;
        mPositioningHelper = positioningHelper;
        mInstructionsHelper = instructionsHelper;
        mNtpTimeService = ntpTimeService;
        mRoutingService = routingService;
        mTrafficLightService = trafficLightService;
        mStateHelper = stateHelper;
        mTrafficFlowService = trafficFlowService;

        //initiate RoutingService and TrafficLightService
        Single.zip(mRoutingService.getInitObservable(), mTrafficLightService.getInitObservable(), new BiFunction<GraphHopper, TrafficLightController, String>() {
            @Override
            public String apply(@NonNull GraphHopper graphHopper, @NonNull TrafficLightController trafficLightController) throws Exception {
                //after RoutingService and TrafficLightService are initiated
                try {
                    if (graphHopper instanceof TrafficLightGraphHopper) {
                        TrafficLightGraphHopper trafficLightGraphHopper = ((TrafficLightGraphHopper) graphHopper);

                        //set TrafficLightAlgoFactoryDecorator (to use DIJKSTRA_TRAFFIC_LIGHT and ASTAR_TRAFFIC_LIGHT) and set time
                        trafficLightGraphHopper.addAlgorithmFactoryDecorator(new TrafficLightAlgoFactoryDecorator());
                        trafficLightGraphHopper.setExactTime(mNtpTimeService);

                        //precalculate tls <-> adjNode distances
                        trafficLightController.calculateAllDistancesOfTlsToAdjNode(trafficLightGraphHopper.getGraphHopperStorage().getNodeAccess());

                        //set trafficLightController of trafficLightGraphHopper
                        trafficLightGraphHopper.setTrafficLightController(trafficLightController);

                        return "TrafficLightGraphHopper used";
                    } else {
                        return "GraphHopper used";
                    }
                } catch (Exception e) {
                    return null;
                }
            }
        }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(TAG, "INIT: onSubscribe");
                    }

                    @Override
                    public void onSuccess(String s) {
                        Log.i(TAG, "INIT: onSuccess: " + s);

                        //start traffic flow updates
//                        mTrafficFlowService.startTrafficFlowUpdates();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "INIT: onError: " + e.getMessage());
                    }
                });
    }

    public StateHelperInt getStateHelper() {
        return mStateHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public GeocodingServiceInt getGeocodingService() {
        return mGeocodingService;
    }

    public LocationServiceInt getLocationService() {
        return mLocationService;
    }

    public PositioningHelperInt getPositioningHelper() {
        return mPositioningHelper;
    }

    public InstructionsHelperInt getInstructionsHelper() {
        return mInstructionsHelper;
    }

    public MapDirectoryHelperInt getMapDirectoryHelper() {
        return mMapDirectoryHelper;
    }

    public ExactTime getNtpTimeService() {
        return mNtpTimeService;
    }

    public RoutingServiceInt getRoutingService() {
        return mRoutingService;
    }

    public TrafficLightServiceInt getTrafficLightService() {
        return mTrafficLightService;
    }

    public TrafficFlowServiceInt getTrafficFlowService() {
        return mTrafficFlowService;
    }

    /*****
     * interfaces for services and helpers
     *****/

    public interface StateHelperInt{
        public StateHelper.State getCurrentState();
        public void setCurrentState(StateHelper.State currentState);
    }

    public interface GeocodingServiceInt{
        public NamedLatLong[] searchNearBy(LatLongPoint location, String query) throws IOException;
        public ObservableSource<NamedLatLong[]> searchQueryNearByLocation(final String query, final LatLongPoint location);
    }

    public interface LocationServiceInt{
        public Observable<LocationAndSpeed> observable();
        public Location getLastLocation();
        public double getLastSpeed();
    }

    public interface PositioningHelperInt{
        public PathPosition calcMatchedPositionBasedOnCurrentPath(PointList pathPoints, Location rawLocation, LatLongPoint lastPrevPathPoint);
    }

    public interface InstructionsHelperInt{
        public InstructionWrapper find(InstructionList instructionList, double lat, double lon, double maxDistance);
    }

    public interface MapDirectoryHelperInt{
        public File getAreaFolder();
        public File getMapsFolder();
        public String getCurrentArea();
    }

    public interface RoutingServiceInt{
        public Single<CustomGHResponse> calcRoute(final double fromLat, final double fromLon, final double toLat, final double toLon);
        public boolean isReady();
        public Single<GraphHopper> getInitObservable();
        public GraphHopper getGraphHopper();
        public ReadWriteLock getReadWriteLock();
    }

    public interface TrafficLightServiceInt{
        public boolean isReady();
        public Single<TrafficLightController> getInitObservable();
        public TrafficLightController getTrafficLightController();
        public List<TrafficLightSignal> getAllTrafficLightSignalsOnPath(LatLongPoint position, TIntList pathNodes, GraphHopper hopper);
        public TrafficLightSignalWrapper getNextTrafficLightSignalOnPath(LatLongPoint position, TIntList pathNodes, GraphHopper hopper);
    }

    public interface TrafficFlowServiceInt{
        public void startTrafficFlowUpdates();
    }
}
