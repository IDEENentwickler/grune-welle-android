package com.creadory.smove.data.remote.util;


import com.creadory.smove.core.util.LatLongPoint;

/**
 * This class represents a named location. It will be used as wrapper for geocoding result when user is searching for specific location.
 */
public class NamedLatLong {
    public final LatLongPoint latLong;
    public final String name;
    public final String vicinity;

    public NamedLatLong(LatLongPoint latLong, String name, String vicinity) {
        this.latLong = latLong;
        this.name = name;
        this.vicinity = vicinity;
    }

    @Override
    public String toString() {
        return "NamedLatLong{" +
                "latLong=" + latLong +
                ", name='" + name + '\'' +
                ", vicinity='" + vicinity + '\'' +
                '}';
    }
}
