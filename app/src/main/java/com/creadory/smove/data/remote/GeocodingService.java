package com.creadory.smove.data.remote;

import com.creadory.smove.AppConstants;
import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.data.remote.util.NamedLatLong;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLEncoder;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Searches for place near by location using Here Maps API.
 * Documentation: https://developer.here.com/rest-apis/documentation/places/topics_api/resource-autosuggest.html
 */
@Singleton
public class GeocodingService implements DataManager.GeocodingServiceInt{
    // TODO: Remove .cit in production environment
    private static final String SEARCH_URL = "https://places.cit.api.here.com/places/v1/autosuggest?" +
            "app_id=" + AppConstants.HERE.APP_ID +
            "&app_code=" + AppConstants.HERE.APP_CODE;
    public static final int LIMIT = 5;

    @Inject
    public GeocodingService() {
    }

    @Override
    public ObservableSource<NamedLatLong[]> searchQueryNearByLocation(final String query, final LatLongPoint location) {
        return new ObservableSource<NamedLatLong[]>() {
            @Override
            public void subscribe(Observer<? super NamedLatLong[]> observer) {
                try {
                    observer.onNext(searchNearBy(location, query));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public NamedLatLong[] searchNearBy(LatLongPoint location, String query) throws IOException {
        String url = new StringBuilder(SEARCH_URL)
                .append("&at=").append(URLEncoder.encode(location.lat + "," + location.lon, "UTF-8"))
                .append("&q=").append(URLEncoder.encode(query, "UTF-8"))
                .append("&size=").append(URLEncoder.encode(""+(LIMIT+1), "UTF-8"))
                .append("&result_types=address")
                .append("&tf=plain")
                .toString();

        Request request = new Request.Builder().get().url(url).build();
        Response response = new OkHttpClient().newCall(request).execute();
        switch (response.code()) {
            case 200:
                String body = response.body().string();
                JsonArray results = new JsonParser().parse(body).getAsJsonObject().getAsJsonArray("results");
                Gson gson = new GsonBuilder().registerTypeAdapter(NamedLatLong.class, new NamedLatLongDeserializer()).create();
                return gson.fromJson(results, NamedLatLong[].class);
            default:
                throw new IOException("Expected code 200, but code is: " + response.code());
        }
    }

    private class NamedLatLongDeserializer implements JsonDeserializer<NamedLatLong> {
        public NamedLatLong deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            JsonObject jsonObj = json.getAsJsonObject();

            JsonArray position = jsonObj.getAsJsonArray("position");
            double lat = position.get(0).getAsDouble();
            double lon = position.get(1).getAsDouble();

            return new NamedLatLong(new LatLongPoint(lat, lon), jsonObj.get("title").getAsString(), jsonObj.get("vicinity").getAsString());
        }
    }
}
