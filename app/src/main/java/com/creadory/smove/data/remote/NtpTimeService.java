package com.creadory.smove.data.remote;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.creadory.smove.AppConstants;
import com.creadory.smove.core.util.time.ExactTime;
import com.creadory.smove.data.remote.util.SntpClient;
import com.creadory.smove.data.remote.util.Timing;
import com.creadory.smove.injection.ApplicationContext;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class NtpTimeService implements ExactTime{
    private static final String TAG = "NtpTimeService";
    private static final int DEFAULT_SOCKET_TIMEOUT = 1000; // 1 seconds

    private int mTimeout;
    private Timing mTiming;
    private Context mContext;

    @Inject
    public NtpTimeService(@ApplicationContext Context context) {
        mTimeout = DEFAULT_SOCKET_TIMEOUT;
        mContext = context;

        //fetch ntp time every x minutes, first emittion delayed by x minutes
        Disposable subscription = Observable.interval(AppConstants.TIME.MINUTES_TO_UPDATE_NTP_TIME, TimeUnit.MINUTES, Schedulers.io())
                .timer(10, TimeUnit.SECONDS, Schedulers.io())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        if (hasConnection()) {
                            fetchNtpTime();
                        }
                    }
                });
    }

    @Override
    public Calendar getCurrentTimeCalendar() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getCurrentTimeDate());
        return cal;
    }

    @Override
    public Date getCurrentTimeDate() {
        if (mTiming == null){
            return new Date();
        }
        return new Date(mTiming.getTime());
    }

    @Override
    public boolean hasExactTime() {
        return (mTiming != null);
    }

    /**
     * Get time difference of current time and NTP time.
     * Positive value means that current time is older than NTP time.
     *
     * @return time difference in milliseconds
     */
    private long getTimeDifference() {
        if (mTiming == null){
            return 0;
        }
        return System.currentTimeMillis() - mTiming.getTime();
    }

    private void fetchNtpTime() {
        for (String ntpServer : AppConstants.TIME.NTP_SERVERS) {
            try {
                Log.d(TAG, "Request NTP time from " + ntpServer);
                Timing timing = SntpClient.requestTime(ntpServer, mTimeout);
                if (timing != null) {
                    this.mTiming = timing;
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.mTiming = null;
    }

    private boolean hasConnection() {
        if (mContext == null) {
            return false;
        }

        ConnectivityManager CManager =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo NInfo = CManager.getActiveNetworkInfo();
        if (NInfo != null && NInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return new StringBuilder("<NtpTimeService")
                .append(" ntpTime=").append(getCurrentTimeDate())
                .append(", system=").append(new Date())
                .append(", diff=").append(getTimeDifference())
                .append(">")
                .toString();
    }
}
