package com.creadory.smove.data.local;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.creadory.smove.core.trafficlights.SignalPlan;
import com.creadory.smove.core.trafficlights.TrafficLight;
import com.creadory.smove.core.trafficlights.TrafficLightSignal;
import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.data.local.util.TrafficLightDB;
import com.creadory.smove.injection.ApplicationContext;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.WKBReader;

import javax.inject.Inject;
import javax.inject.Singleton;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.THashMap;
import gnu.trove.map.hash.TIntObjectHashMap;


/**
 * Created by Tim on 05.12.2016.
 */
@Singleton
public class DatabaseHelper {
    private static final String TAG = "DatabaseHelper";

    private WKBReader wkbReader;
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;

    @Inject
    public DatabaseHelper(@ApplicationContext Context context) {
        this.openHelper = new TrafficLightDB(context);
        this.wkbReader = new WKBReader();
    }

    private void openDB() {
        this.database = openHelper.getReadableDatabase();
    }

    private void closeDB(Cursor cursor) {
        if (database != null) {
            this.database.close();
        }
        if (cursor != null) {
            cursor.close();
        }
    }

    public boolean fillWithTrafficLights(THashMap<String, TrafficLight> trafficLights) {
        Cursor cursor = null;
        boolean returnVal = false;

        try {
            openDB();
            cursor = database.rawQuery("SELECT * FROM berlin_tl_position", null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int nameExtension = cursor.getInt(1); //TK
                    String name = cursor.getString(0); //Anwahlnummer
                    Point position = (Point) wkbReader.read(cursor.getBlob(2)); //Position

                    String key = name + (nameExtension == 0 ? "" : "_" + Integer.toString(nameExtension));
                    trafficLights.put(key, new TrafficLight(name, nameExtension, new LatLongPoint(position.getY(), position.getX())));
                } while (cursor.moveToNext());
            }
            returnVal = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDB(cursor);
        }
        return returnVal;
    }

    public boolean fillTrafficLightWithWeeklySchedule(TrafficLight trafficLight, TIntObjectMap<SignalPlan> signalPlans) {
        Cursor cursor = null;
        boolean returnVal = false;

        try {
            openDB();
            cursor = database.rawQuery("SELECT w.Anwahlnummer, w.WeekDay, w.StartTime, w.EndTime, w.SignalPlan " +
                    "FROM berlin_tl_weekschedule w " +
                    "WHERE w.Anwahlnummer = ?", new String[]{trafficLight.getName()});

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String numberString = cursor.getString(4); //SignalPlan
                    int number = numberString.toLowerCase().contains("off") ? -1 : Integer.parseInt(numberString);

                    SignalPlan signalPlan = signalPlans.get(number);
                    if (signalPlan != null) {
                        trafficLight.addWeekDaySchedule(cursor.getString(1)/*WeekDay*/, cursor.getString(2)/*StartTime*/, cursor.getString(3)/*EndTime*/, signalPlan);
                    }
                } while (cursor.moveToNext());
            }
            returnVal = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDB(cursor);
        }
        return returnVal;
    }

    public boolean fillTrafficLightWithTrafficLightSignals(TrafficLight trafficLight) {
        Cursor cursor = null;
        boolean returnVal = false;

        try {
            String nameExtensionFilter = trafficLight.hasNameExtension() ? " AND TK = " + trafficLight.getNameExtension() : "";
            openDB();
            cursor = database.rawQuery("SELECT * FROM berlin_tl_signalgeber_position " +
                    "WHERE Anwahlnummer = ?" + nameExtensionFilter, new String[]{trafficLight.getName()});

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    byte[] positionBytes = cursor.getBlob(3); //Position
                    LatLongPoint position = positionBytes != null ? getLatLongPoint((Point) wkbReader.read(positionBytes)) : trafficLight.getPosition();
                    TrafficLightSignal tlSignal = new TrafficLightSignal(trafficLight, position,
                            cursor.getInt(4)/*IndexNodeOne*/, cursor.getInt(6)/*IndexNodeTwo*/);
                    String signalNames = cursor.getString(2); //Signalgeber
                    if (signalNames != null) {
                        tlSignal.setSignalNames(signalNames);
                    }
                    trafficLight.addTrafficLightSignal(tlSignal);
                } while (cursor.moveToNext());
            }
            returnVal = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDB(cursor);
        }
        return returnVal;
    }

    public TIntObjectMap<SignalPlan> getSignalPlans(String tlName) {
        Cursor cursor = null;
        TIntObjectMap<SignalPlan> signalPlans = new TIntObjectHashMap<SignalPlan>();

        try {
            openDB();
            cursor = database.rawQuery("SELECT * FROM" +
                    "(SELECT DISTINCT w.Anwahlnummer, w.SignalPlan, w.IntervalTime, s.Signalgeber, s.Signals " +
                    "FROM berlin_tl_weekschedule w " +
                    "LEFT JOIN berlin_tl_signalplan s ON (w.Anwahlnummer = s.Anwahlnummer) AND (w.SignalPlan = s.SignalPlan)) " +
                    "WHERE Anwahlnummer = ? ORDER BY \"SignalPlan\";", new String[]{tlName});

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String numberString = cursor.getString(1); //SignalPlan
                    int number = numberString.toLowerCase().contains("off") ? -1 : Integer.parseInt(numberString);

                    SignalPlan signalPlan = signalPlans.get(number);
                    if (signalPlan == null) {
                        int interval = (number == -1) ? 0 : cursor.getInt(2); //IntervalTime
                        signalPlan = new SignalPlan(number, interval);
                        signalPlans.put(number, signalPlan);
                    }

                    if (number != -1) {
                        signalPlan.addSignals(cursor.getString(3)/*Signalgeber*/, cursor.getString(4)/*Signals*/);
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDB(cursor);
        }

//        TIntObjectIterator<SignalPlan> iter = signalPlans.iterator();
//        while(iter.hasNext()){
//            iter.advance();
//            logger.debug(iter.value().toString());
//        }
        return signalPlans;
    }

    private LatLongPoint getLatLongPoint(Point point) {
        return new LatLongPoint(point.getY(), point.getX());
    }
}