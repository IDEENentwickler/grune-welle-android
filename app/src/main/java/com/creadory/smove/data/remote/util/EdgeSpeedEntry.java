package com.creadory.smove.data.remote.util;

/**
 * Created by Tim on 17.11.2016.
 */
public class EdgeSpeedEntry {
    private int a = -1;
    private int b = -1;
    private int s = -1;
    private long f = 0;

    public EdgeSpeedEntry(int adjNode, int baseNode, int speed, long flags) {
        this.a = adjNode;
        this.b = baseNode;
        this.s = speed;
        this.f = flags;
    }

    public int getSpeed(){
        return s;
    }

    public long getFlags() {
        return f;
    }

    public int getAdjNode() {
        return a;
    }

    public int getBaseNode() {
        return b;
    }
}
