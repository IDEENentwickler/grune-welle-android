package com.creadory.smove.data.map.util;

import android.location.Location;

/**
 * Created by Tim on 06.03.2017.
 */

public class LocationAndSpeed {
    public final Location location;
    public final double speed;

    private boolean newRoute = false;

    public LocationAndSpeed(Location location, double speed) {
        this.location = location;
        this.speed = speed;
    }

    public boolean isNewRoute() {
        return newRoute;
    }

    public void setNewRoute(boolean newRoute) {
        this.newRoute = newRoute;
    }

    @Override
    public String toString() {
        return "LocationAndSpeed{" +
                "location=" + location +
                ", speed=" + speed +
                '}';
    }
}
