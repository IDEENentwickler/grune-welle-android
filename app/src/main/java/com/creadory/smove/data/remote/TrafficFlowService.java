package com.creadory.smove.data.remote;

import android.util.Log;

import com.creadory.smove.AppConstants;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.data.map.RoutingService;
import com.creadory.smove.data.remote.util.EdgeSpeedData;
import com.creadory.smove.util.LogUtil;

import java.util.concurrent.locks.ReadWriteLock;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by Tim on 02.04.2017.
 */

@Singleton
public class TrafficFlowService implements DataManager.TrafficFlowServiceInt{
    private static final String TAG = "TrafficFlowService";

    private RoutingService mRoutingService;
    private ReadWriteLock mReadWriteLock;
    private Retrofit mRetrofit;
    private TrafficFlowEndpointInterface mApiService;
    private LogUtil mLogUitl;

    @Inject
    public TrafficFlowService(RoutingService routingService, LogUtil logUtil) {
        mLogUitl = logUtil;
        mRoutingService = routingService;
        mReadWriteLock = routingService.getReadWriteLock();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.TRAFFIC_FLOW.FLOW_SERVER)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mApiService = mRetrofit.create(TrafficFlowEndpointInterface.class);
    }

    @Override
    public void startTrafficFlowUpdates(){
        mApiService.getTrafficFlow()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<EdgeSpeedData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe");
                    }

                    @Override
                    public void onNext(EdgeSpeedData edgeSpeedData) {
                        Log.d(TAG, "onNext: " + edgeSpeedData.size());

                        //write matched traffic flow to graph
                        mReadWriteLock.writeLock().lock();
                        try {
                            if(mRoutingService.isReady()) {
                                Log.d(TAG, "START writing EdgeSpeedData [" + edgeSpeedData.size() + "] ...");
                                edgeSpeedData.writeEdgeDataToGraph(mRoutingService.getGraphHopper().getGraphHopperStorage());
//                                Log.d(TAG, "DONE writing EdgeSpeedData [" + edgeSpeedData.size() + "]");
                                mLogUitl.logUser(TAG, "DONE writing EdgeSpeedData [" + edgeSpeedData.size() + "]");
                            }else{
                                Log.d(TAG, "RoutingService not ready!");
                            }
                        } finally {
                            mReadWriteLock.writeLock().unlock();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.getMessage());
                        mLogUitl.logUser(TAG, "Error: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete");
                    }
                });
    }

    public interface TrafficFlowEndpointInterface {
        @GET("trafficflow")
        Observable<EdgeSpeedData> getTrafficFlow();
    }
}
