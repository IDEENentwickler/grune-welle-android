package com.creadory.smove.data.map;

import android.location.Location;

import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.data.map.util.PathPosition;
import com.creadory.smove.util.MathUtil;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.Helper;
import com.graphhopper.util.PointList;
import com.graphhopper.util.shapes.GHPoint;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Tim on 13.03.2017.
 */

@Singleton
public class PositioningHelper implements DataManager.PositioningHelperInt{
    private static final String TAG = "PositioningHelper";
    private DistanceCalc mDistCalc;

    @Inject
    public PositioningHelper() {
        mDistCalc = Helper.DIST_EARTH;
    }

    @Override
    public PathPosition calcMatchedPositionBasedOnCurrentPath(PointList pathPoints, Location rawLocation, LatLongPoint lastPrevPathPoint) {
        double lat = rawLocation.getLatitude();
        double lon = rawLocation.getLongitude();

        if (pathPoints != null && pathPoints.size() > 1) {    //in the middle of the path
            double prevLat = pathPoints.getLat(0);
            double prevLon = pathPoints.getLon(0);

            //get index of next pathPoint and its distance to rawLocation
            int nextPathPointIndex = 0;
            double minDistance = mDistCalc.calcNormalizedDist(lat, lon, prevLat, prevLon);
            for (int i = 1; i < pathPoints.size(); i++) {
                double currLat = pathPoints.getLat(i);
                double currLon = pathPoints.getLon(i);

                double distance;
                if (mDistCalc.validEdgeDistance(lat, lon, currLat, currLon, prevLat, prevLon)) {
                    distance = mDistCalc.calcNormalizedEdgeDistance(lat, lon, currLat, currLon, prevLat, prevLon);
                } else {
                    distance = mDistCalc.calcNormalizedDist(lat, lon, currLat, currLon);
                }

                if (distance < minDistance) {
                    minDistance = distance;
                    nextPathPointIndex = i;
                }

                prevLat = currLat;
                prevLon = currLon;
            }

            //calc mapped location based on two closest path points
            LatLongPoint prevPathPoint = null;
            double otherLat, otherLon;
            if (nextPathPointIndex > 0) {    //if next PathPoint not the first one -> take prev PathPoint
                otherLat = pathPoints.getLat(nextPathPointIndex - 1);
                otherLon = pathPoints.getLon(nextPathPointIndex - 1);
                prevPathPoint = new LatLongPoint(otherLat, otherLon);
            } else if (lastPrevPathPoint != null) {  //if next PathPoint first one and lastPrevPathPoint available
                otherLat = lastPrevPathPoint.lat;
                otherLon = lastPrevPathPoint.lon;
                prevPathPoint = new LatLongPoint(otherLat, otherLon);
            } else {  //if next PathPoint first one and lastPrevPathPoint not available -> take next PathPoint
                otherLat = pathPoints.getLat(nextPathPointIndex + 1);
                otherLon = pathPoints.getLon(nextPathPointIndex + 1);
            }
            GHPoint matchedPoint = mDistCalc.calcCrossingPointToEdge(lat, lon,
                    pathPoints.getLat(nextPathPointIndex), pathPoints.getLon(nextPathPointIndex), otherLat, otherLon);

            //create new list with left pathPoints
            PointList leftPathPoints = new PointList();
            for (int i = nextPathPointIndex; i < pathPoints.size(); i++) {
                leftPathPoints.add(pathPoints.getLat(i), pathPoints.getLon(i));
            }

            //bearing of current position
            float bearing = getBearing(rawLocation, prevPathPoint, new LatLongPoint(leftPathPoints.getLat(0), leftPathPoints.getLon(0)));

            return new PathPosition(matchedPoint, leftPathPoints, prevPathPoint, bearing, mDistCalc.calcDenormalizedDist(minDistance));
        } else if (pathPoints != null && pathPoints.size() > 0 && lastPrevPathPoint != null) {    //only one PathPoint left to path end
            GHPoint matchedPoint = mDistCalc.calcCrossingPointToEdge(lat, lon,
                    pathPoints.getLat(0), pathPoints.getLon(0), lastPrevPathPoint.lat, lastPrevPathPoint.lon);
            double minDistance = mDistCalc.calcNormalizedEdgeDistance(lat, lon,
                    pathPoints.getLat(0), pathPoints.getLon(0), lastPrevPathPoint.lat, lastPrevPathPoint.lon);
            float bearing = getBearing(rawLocation, lastPrevPathPoint, new LatLongPoint(pathPoints.getLat(0), pathPoints.getLon(0)));

            return new PathPosition(matchedPoint, pathPoints, lastPrevPathPoint, bearing, mDistCalc.calcDenormalizedDist(minDistance));
        } else {  //if no pathPoints available
            return null;
        }
    }

    //bearing between 0 and 360
    private float getBearing(Location rawLocation, LatLongPoint prevPathPoint, LatLongPoint nextPathPoint) {
        if (prevPathPoint != null) {
            return MathUtil.calcBearing(prevPathPoint.lat, prevPathPoint.lon, nextPathPoint.lat, nextPathPoint.lon);
        } else if (rawLocation.hasBearing()) {
            return rawLocation.getBearing();
        } else {
            return MathUtil.calcBearing(rawLocation.getLatitude(), rawLocation.getLongitude(), nextPathPoint.lat, nextPathPoint.lon);
        }
    }

//    public double calcLeftPathDistance(PointList leftPathPoints, GHPoint matchedPosition){
//        if(leftPathPoints.isEmpty()){
//            return 0;
//        }else {
//            double distance = mDistCalc.calcNormalizedDist(matchedPosition.getLat(), matchedPosition.getLon(), leftPathPoints.getLat(0), leftPathPoints.getLon(0));
//            for (int i = 1; i < leftPathPoints.size(); i++) {
//                distance += mDistCalc.calcNormalizedDist(leftPathPoints.getLat(i), leftPathPoints.getLon(i), leftPathPoints.getLat(i-1), leftPathPoints.getLon(i-1));
//            }
//            return mDistCalc.calcDenormalizedDist(distance);
//        }
//    }

}
