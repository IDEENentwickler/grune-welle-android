package com.creadory.smove.data.map;

import android.util.Log;

import com.creadory.smove.AppConstants;
import com.creadory.smove.core.routing.Car2WeightFlagEncoder;
import com.creadory.smove.core.routing.TrafficLightGraphHopper;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.data.map.util.CustomGHResponse;
import com.creadory.smove.util.LogUtil;
import com.graphhopper.GHRequest;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.Path;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.util.Parameters;
import com.graphhopper.util.StopWatch;

import java.io.File;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Tim on 01.03.2017.
 */

@Singleton
public class RoutingService implements DataManager.RoutingServiceInt{
    private static final String TAG = "RoutingService";

    private MapDirectoryHelper mMapDirectoryHelper;
    private LogUtil mLogUtil;
    private ReadWriteLock mReadWriteLock;

    private GraphHopper hopper;
    private volatile boolean prepareInProgress = false;

    @Inject
    public RoutingService(MapDirectoryHelper mapDirectoryHelper, LogUtil logUtil) {
        this.mMapDirectoryHelper = mapDirectoryHelper;
        this.mLogUtil = logUtil;
        this.mReadWriteLock = new ReentrantReadWriteLock();
    }

    @Override
    public Single<CustomGHResponse> calcRoute(final double fromLat, final double fromLon,
                                         final double toLat, final double toLon) {
        if (!isReady()) {
            return null;
        }

        return Single.create(new SingleOnSubscribe<CustomGHResponse>() {
            @Override
            public void subscribe(SingleEmitter<CustomGHResponse> emitter) throws Exception {
                mReadWriteLock.readLock().lock();
                try {
                    StopWatch sw = new StopWatch().start();
                    GHRequest req = new GHRequest(fromLat, fromLon, toLat, toLon)
                            .setAlgorithm(AppConstants.Algorithms.ASTAR_TRAFFIC_LIGHT)
//                            .setAlgorithm(Parameters.Algorithms.DIJKSTRA_BI)
                            .setWeighting("fastest");
                    req.getHints().put(Parameters.Routing.INSTRUCTIONS, "true");

//                    GHResponse resp = hopper.route(req);
                    CustomGHResponse resp = new CustomGHResponse();
                    List<Path> paths = hopper.calcPaths(req, resp);
                    resp.setPaths(paths);

                    float time = sw.stop().getSeconds();
                    Log.d(TAG, "from:" + fromLat + "," + fromLon + " to:" + toLat + ","
                            + toLon + " found path with distance:" + resp.getBest().getDistance()
                            / 1000f + ", nodes:" + resp.getBest().getPoints().getSize() + ", time:"
                            + time + " " + resp.getDebugInfo());
                    emitter.onSuccess(resp);
                } catch (Exception e) {
                    emitter.onError(e);
                } finally {
                    mReadWriteLock.readLock().unlock();
                }
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public boolean isReady() {
        // only return true if already loaded
        if (hopper != null)
            return true;

        if (prepareInProgress) {
            mLogUtil.logUser(TAG, "Preparation still in progress");
            return false;
        }
        mLogUtil.logUser(TAG, "Prepare finished but hopper not ready. This happens when there was an error while loading the files");
        return false;
    }

    @Override
    public Single<GraphHopper> getInitObservable() {
        return Single.create(new SingleOnSubscribe<GraphHopper>() {
            @Override
            public void subscribe(SingleEmitter<GraphHopper> emitter) throws Exception {
                prepareInProgress = true;
                try {
                    TrafficLightGraphHopper tmpHopp = new TrafficLightGraphHopper();
//                        GraphHopper tmpHopp = new GraphHopper();
                    tmpHopp.forDesktop(); //.forMobile(); //if forMobile(=DAType MMAP), changes to graph (e.g. edge.setSpeed) are directly flushed/saved
                    tmpHopp.setCHEnabled(false);

                    tmpHopp.setEncodingManager(new EncodingManager(new Car2WeightFlagEncoder(8, 1, 1)));
                    tmpHopp.load(new File(mMapDirectoryHelper.getMapsFolder(), mMapDirectoryHelper.getCurrentArea()).getAbsolutePath() + "-gh");
                    Log.i(TAG, "found graph " + tmpHopp.getGraphHopperStorage().toString() + ", nodes:" + tmpHopp.getGraphHopperStorage().getNodes());

                    hopper = tmpHopp;
                    Log.i(TAG, "Finished to initiate GraphHopper.");
                    emitter.onSuccess(tmpHopp);
                } catch (Exception e) {
                    Log.i(TAG, "An error happened while initiating GraphHopper: " + e.getMessage());
                    emitter.onError(e);
                }
                prepareInProgress = false;
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public GraphHopper getGraphHopper() {
        return hopper;
    }

    @Override
    public ReadWriteLock getReadWriteLock(){
        return mReadWriteLock;
    }
}
