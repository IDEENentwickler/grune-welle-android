package com.creadory.smove.data.map;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.creadory.smove.data.DataManager;
import com.creadory.smove.data.map.util.LimitedList;
import com.creadory.smove.data.map.util.LocationAndSpeed;
import com.creadory.smove.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


@Singleton
public class LocationService implements DataManager.LocationServiceInt{
    private static final int LAST_POINTS_CACHE_SIZE = 5;
    private static final int MIN_MILLIS = 1000;
    private static final int MIN_METERS = 1;

    private LocationManager mLocationManager;
    private LimitedList<Location> mLastRawLocations;
    private double mLastSpeed = 0;

    private final PublishSubject<LocationAndSpeed> mLocationSubject;

    @Inject
    public LocationService(@ApplicationContext Context context) {
        mLocationSubject = PublishSubject.create();

        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mLastRawLocations = new LimitedList<>(LAST_POINTS_CACHE_SIZE);
        mLastRawLocations.addOnlyNew(getLastLocation());

        mLocationManager.requestLocationUpdates(getBestProvider(), MIN_MILLIS, MIN_METERS, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (isBetterLocation(location, mLastRawLocations.getLast())) {
                    triggerOnLocationChanged(location);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        });
    }

    @Override
    public Observable<LocationAndSpeed> observable() {
        return mLocationSubject;
    }

    @Override
    public Location getLastLocation() {
        Location lastDecentLocation = null;

        if (mLastRawLocations.size() > 0) {
            lastDecentLocation = mLastRawLocations.getLast();
        } else {
            Location locationGPS = getLastGPSLocation();
            Location locationNet = getLastNetworkLocation();

            if (locationNet != null) {
                lastDecentLocation = locationNet;
            }

            if (locationGPS != null && isBetterLocation(locationGPS, locationNet)) {
                lastDecentLocation = locationGPS;
            }
        }

        return lastDecentLocation;
    }

    @Override
    public double getLastSpeed() {
        return mLastSpeed;
    }

    private void triggerOnLocationChanged(Location location) {
        mLastRawLocations.addOnlyNew(location);

        if (location.hasSpeed()) {
            mLastSpeed = location.getSpeed() * 3.6; // m/s * 3.6 = km/h
        }else{
            mLastSpeed = 0;
        }

        mLocationSubject.onNext(new LocationAndSpeed(location, mLastSpeed));
    }

    private Location getLastGPSLocation() {
        return mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    private Location getLastNetworkLocation() {
        return mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     * @param location            The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > 4 * MIN_MILLIS;
        boolean isSignificantlyOlder = timeDelta < -4 * MIN_MILLIS;
        boolean isNewer = timeDelta > 0;

        // If it's been more than 1000 millis since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than 1000 millis older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null && provider2 == null) return true;
        return provider1.equals(provider2);
    }

    private String getBestProvider() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        return mLocationManager.getBestProvider(criteria, true);
    }
}