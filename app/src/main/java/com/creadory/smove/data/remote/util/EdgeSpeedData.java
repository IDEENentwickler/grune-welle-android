package com.creadory.smove.data.remote.util;

import com.creadory.smove.core.routing.Car2WeightFlagEncoder;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.util.EdgeIteratorState;

import java.util.HashMap;

/**
 * Created by Tim on 17.11.2016.
 */
public class EdgeSpeedData extends HashMap<Integer, EdgeSpeedEntry> {

    public EdgeSpeedEntry putEdgeEntry(int edgeId, int adjNode, int baseNode, int speed, long flags){
        return super.put(edgeId, new EdgeSpeedEntry(adjNode, baseNode, speed, flags));
    }

    public void writeEdgeDataToGraph(GraphHopperStorage graphHopperStorage){
        for(Entry<Integer, EdgeSpeedEntry> entry : entrySet()) {
            EdgeIteratorState edge = graphHopperStorage.getEdgeIteratorState(entry.getKey(), entry.getValue().getAdjNode());
            edge.setFlags(entry.getValue().getFlags());
        }
    }

    public void writeEdgeDataToGraph(GraphHopper graphHopper){
        FlagEncoder flagEncoder = graphHopper.getEncodingManager().getEncoder(Car2WeightFlagEncoder.NAME);
        for(Entry<Integer, EdgeSpeedEntry> entry : entrySet()) {
            EdgeIteratorState edge = graphHopper.getGraphHopperStorage().getEdgeIteratorState(entry.getKey(), entry.getValue().getAdjNode());
            long flags = flagEncoder.setSpeed(edge.getFlags(), entry.getValue().getSpeed());
            edge.setFlags(flags);
        }
    }
}
