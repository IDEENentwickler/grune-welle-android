package com.creadory.smove.data.local.util;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by Tim on 05.02.2017.
 */

public class TrafficLightDB extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "berlin_trafficlights_06.02.17.sqlite";
    private static final int DATABASE_VERSION = 1;

    public TrafficLightDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}