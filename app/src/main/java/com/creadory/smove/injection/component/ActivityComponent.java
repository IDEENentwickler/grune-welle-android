package com.creadory.smove.injection.component;

import com.creadory.smove.injection.PerActivity;
import com.creadory.smove.injection.module.ActivityModule;
import com.creadory.smove.ui.main.MainActivity;
import com.creadory.smove.ui.map.MapFragment;

import dagger.Subcomponent;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(MapFragment mapFragment);
}
