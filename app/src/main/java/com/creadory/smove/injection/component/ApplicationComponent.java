package com.creadory.smove.injection.component;

import android.app.Application;
import android.content.Context;

import com.creadory.smove.data.DataManager;
import com.creadory.smove.injection.ApplicationContext;
import com.creadory.smove.injection.module.ApplicationModule;
import com.creadory.smove.util.LogUtil;
import com.creadory.smove.util.RxEventBus;

import javax.inject.Singleton;

import dagger.Component;
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext Context context();
    Application application();
    DataManager dataManager();
    LogUtil logUtil();
    RxEventBus rxEventBus();
}
