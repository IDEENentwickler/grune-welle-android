package com.creadory.smove.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.creadory.smove.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Tim on 01.03.2017.
 */

@Singleton
public class LogUtil {
    private Context context;

    @Inject
    public LogUtil(@ApplicationContext Context context){
        this.context = context;
    }

    public void logUser(String tag, String str) {
        Log.i(tag, str);
        Toast.makeText(context, str, Toast.LENGTH_LONG).show();
    }
}
