package com.creadory.smove.util;

import android.location.Location;

import com.creadory.smove.core.util.LatLongPoint;
import com.graphhopper.util.PointList;
import com.vividsolutions.jts.math.Vector2D;

import org.oscim.core.GeoPoint;

import java.util.TreeMap;

public final class MathUtil {
    private static final int EARTH_RADIUS = 6371000; // meters
    private static final double QUARTERPI = Math.PI / 4.0;

    public static Vector2D rotate(Vector2D vector, double degrees) {
        double rad = Math.toRadians(-degrees);
        double x = (vector.getX() * Math.cos(rad)) - (vector.getY() * Math.sin(rad));
        double y = (vector.getX() * Math.sin(rad)) + (vector.getY() * Math.cos(rad));
        return new Vector2D(x, y);
    }

    public static Vector2D convertLatLonPointToVector2D(LatLongPoint latLongPoint) {
        return convertLatLonPointToVector2D(latLongPoint.lat, latLongPoint.lon);
    }
    public static Vector2D convertLatLonPointToVector2D(double lat, double lon) {
        double longitude = Math.toRadians(lon);
        double latitude = Math.toRadians(lat);

        double x = longitude;
        double y = Math.log(Math.tan(QUARTERPI + 0.5 * latitude));
        return new Vector2D(x, y);
    }

    public static LatLongPoint convertVector2dToLatLongPoint(Vector2D xyPoint) {
        double longitude = xyPoint.getX();
        double latitude = (Math.atan(Math.exp(xyPoint.getY())) - QUARTERPI) / 0.5;

        double lat = Math.toDegrees(latitude);
        double lon = Math.toDegrees(longitude);
        return new LatLongPoint(lat, lon);
    }

    public static Vector2D convertLatLongPointToVector2D(LatLongPoint geoLatLongPoint) {
        return new Vector2D(geoLatLongPoint.lon, geoLatLongPoint.lat);
    }

    public static Vector2D convertLatLongPointsToVector2D(LatLongPoint a, LatLongPoint b) {
        return convertLatLongPointToVector2D(b).subtract(convertLatLongPointToVector2D(a));
    }

    public static GeoPoint convertLatLongPointsToGeoPoint(LatLongPoint latLongPoint) {
        return new GeoPoint(latLongPoint.lat, latLongPoint.lon);
    }

    public static LatLongPoint convertLocToLatLongPoint(Location loc) {
        if (loc == null) return null;
        return new LatLongPoint(loc.getLatitude(), loc.getLongitude());
    }

    public static GeoPoint convertLocToGeoPoint(Location loc) {
        if (loc == null) return null;
        return new GeoPoint(loc.getLatitude(), loc.getLongitude());
    }

    //http://stackoverflow.com/questions/837872/calculate-distance-in-meters-when-you-know-longitude-and-latitude-in-java
    public static double getDistanceFromLatLonInMeters(LatLongPoint from, LatLongPoint to) {
        double dLat = Math.toRadians(to.lat-from.lat);
        double dLng = Math.toRadians(to.lon-from.lon);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(from.lat)) *
                Math.cos(Math.toRadians(to.lat)) * Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return EARTH_RADIUS * c;
    }

    public static double getAngle(Vector2D v1, Vector2D v2){
        return Math.toDegrees(v1.angle(v2));
    }

    public static LatLongPoint getCorrectedMapMatchCoordinate(PointList points, LatLongPoint latLongPoint) {
        LatLongPoint finalPoint = null;
        if (points.size() > 1) {
            //find the two closest points to current GPS point
            TreeMap<Double, LatLongPoint> distancesList = new TreeMap<>();
            for(int i = 0; i < points.size(); i++){
                LatLongPoint tmp = new LatLongPoint(points.getLat(i), points.getLon(i));
                distancesList.put(MathUtil.getDistanceFromLatLonInMeters(tmp, latLongPoint), tmp);
            }
            LatLongPoint closestPoint = distancesList.pollFirstEntry().getValue();

            Vector2D externalPoint = MathUtil.convertLatLonPointToVector2D(latLongPoint);
            Vector2D pOneXY = MathUtil.convertLatLonPointToVector2D(closestPoint);

            boolean firstRun = true;
            boolean pointInInterval = false;
            Vector2D finalPointOnLineXY = null;

            while (!distancesList.isEmpty() && !pointInInterval) {
                LatLongPoint pTwoLatLong = distancesList.pollFirstEntry().getValue();
                Vector2D pTwoXY = MathUtil.convertLatLonPointToVector2D(pTwoLatLong);
                Vector2D pointOnLineXY = calcCrossingPointOnLine(pOneXY, pTwoXY, externalPoint);
                if (firstRun) {
                    firstRun = false;
                    finalPointOnLineXY = pointOnLineXY;
                }

                if (((pOneXY.getX() <= pointOnLineXY.getX() && pointOnLineXY.getX() <= pTwoXY.getX()) || (pOneXY.getX() >= pointOnLineXY.getX() && pointOnLineXY.getX() >= pTwoXY.getX())) &&
                        ((pOneXY.getY() <= pointOnLineXY.getY() && pointOnLineXY.getY() <= pTwoXY.getY()) || (pOneXY.getY() >= pointOnLineXY.getY() && pointOnLineXY.getY() >= pTwoXY.getY()))) {
                    pointInInterval = true;
                    finalPointOnLineXY = pointOnLineXY;
                }
            }

            finalPoint = MathUtil.convertVector2dToLatLongPoint(finalPointOnLineXY);
        }
        return finalPoint;
    }

    //Lotfußpunktverfahren mit laufendem Punkt - line (linePointOne, linePointTwo) and point
    public static Vector2D calcCrossingPointOnLine(Vector2D linePointOne, Vector2D linePointTwo, Vector2D point) {

        double r = (((linePointOne.getX() - point.getX()) * (linePointTwo.getX() - linePointOne.getX())) +
                ((linePointOne.getY() - point.getY()) * (linePointTwo.getY() - linePointOne.getY()))) /
                (-1.0 * (Math.pow(linePointTwo.getX() - linePointOne.getX(), 2) + Math.pow(linePointTwo.getY() - linePointOne.getY(), 2)));

        double latFinal = linePointOne.getX() + (r * (linePointTwo.getX() - linePointOne.getX()));
        double lonFinal = linePointOne.getY() + (r * (linePointTwo.getY() - linePointOne.getY()));

        return new Vector2D(latFinal, lonFinal);
    }

    //http://stackoverflow.com/questions/8123049/calculate-bearing-between-two-locations-lat-long
    public static float calcBearing(double lat1, double lon1, double lat2, double lon2){
        double dLon = (lon2 - lon1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1)*Math.sin(lat2) - Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
        double brng = Math.toDegrees((Math.atan2(y, x)));
        return (float)(360 - ((brng + 360) % 360));
    }
}
