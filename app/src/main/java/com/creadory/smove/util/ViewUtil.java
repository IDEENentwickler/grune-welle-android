package com.creadory.smove.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class ViewUtil {
    private static DecimalFormat mDecimalFormat = new DecimalFormat("0.00");
    private static SimpleDateFormat mStandardDateFormat = new SimpleDateFormat("HH:mm", Locale.GERMANY);
    private static SimpleDateFormat mNtpDateFormat = new SimpleDateFormat("HH:mm:ss:SS", Locale.GERMANY);

    public static float pxToDp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / (densityDpi / 160f);
    }

    public static int dpToPx(int dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }

    public static String getTimeString(long timeInMillis){
        String time = "";
        long timeInSeconds = timeInMillis / 1000;  //in seconds
        if(timeInSeconds >= 60){         //if > 1 minute
            long minutes = timeInSeconds / 60;   //in minutes
            if(timeInSeconds >= 3600) {  //if > 1 hour
                time = minutes / 60 + "h ";     //hours
                time += minutes % 60 + "min";   //and minutes
            }else{
                time = minutes + "min";
            }
        }else{
            time = timeInSeconds + "s";
        }
        return time;
    }

    public static String getDistanceString(double distanceInMeter){
        String distance = "";
        if(distanceInMeter >= 1000){
            distanceInMeter /= 1000;    //show in km
            distance = mDecimalFormat.format(distanceInMeter) + "km";
        }else {
            distance = (int)distanceInMeter + "m";
        }
        return distance;
    }

    public static void setMarginBottom(View v, int bottomPx) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)v.getLayoutParams();
        params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottomPx);
    }

    public static String getStandardDateString(Date date){
        return mStandardDateFormat.format(date);
    }

    public static String getNtpDateString(Date date) {
        return mNtpDateFormat.format(date);
    }
}
