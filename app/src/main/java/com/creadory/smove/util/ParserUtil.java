package com.creadory.smove.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Tim on 26.01.2017.
 */
public class ParserUtil {
    public static Calendar parseDate(String date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.GERMANY);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(date));
            return calendar;
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("ParserUtil", e.toString());
        }
        return null;
    }
}
