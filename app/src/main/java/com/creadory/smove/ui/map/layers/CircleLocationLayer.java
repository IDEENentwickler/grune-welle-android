package com.creadory.smove.ui.map.layers;

import com.creadory.smove.ui.map.renderer.CircleLocationRenderer;

import org.oscim.core.MercatorProjection;
import org.oscim.layers.Layer;
import org.oscim.map.Map;

/**
 * Created by Tim on 07.03.2017.
 */

public class CircleLocationLayer extends Layer {
    public final CircleLocationRenderer locationRenderer;
    private float bearing = 0;

    public CircleLocationLayer(Map map) {
        super(map);

        mRenderer = locationRenderer = new CircleLocationRenderer(mMap, this);
        locationRenderer.setCallback(new CircleLocationRenderer.Callback() {
            @Override
            public float getRotation() {
                return bearing;
            }
        });
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (enabled == isEnabled())
            return;

        super.setEnabled(enabled);

        if (!enabled)
            locationRenderer.animate(false);
    }

    public void setPosition(double latitude, double longitude, double accuracy) {
        double x = MercatorProjection.longitudeToX(longitude);
        double y = MercatorProjection.latitudeToY(latitude);
        double radius = accuracy / MercatorProjection.groundResolution(latitude, 1);
        locationRenderer.setLocation(x, y, radius);
        locationRenderer.animate(true);
    }

    public void setPosition(double latitude, double longitude, double accuracy, float bearing) {
        this.bearing = bearing;
        setPosition(latitude, longitude, accuracy);
    }
}
