package com.creadory.smove.ui.map.layers;

import org.oscim.layers.marker.ItemizedLayer;
import org.oscim.layers.marker.MarkerInterface;
import org.oscim.layers.marker.MarkerRendererFactory;
import org.oscim.layers.marker.MarkerSymbol;
import org.oscim.map.Map;

import java.util.List;

/**
 * Created by Tim on 10.03.2017.
 */

public class CustomItemizedLayer<Item extends MarkerInterface> extends ItemizedLayer {
    public CustomItemizedLayer(Map map, MarkerSymbol defaultMarker) {
        super(map, defaultMarker);
    }

    public CustomItemizedLayer(Map map, List list, MarkerSymbol defaultMarker, OnItemGestureListener listener) {
        super(map, list, defaultMarker, listener);
    }

    public CustomItemizedLayer(Map map, List list, MarkerRendererFactory markerRendererFactory, OnItemGestureListener listener) {
        super(map, list, markerRendererFactory, listener);
    }

    public CustomItemizedLayer(Map map, MarkerRendererFactory markerRendererFactory) {
        super(map, markerRendererFactory);
    }

    public void updateMarker(){
        mMarkerRenderer.update();
    }
}
