package com.creadory.smove.ui.map;

import android.location.Location;

import com.creadory.smove.core.trafficlights.TrafficLightSignal;
import com.creadory.smove.core.trafficlights.TrafficLightSignalState;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.injection.ConfigPersistent;
import com.creadory.smove.ui.base.BasePresenter;
import com.creadory.smove.ui.main.MainPresenter;
import com.creadory.smove.ui.map.util.MarkerItemWrapper;
import com.creadory.smove.util.RxEventBus;

import org.oscim.core.GeoPoint;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Tim on 04.03.2017.
 */

@ConfigPersistent
public class MapPresenter extends BasePresenter<MapMvpView> {
    private static final String TAG = "MapPresenter";

    private MainPresenter mMainPresenter;
    private RxEventBus mRxEventBus;

    @Inject
    public MapPresenter(DataManager dataManager, RxEventBus rxEventBus) {
        super(dataManager);
        mRxEventBus = rxEventBus;
    }

    @Override
    public void attachView(MapMvpView mvpView) {
        super.attachView(mvpView);
        setupUserLocation();
        setupTrafficLights();
    }

    private void setupUserLocation(){
        switch (getDataManager().getStateHelper().getCurrentState()){
            case START:
                getMvpView().followUser(true);
            case SEARCH:
            case SEARCH_RESULT:
            case ROUTER_RESULT:
                getMvpView().switchToStandardView();
                break;
            case NAVIGATION:
                getMvpView().followUser(true);
                getMvpView().switchToNavigationView(getDataManager().getLocationService().getLastLocation(), false);
                break;
        }

        Location lastLocation = getDataManager().getLocationService().getLastLocation();
        if(lastLocation != null){
            getMvpView().updateUserPosition(lastLocation);
        }
    }

    private void setupTrafficLights(){
        if(getDataManager().getTrafficLightService().isReady()){
            getMvpView().addTrafficLightSignalMarkers(getDataManager().getTrafficLightService().getTrafficLightController().getAllTrafficLightSignals());
        }else{
            mRxEventBus.observable().subscribe(new Consumer<Object>() {
                @Override
                public void accept(@NonNull Object o) throws Exception {
                    getMvpView().addTrafficLightSignalMarkers(getDataManager().getTrafficLightService().getTrafficLightController().getAllTrafficLightSignals());
                }
            });
        }
    }

    protected void setupTrafficLightSignalUpdates(final TrafficLightSignal trafficLightSignal, final MarkerItemWrapper markerItemWrapper){
        //update tls in mapView
        TrafficLightSignalState tlsState = getDataManager().getTrafficLightService().getTrafficLightController().
                getTrafficLightSignalState(getDataManager().getNtpTimeService().getCurrentTimeCalendar(), trafficLightSignal);
        getMvpView().updateTrafficLightSignalMarker(trafficLightSignal, markerItemWrapper.getMarkerItem(), tlsState.getState());

        //trigger again when tls state changes
        markerItemWrapper.setDisposable(
                Single.timer(tlsState.getTimeLeft(), TimeUnit.SECONDS, Schedulers.computation()).subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        setupTrafficLightSignalUpdates(trafficLightSignal, markerItemWrapper);
                    }
                }));
    }

    public void setMainPresenter(MainPresenter mainPresenter) {
        this.mMainPresenter = mainPresenter;
    }

    /*****
     * MVP Presenter methods implementation, called from View (MapFragment)
     *****/

    public void onMapLongPress(GeoPoint p){
        if(mMainPresenter != null){
            mMainPresenter.onMapLongPress(p);
        }
    }

    public void onMapDoubleTap(GeoPoint p){
        if(mMainPresenter != null){
            mMainPresenter.onMapDoubleTap(p);
        }
    }

    public void onMapActionMove(){
        if(mMainPresenter != null){
            mMainPresenter.onMapActionMove();
        }
    }
}
