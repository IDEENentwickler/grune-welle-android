package com.creadory.smove.ui.main;

import android.app.SearchManager;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.creadory.smove.R;
import com.creadory.smove.data.remote.util.NamedLatLong;
import com.creadory.smove.ui.base.BaseActivity;
import com.creadory.smove.util.DialogFactory;
import com.creadory.smove.util.LogUtil;
import com.creadory.smove.util.ViewUtil;
import com.graphhopper.util.Instruction;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.creadory.smove.R.id.toolbar;

public class MainActivity extends BaseActivity implements MainMvpView {
    private static final String TAG = "MainActivity";

    @Inject
    LogUtil logUtil;
    @Inject
    MainPresenter mainPresenter;

    @BindView(R.id.app_actionBar) AppBarLayout appActionBar;

    @BindView(R.id.fab_location) FloatingActionButton mFabLocation;

    //toolbar
    @BindView(toolbar) Toolbar mToolbar;
    @BindView(R.id.toolbar_search_result) LinearLayout mToolbarSearchResult;
    @BindView(R.id.text_search_result) TextView mTextToolbarSearchResult;
    @BindView(R.id.back_search_result) ImageButton mToolbarSearchResultBackButton;

    //search result bottom info
    @BindView(R.id.container_searchResultInfo) LinearLayout mContainerSearchResultInfo;
    @BindView(R.id.searchResultInfo_name) TextView mTextNameSearchResultInfo;
    @BindView(R.id.searchResultInfo_vicinity) TextView mTextVicinitySearchResultInfo;
    @BindView(R.id.fab_searchResultInfo) FloatingActionButton mFabSearchResultInfo;

    //route result bottom info
    @BindView(R.id.container_routeResultInfo) RelativeLayout mContainerRouteResultInfo;
    @BindView(R.id.routeResultInfo_loadingPanel) LinearLayout mLoadingPanelRouteResultInfo;
    @BindView(R.id.routeResultInfo_content) LinearLayout mContentRouteResultInfo;
    @BindView(R.id.routeResultInfo_distance) TextView mTextDistanceRouteResultInfo;
    @BindView(R.id.routeResultInfo_time) TextView mTextTimeRouteResultInfo;
    @BindView(R.id.fab_routeResultInfo) FloatingActionButton mFabRouteResultInfo;

    //navigation info
    //current route
    @BindView(R.id.container_navigationInfo) RelativeLayout mContainerNavigationInfo;
    @BindView(R.id.navigationInfo_direction) LinearLayout mDirectionNavigationInfo;
    @BindView(R.id.navigationInfo_directionArrow) ImageView mArrowDirectionNavigationInfo;
    @BindView(R.id.navigationInfo_directionDistance) TextView mTextDistanceDirectionNavigationInfo;
    @BindView(R.id.navigationInfo_directionInfo) TextView mTextInfoDirectionNavigationInfo;
    //faster route
    @BindView(R.id.navigationInfo_directionFaster) LinearLayout mDirectionFasterNavigationInfo;
    @BindView(R.id.navigationInfo_directionFasterArrow) ImageView mArrowDirectionFasterNavigationInfo;
    @BindView(R.id.navigationInfo_directionFasterDistance) TextView mTextDistanceDirectionFasterNavigationInfo;
    @BindView(R.id.navigationInfo_directionFasterInfo) TextView mTextInfoDirectionFasterNavigationInfo;
    @BindView(R.id.navigationInfo_directionFasterTime) TextView mTextTimeDirectionFasterNavigationInfo;
    //bottom info
    @BindView(R.id.navigationInfo_bottom) LinearLayout mBottomNavigationInfo;
    @BindView(R.id.navigationInfo_bottomSpeed) TextView mTextSpeedBottomNavigationInfo;
    @BindView(R.id.navigationInfo_bottomArrival) TextView mTextArrivalBottomNavigationInfo;
    @BindView(R.id.navigationInfo_bottomDistanceTimeLeft) TextView mTextDistanceTimeLeftBottomNavigationInfo;
    //recommended speed
    @BindView(R.id.navigationInfo_recommendedSpeed) RelativeLayout mRecommendedSpeedNavigationInfo;
    @BindView(R.id.navigationInfo_recommendedSpeedImage) ImageView mImageRecommendedSpeedNavigationInfo;
    @BindView(R.id.navigationInfo_recommendedSpeedValue) TextView mTextRecommendedSpeedNavigationInfo;
    @BindView(R.id.navigationInfo_recommendedSpeedUnit) TextView mTextRecommendedUnitNavigationInfo;

    private TextView mNtpTimeTextView;
    private MenuItem mSearchItem;

    private SimpleCursorAdapter mSuggestionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        mToolbarSearchResultBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainPresenter.onSearchResultToolbarBackPressed();
            }
        });

        mFabLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainPresenter.onFaButtonLocationClicked();
            }
        });

        mFabSearchResultInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainPresenter.onFaButtonSearchResultClicked();
            }
        });

        mFabRouteResultInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainPresenter.onFaButtonRouteResultClicked();
            }
        });

        DrawerLayout drawer = ButterKnife.findById(this, R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = ButterKnife.findById(this, R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mNtpTimeTextView = (TextView)navigationView.getHeaderView(0).findViewById(R.id.ntpTimeTextView);

        //start NTP timer (shown in navigationView)
        showNtpTime(0);

        mainPresenter.attachView(this);
    }

    //shows ntp time in main menu
    private void showNtpTime(long delay){
        Single.timer(delay, TimeUnit.MILLISECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        long millisInSecond = 0;

                        if(mainPresenter.getDataManager().getNtpTimeService().hasExactTime()) {
                            Calendar currentTime = mainPresenter.getDataManager().getNtpTimeService().getCurrentTimeCalendar();
                            mNtpTimeTextView.setText(ViewUtil.getNtpDateString(currentTime.getTime()));
                            millisInSecond = currentTime.get(Calendar.MILLISECOND);
                        }

                        //run next time when second is full -> 1000 millis - millisInSecond
                        showNtpTime(1000 - millisInSecond);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_toolbar, menu);

        mSearchItem = menu.findItem(R.id.action_search);
        //set color of search icon
        Drawable drawable = mSearchItem.getIcon();
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        }

        //listens when search is closing
        MenuItemCompat.setOnActionExpandListener(mSearchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return mainPresenter.onSearchActionCollapse();
            }
        });

        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mSuggestionAdapter = new SimpleCursorAdapter(this,
                R.layout.search_dropdown_item_1line,
                null,
                new String[] {"resultName"},
                new int[] {R.id.item_text1},
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        searchView.setSuggestionsAdapter(mSuggestionAdapter);
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                mainPresenter.searchResultSelected(position);
                return true;
            }
        });
        mainPresenter.initSearch(searchView);

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_location:
                mainPresenter.onOne();
                break;
            case R.id.nav_gallery:
                mainPresenter.onTwo();
                break;
            case R.id.nav_slideshow:
                mainPresenter.onThree();
                break;
            case R.id.nav_manage:
                mainPresenter.onFour();
                break;
            case R.id.nav_share:
                break;
            case R.id.nav_send:
                break;
            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        if(!mainPresenter.onBackPressed()){
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mainPresenter.detachView();
    }

    /*****
     * MVP View methods implementation
     *****/

    @Override
    public void showSuggestions(NamedLatLong[] namedLatLongs) {
        MatrixCursor cursor = new MatrixCursor(new String[]{BaseColumns._ID, "resultName", "lat", "long"});
        for (int i = 0; i < namedLatLongs.length; i++) {
            cursor.addRow(new Object[]{i, namedLatLongs[i].name + ", " + namedLatLongs[i].vicinity,
                    namedLatLongs[i].latLong.lat, namedLatLongs[i].latLong.lon});
        }
        mSuggestionAdapter.changeCursor(cursor);
        mSuggestionAdapter.notifyDataSetChanged();
    }

    @Override
    public void hideKeyBoard() {
        ViewUtil.hideKeyboard(this);
    }

    @Override
    public void showError() {
        DialogFactory.createGenericErrorDialog(this, getString(R.string.error_activity_main))
                .show();
    }

    @Override
    public void showFaButtonLocation() {
        mFabLocation.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFaButtonLocation() {
        mFabLocation.setVisibility(View.GONE);
    }

    @Override
    public void showMainToolbar() {
        mToolbarSearchResult.setVisibility(View.GONE);
        mTextToolbarSearchResult.setText("");
        mToolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSearchResultToolbar(String searchResult) {
        mToolbar.setVisibility(View.GONE);
        mTextToolbarSearchResult.setText(searchResult);
        mToolbarSearchResult.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSearchResultInfo(String name, String vicinity){
        mTextNameSearchResultInfo.setText(name);
        mTextVicinitySearchResultInfo.setText(vicinity);
        mContainerSearchResultInfo.setVisibility(View.VISIBLE);
        mFabSearchResultInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSearchResultInfo(){
        mContainerSearchResultInfo.setVisibility(View.GONE);
        mFabSearchResultInfo.setVisibility(View.GONE);
        mTextNameSearchResultInfo.setText("");
        mTextVicinitySearchResultInfo.setText("");
    }

    @Override
    public void showRouteResultInfoLoading(){
        mContainerRouteResultInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRouteResultInfoContent(double distanceInMeter, long timeInMillis){
        mTextTimeRouteResultInfo.setText(ViewUtil.getTimeString(timeInMillis));
        mTextDistanceRouteResultInfo.setText(ViewUtil.getDistanceString(distanceInMeter));

        mLoadingPanelRouteResultInfo.setVisibility(View.GONE);
        mContentRouteResultInfo.setVisibility(View.VISIBLE);
        mFabRouteResultInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRouteResultInfo(){
        mFabRouteResultInfo.setVisibility(View.GONE);
        mContainerRouteResultInfo.setVisibility(View.GONE);
        mLoadingPanelRouteResultInfo.setVisibility(View.VISIBLE);
        mContentRouteResultInfo.setVisibility(View.GONE);
        mTextDistanceRouteResultInfo.setText("");
        mTextTimeRouteResultInfo.setText("");
    }

    @Override
    public void hideAppToolbar(){
        mSearchItem.collapseActionView();
        appActionBar.setVisibility(View.GONE);
    }

    @Override
    public void showAppToolbar(){
        appActionBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNavigationInfoContainer() {
        mContainerNavigationInfo.setVisibility(View.VISIBLE);
        ViewUtil.setMarginBottom(mFabLocation, (int)getResources().getDimension(R.dimen.fab_margin_bottom_navigation));
    }

    @Override
    public void hideNavigationInfoContainer(){
        mContainerNavigationInfo.setVisibility(View.GONE);
        ViewUtil.setMarginBottom(mFabLocation, (int)getResources().getDimension(R.dimen.fab_margin));
    }

    @Override
    public void setNavigationInfoUserSpeed(int speedKmH) {
        String speed = speedKmH + "km/h";
        mTextSpeedBottomNavigationInfo.setText(speed);
    }

    @Override
    public void setNavigationInfoRouteInfo(double distanceInMeter, long timeInMillis, Calendar arrivalTime) {
        mTextArrivalBottomNavigationInfo.setText(ViewUtil.getStandardDateString(arrivalTime.getTime()));
        String distanceTime = ViewUtil.getDistanceString(distanceInMeter) + " \u2022 " + ViewUtil.getTimeString(timeInMillis);
        mTextDistanceTimeLeftBottomNavigationInfo.setText(distanceTime);
    }

    @Override
    public void showNavigationInfoInstructions(int sign, String name, double distance) {
        updateNavigationInfoInstructions(sign, name, distance);
        mDirectionNavigationInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNavigationInfoInstructions() {
        mDirectionNavigationInfo.setVisibility(View.GONE);
    }

    @Override
    public void updateNavigationInfoInstructions(int sign, String name, double distance) {
        mTextDistanceDirectionNavigationInfo.setText(ViewUtil.getDistanceString(distance));
        mTextInfoDirectionNavigationInfo.setText(name);
        updateNavigationInfoInstructionsSign(sign, mArrowDirectionNavigationInfo);
    }

    @Override
    public void showFasterNavigationInfoInstructions(int sign, String name, double distance, long timeMillisDiffFaster) {
        updateFasterNavigationInfoInstructions(sign, name, distance);
        mTextTimeDirectionFasterNavigationInfo.setText(ViewUtil.getTimeString(timeMillisDiffFaster));
        mDirectionFasterNavigationInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFasterNavigationInfoInstructions() {
        mDirectionFasterNavigationInfo.setVisibility(View.GONE);
    }

    @Override
    public void updateFasterNavigationInfoInstructions(int sign, String name, double distance) {
        mTextDistanceDirectionFasterNavigationInfo.setText(ViewUtil.getDistanceString(distance));
        mTextInfoDirectionFasterNavigationInfo.setText(name);
        updateNavigationInfoInstructionsSign(sign, mArrowDirectionFasterNavigationInfo);
    }

    private void updateNavigationInfoInstructionsSign(int sign, ImageView imageView){
        switch (sign) {
            case Instruction.TURN_SHARP_LEFT:
            case Instruction.TURN_LEFT:
                imageView.setImageResource(R.drawable.ic_arrow_left);
                break;
            case Instruction.TURN_SLIGHT_LEFT:
                imageView.setImageResource(R.drawable.ic_arrow_soft_left);
                break;
            case Instruction.TURN_SHARP_RIGHT:
            case Instruction.TURN_RIGHT:
                imageView.setImageResource(R.drawable.ic_arrow_right);
                break;
            case Instruction.TURN_SLIGHT_RIGHT:
                imageView.setImageResource(R.drawable.ic_arrow_soft_right);
                break;
            case Instruction.FINISH:
                imageView.setImageResource(R.drawable.marker_icon_red);
                break;
            default:
                imageView.setImageResource(R.drawable.ic_arrow_straight);
        }
    }

    @Override
    public void showSpeedRecArrowUp(int speed) {
        mImageRecommendedSpeedNavigationInfo.setImageResource(R.drawable.ic_thick_arrow_up);
        mImageRecommendedSpeedNavigationInfo.setBackgroundResource(R.drawable.ic_thick_arrow_up);
        mImageRecommendedSpeedNavigationInfo.setColorFilter(ContextCompat.getColor(this, R.color.colorSpeedUp));
        showSpeedRec(speed);
    }

    @Override
    public void showSpeedRecArrowDown(int speed) {
        mImageRecommendedSpeedNavigationInfo.setImageResource(R.drawable.ic_thick_arrow_down);
        mImageRecommendedSpeedNavigationInfo.setBackgroundResource(R.drawable.ic_thick_arrow_down);
        mImageRecommendedSpeedNavigationInfo.setColorFilter(ContextCompat.getColor(this, R.color.colorSpeedDown));
        showSpeedRec(speed);
    }

    @Override
    public void showSpeedRecCircleGreen(int speed) {
        mImageRecommendedSpeedNavigationInfo.setImageResource(R.drawable.ic_circle);
        mImageRecommendedSpeedNavigationInfo.setBackgroundResource(R.drawable.ic_circle);
        mImageRecommendedSpeedNavigationInfo.setColorFilter(ContextCompat.getColor(this, R.color.colorSpeedOk));
        showSpeedRec(speed);
    }

    private void showSpeedRec(int speed){
        if(speed == Integer.MAX_VALUE){
            mTextRecommendedSpeedNavigationInfo.setText("");
            mTextRecommendedUnitNavigationInfo.setText("");
        }else {
            mTextRecommendedSpeedNavigationInfo.setText(Integer.toString(speed));
            mTextRecommendedUnitNavigationInfo.setText(R.string.speed_unit);
        }
        mRecommendedSpeedNavigationInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSpeedRecCircleRed(int seconds) {
        mImageRecommendedSpeedNavigationInfo.setImageResource(R.drawable.ic_circle);
        mImageRecommendedSpeedNavigationInfo.setBackgroundResource(R.drawable.ic_circle);
        mImageRecommendedSpeedNavigationInfo.setColorFilter(ContextCompat.getColor(this, R.color.colorStop));

        mTextRecommendedSpeedNavigationInfo.setText(Integer.toString(seconds));
        mTextRecommendedUnitNavigationInfo.setText(R.string.time_unit_short);
        mRecommendedSpeedNavigationInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSpeedRec() {
        mRecommendedSpeedNavigationInfo.setVisibility(View.GONE);
    }
}
