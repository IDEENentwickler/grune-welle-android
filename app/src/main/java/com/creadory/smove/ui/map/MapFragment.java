package com.creadory.smove.ui.map;

import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creadory.smove.R;
import com.creadory.smove.core.trafficlights.Phase;
import com.creadory.smove.core.trafficlights.TrafficLightSignal;
import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.ui.base.BaseActivity;
import com.creadory.smove.ui.map.layers.CircleLocationLayer;
import com.creadory.smove.ui.map.layers.CustomItemizedLayer;
import com.creadory.smove.ui.map.layers.NaviLocationLayer;
import com.creadory.smove.ui.map.util.MarkerItemWrapper;
import com.creadory.smove.util.LogUtil;
import com.creadory.smove.util.MathUtil;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.Instruction;
import com.graphhopper.util.InstructionList;

import org.oscim.android.MapView;
import org.oscim.android.canvas.AndroidGraphics;
import org.oscim.backend.canvas.Bitmap;
import org.oscim.core.BoundingBox;
import org.oscim.core.GeoPoint;
import org.oscim.core.MapPosition;
import org.oscim.core.Tag;
import org.oscim.core.Tile;
import org.oscim.event.Event;
import org.oscim.event.Gesture;
import org.oscim.event.GestureListener;
import org.oscim.event.MotionEvent;
import org.oscim.layers.GroupLayer;
import org.oscim.layers.Layer;
import org.oscim.layers.marker.ItemizedLayer;
import org.oscim.layers.marker.MarkerItem;
import org.oscim.layers.marker.MarkerSymbol;
import org.oscim.layers.tile.buildings.BuildingLayer;
import org.oscim.layers.tile.vector.VectorTileLayer;
import org.oscim.layers.tile.vector.labeling.LabelLayer;
import org.oscim.layers.vector.PathLayer;
import org.oscim.layers.vector.geometries.Style;
import org.oscim.map.Map;
import org.oscim.theme.RenderTheme;
import org.oscim.theme.VtmThemes;
import org.oscim.theme.rule.Rule;
import org.oscim.theme.styles.RenderStyle;
import org.oscim.theme.styles.SymbolStyle;
import org.oscim.tiling.source.mapfile.MapFileTileSource;
import org.oscim.utils.Easing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.TIntObjectMap;

import static com.creadory.smove.R.id.mapView;

/**
 * Created by Tim on 02.03.2017.
 */

public class MapFragment extends Fragment implements MapMvpView{
    private static final String TAG = "MapFragment";

    @Inject MapPresenter mapPresenter;
    @Inject LogUtil logUtil;

    private View mContentView;
    private MapView mMapView;

    //layer
    private ItemizedLayer<MarkerItem> mTargetItemizedLayer;
    private CustomItemizedLayer<MarkerItem> mTrafficLightsItemizedLayer;
    private ItemizedLayer<MarkerItem> mInstructionsLayer;
    private PathLayer mAlternativePathLayer;
    private PathLayer mPathLayer;
    private CircleLocationLayer mCircleLocationLayer;
    private NaviLocationLayer mNaviLocationLayer;

    //Marker
    private MarkerSymbol mTlOffMarkerSymbol;
    private MarkerSymbol mTlNoneMarkerSymbol;
    private MarkerSymbol mTlGreenMarkerSymbol;
    private MarkerSymbol mTlRedMarkerSymbol;

    //holds only trafficLightSignals with observable attached
    private List<MarkerItemWrapper> trafficLightSignalMarkers;

    //should map should follow user
    private boolean mFollowUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = super.onCreateView(inflater, container, savedInstanceState);
        if (mContentView == null) {
            mContentView = inflater.inflate(R.layout.fragment_map, container, false);
        }

       ((BaseActivity)getActivity()).activityComponent().inject(this);
        loadMapView(mContentView);

        mTlOffMarkerSymbol = createMarkerSymbol(R.drawable.ic_traffic_light_template_bg_white, MarkerSymbol.HotspotPlace.CENTER);
        mTlNoneMarkerSymbol = createMarkerSymbol(R.drawable.ic_traffic_light_none_bg_white, MarkerSymbol.HotspotPlace.CENTER);
        mTlGreenMarkerSymbol = createMarkerSymbol(R.drawable.ic_traffic_light_green_bg_white, MarkerSymbol.HotspotPlace.CENTER);
        mTlRedMarkerSymbol = createMarkerSymbol(R.drawable.ic_traffic_light_red_bg_white, MarkerSymbol.HotspotPlace.CENTER);

        trafficLightSignalMarkers = new ArrayList<MarkerItemWrapper>();

        mapPresenter.attachView(this);
        return mContentView;
    }

    private void loadMapView(View parent) {
        logUtil.logUser(TAG, "loading map");

        Tile.SIZE = Tile.calculateTileSize(getResources().getDisplayMetrics().scaledDensity);
        mMapView = (MapView) parent.findViewById(mapView);

        //Gesture Recognizer
        mMapView.map().layers().add(new MainMapGestureReceiver(mMapView.map()));

        // Map file source
        MapFileTileSource tileSource = new MapFileTileSource();
        tileSource.setMapFile(new File(mapPresenter.getDataManager().getMapDirectoryHelper().getAreaFolder(),
                mapPresenter.getDataManager().getMapDirectoryHelper().getCurrentArea() + ".map").getAbsolutePath());
        VectorTileLayer l = mMapView.map().setBaseMap(tileSource);
        mMapView.map().setTheme(VtmThemes.DEFAULT);

        //update map style -> don't show osm traffic lights
        RenderTheme t = (RenderTheme) l.getTheme();
        t.traverseRules(new Rule.RuleVisitor(){
            private final SymbolStyle.SymbolBuilder<?> symbolBuilder = SymbolStyle.builder();
            @Override
            public void apply(Rule r) {
                if(r.matchesTags(new Tag[]{new Tag("highway", "traffic_signals")})){
                    for (RenderStyle style : r.styles) {
                        if (style instanceof SymbolStyle) {
                            SymbolStyle s = (SymbolStyle) style;
                            s.set(symbolBuilder.build());
                        }
                    }
                }
                super.apply(r);
            }
        });
        t.updateStyles();

        //Base Layer
        GroupLayer groupLayer = new GroupLayer(mMapView.map());
        groupLayer.layers.add(new BuildingLayer(mMapView.map(), l));
        groupLayer.layers.add(new LabelLayer(mMapView.map(), l));
        mMapView.map().layers().add(groupLayer);

        //Alternative Path Layer
        mAlternativePathLayer = new PathLayer(mMapView.map(), Style.builder()
                .generalization(Style.GENERALIZATION_SMALL)
                .strokeColor(0xf100c90a)
                .strokeWidth(5 * getResources().getDisplayMetrics().density)
                .build());
        mMapView.map().layers().add(mAlternativePathLayer);

        //Current Path Layer
        mPathLayer = new PathLayer(mMapView.map(), Style.builder()
                .generalization(Style.GENERALIZATION_SMALL)
                .strokeColor(0xf100bfe1)
                .strokeWidth(5 * getResources().getDisplayMetrics().density)
                .build());
        mMapView.map().layers().add(mPathLayer);

        //Location Layer - point
        mCircleLocationLayer = new CircleLocationLayer(mMapView.map());
        mCircleLocationLayer.setEnabled(false);
        mMapView.map().layers().add(mCircleLocationLayer);

        //Location Layer - arrow
        mNaviLocationLayer = new NaviLocationLayer(mMapView.map(), 4f);
        mNaviLocationLayer.setEnabled(false);
        mMapView.map().layers().add(mNaviLocationLayer);

        //Instructions Layer
        mInstructionsLayer = new ItemizedLayer<>(mMapView.map(), (MarkerSymbol) null);
        mMapView.map().layers().add(mInstructionsLayer);

        //Traffic Lights Layer
        mTrafficLightsItemizedLayer = new CustomItemizedLayer<MarkerItem>(mMapView.map(), mTlNoneMarkerSymbol);
        mMapView.map().layers().add(mTrafficLightsItemizedLayer);

        //Target Layer
        mTargetItemizedLayer = new ItemizedLayer<>(mMapView.map(), (MarkerSymbol) null);
        mMapView.map().layers().add(mTargetItemizedLayer);

        //Map position
        Location lastLocation = mapPresenter.getDataManager().getLocationService().getLastLocation();
        if(lastLocation != null){   //set to last known user location
            mMapView.map().setMapPosition(lastLocation.getLatitude(), lastLocation.getLongitude(), 1 << 15);
        }else{  //set to map center
            GeoPoint mapCenter = tileSource.getMapInfo().boundingBox.getCenterPoint();
            mMapView.map().setMapPosition(mapCenter.getLatitude(), mapCenter.getLongitude(), 1 << 10);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mapPresenter.detachView();
        if(mMapView != null){
            // Cleanup VTM
            mMapView.map().destroy();
            mMapView = null;
        }
        disposeTrafficLightSignalObservers();
    }

    private class MainMapGestureReceiver extends Layer implements GestureListener, Map.InputListener {
        public MainMapGestureReceiver(Map map) {
            super(map);
        }

        @Override
        public boolean onGesture(Gesture g, MotionEvent e) {
            if (g instanceof Gesture.DoubleTap) {
                GeoPoint p = mMap.viewport().fromScreenPoint(e.getX(), e.getY());
                mapPresenter.onMapDoubleTap(p);
            }else if(g instanceof Gesture.LongPress){
                GeoPoint p = mMap.viewport().fromScreenPoint(e.getX(), e.getY());
                mapPresenter.onMapLongPress(p);
            }
            return false;
        }

        @Override
        public void onInputEvent(Event e, MotionEvent motionEvent) {
            if(motionEvent.getAction() == MotionEvent.ACTION_MOVE){
                mapPresenter.onMapActionMove();
            }
        }
    }

    private MarkerItem createMarkerItem(GeoPoint p, int resource, MarkerSymbol.HotspotPlace hotspotPlace) {
        MarkerItem markerItem = new MarkerItem("", "", p);
        markerItem.setMarker(createMarkerSymbol(resource, hotspotPlace));
        return markerItem;
    }

    private MarkerItem createMarkerItem(GeoPoint p, MarkerSymbol markerSymbol) {
        MarkerItem markerItem = new MarkerItem("", "", p);
        markerItem.setMarker(markerSymbol);
        return markerItem;
    }

    private MarkerSymbol createMarkerSymbol(int resource, MarkerSymbol.HotspotPlace hotspotPlace){
        Drawable drawable = ContextCompat.getDrawable(getActivity(),resource);
        Bitmap bitmap = AndroidGraphics.drawableToBitmap(drawable);
        return new MarkerSymbol(bitmap, hotspotPlace);  //(bitmap, 0.5f, 1);
    }

    private void setMapPosition(MapPosition mapPosition, boolean animate, long duration, Easing.Type easType){
        if(animate) {
            if(mMapView.map().animator().isActive()){
                MapPosition currentAnimPos = mMapView.map().animator().getEndPosition();
                mapPosition.setTilt(currentAnimPos.getTilt());
            }
            mMapView.map().animator().animateTo(duration, mapPosition, easType);
        }else{
            mMapView.map().setMapPosition(mapPosition);
        }
    }

    /*****
     * MVP View methods implementation
     *****/

    @Override
    public void addTrafficLightSignalMarkers(TIntObjectMap<TIntObjectMap<TrafficLightSignal>> trafficLightSignals) {
        TIntObjectIterator<TIntObjectMap<TrafficLightSignal>> iter = trafficLightSignals.iterator();
        while(iter.hasNext()){
            iter.advance();
            TIntObjectIterator<TrafficLightSignal> iterSecondLevel = iter.value().iterator();
            while(iterSecondLevel.hasNext()) {
                iterSecondLevel.advance();
                addTrafficLightSignalMarker(iterSecondLevel.value());
            }
        }
    }

    @Override
    public void addTrafficLightSignalMarkers(List<TrafficLightSignal> trafficLightSignals) {
        for(TrafficLightSignal tls : trafficLightSignals){
            addTrafficLightSignalMarker(tls);
        }
    }

    @Override
    public void addTrafficLightSignalMarker(TrafficLightSignal tls){
        if(tls.getTrafficLight().hasWeeklySchedule()){
            MarkerItemWrapper markerItemWrapper = new MarkerItemWrapper(createMarkerItem(MathUtil.convertLatLongPointsToGeoPoint(tls.getPosition()), mTlNoneMarkerSymbol));
            trafficLightSignalMarkers.add(markerItemWrapper);
            mapPresenter.setupTrafficLightSignalUpdates(tls, markerItemWrapper);
            mTrafficLightsItemizedLayer.addItem(markerItemWrapper.getMarkerItem());
        }else{
            mTrafficLightsItemizedLayer.addItem(createMarkerItem(MathUtil.convertLatLongPointsToGeoPoint(tls.getPosition()), mTlNoneMarkerSymbol));
        }
    }

    @Override
    public void disposeTrafficLightSignalObservers(){
        Log.d(TAG, "disposeTrafficLightSignalObservers");
        for(MarkerItemWrapper item : trafficLightSignalMarkers){
            item.dispose();
        }
    }

    @Override
    public void removeAllTrafficLightSignalMarkers(){
        Log.d(TAG, "removeAllTrafficLightSignalMarkers");
        disposeTrafficLightSignalObservers();
        mTrafficLightsItemizedLayer.removeAllItems();
        mMapView.map().updateMap(true);
    }

    @Override
    public void updateTrafficLightSignalMarker(TrafficLightSignal trafficLightSignal, MarkerItem markerItem, Phase.State phaseState) {
        switch (phaseState) {
            case GREEN:
                markerItem.setMarker(mTlGreenMarkerSymbol);
                break;
            case NOT_GREEN:
                markerItem.setMarker(mTlRedMarkerSymbol);
                break;
            case OFF:
                markerItem.setMarker(mTlOffMarkerSymbol);
                break;
            default:
                markerItem.setMarker(mTlNoneMarkerSymbol);
        }
        mTrafficLightsItemizedLayer.updateMarker();
        mMapView.map().updateMap(true);
    }

    @Override
    public void setSearchResultMarker(LatLongPoint point) {
        mTargetItemizedLayer.removeAllItems();
        mTargetItemizedLayer.addItem(createMarkerItem(new GeoPoint(point.lat, point.lon), R.drawable.marker_icon_red, MarkerSymbol.HotspotPlace.BOTTOM_CENTER));
        mMapView.map().updateMap(true);

        //center map to geoPoint
        mMapView.map().setMapPosition(point.lat, point.lon, 1 << 15);
    }

    @Override
    public void hideSearchResultMarker(){
        mTargetItemizedLayer.removeAllItems();
        mMapView.map().updateMap(true);
    }

    @Override
    public void showPath(PathWrapper response){
        mPathLayer.clearPath();
        List<GeoPoint> geoPoints = new ArrayList<>();
        for (int i = 0; i < response.getPoints().getSize(); i++) {
            geoPoints.add(new GeoPoint(response.getPoints().getLatitude(i), response.getPoints().getLongitude(i)));
        }
        mPathLayer.setPoints(geoPoints);
        mMapView.map().updateMap(true);
    }

    @Override
    public void removePath(){
        mPathLayer.clearPath();
        removeInstructions();
        mMapView.map().updateMap(true);
    }

    @Override
    public void showAlternativePath(PathWrapper response) {
        mAlternativePathLayer.clearPath();
        List<GeoPoint> geoPoints = new ArrayList<>();
        for (int i = 0; i < response.getPoints().getSize(); i++) {
            geoPoints.add(new GeoPoint(response.getPoints().getLatitude(i), response.getPoints().getLongitude(i)));
        }
        mAlternativePathLayer.setPoints(geoPoints);
        mMapView.map().updateMap(true);
    }

    @Override
    public void removeAlternativePath() {
        mAlternativePathLayer.clearPath();
        mMapView.map().updateMap(true);
    }

    @Override
    public void updateUserPosition(Location location){
        if(location != null) {
            if (mCircleLocationLayer.isEnabled()) {
                mCircleLocationLayer.setPosition(location.getLatitude(), location.getLongitude(), location.getAccuracy(), location.getBearing());
                mCircleLocationLayer.setEnabled(true);
            } else if (mNaviLocationLayer.isEnabled()) {
                mNaviLocationLayer.setPosition(location.getLatitude(), location.getLongitude(), location.getBearing());
            }

            if (mFollowUser) {
                MapPosition mapPosition = mMapView.map().getMapPosition();
                mapPosition.setPosition(location.getLatitude(), location.getLongitude());
                if (mNaviLocationLayer.isEnabled()) {
                    mapPosition.setBearing(-location.getBearing());
                    mapPosition.setTilt(70);
                }
                setMapPosition(mapPosition, true, 500, Easing.Type.LINEAR);
            }

            mMapView.map().updateMap(true);
        }
    }

    @Override
    public void followUser(boolean enable){
        mFollowUser = enable;
    }

    @Override
    public void setMapPosition(Location location, boolean animate){
        if(location != null) {
            MapPosition mapPosition = mMapView.map().getMapPosition();
            mapPosition.setPosition(location.getLatitude(), location.getLongitude());
            mapPosition.setZoomLevel(15);
            mapPosition.setTilt(0);
            mapPosition.setBearing(0);
            setMapPosition(mapPosition, animate, 500, Easing.Type.SINE_INOUT);
        }
    }

    @Override
    public void showOverview(Location location, LatLongPoint point, boolean animate){
        if(location != null && point != null){
            MapPosition mapPosition = new MapPosition();
            BoundingBox bb = new BoundingBox(
                    Math.min(location.getLatitude(), point.lat),
                    Math.min(location.getLongitude(), point.lon),
                    Math.max(location.getLatitude(), point.lat),
                    Math.max(location.getLongitude(), point.lon));

            mapPosition.setByBoundingBox(bb, mMapView.getWidth(), mMapView.getHeight());
            mapPosition.setTilt(0);
            mapPosition.setBearing(0);
            mapPosition.setScale(mapPosition.getScale() * 0.5);
            setMapPosition(mapPosition, animate, 500, Easing.Type.SINE_INOUT);
        }
    }

    @Override
    public void switchToStandardView() {
        mMapView.map().viewport().setMapScreenCenter(0.0f);
        mNaviLocationLayer.setEnabled(false);
        mCircleLocationLayer.setEnabled(true);
        mMapView.map().updateMap(true);
    }

    @Override
    public void switchToNavigationView(Location location, boolean animate) {
        mMapView.map().viewport().setMapScreenCenter(0.5f);
        mCircleLocationLayer.setEnabled(false);
        mNaviLocationLayer.setEnabled(true);
        followUser(true);

        if(location != null) {
            mNaviLocationLayer.setPosition(location.getLatitude(), location.getLongitude(), location.getBearing());
            MapPosition mapPosition = new MapPosition();
            mapPosition.setTilt(70);
            mapPosition.setZoomLevel(18);
            mapPosition.setBearing(-location.getBearing());
            mapPosition.setPosition(location.getLatitude(), location.getLongitude());
            setMapPosition(mapPosition, animate, 2000, Easing.Type.SINE_INOUT);
        }else{
            mMapView.map().updateMap(true);
        }
    }

    @Override
    public void showInstructions(InstructionList instructions) {
        for(int i = 1; i < instructions.getSize() - 1; i++) {
            Instruction instruction = instructions.get(i);
            int resource;
            switch (instruction.getSign()) {
                case Instruction.TURN_SHARP_LEFT:
                case Instruction.TURN_LEFT:
                    resource = R.drawable.ic_arrow_left;
                    break;
                case Instruction.TURN_SLIGHT_LEFT:
                    resource = R.drawable.ic_arrow_soft_left;
                    break;
                case Instruction.TURN_SHARP_RIGHT:
                case Instruction.TURN_RIGHT:
                    resource = R.drawable.ic_arrow_right;
                    break;
                case Instruction.TURN_SLIGHT_RIGHT:
                    resource = R.drawable.ic_arrow_soft_right;
                    break;
                default:
                    resource = R.drawable.ic_arrow_straight;
            }
            MarkerSymbol markerSymbol = createMarkerSymbol(resource, MarkerSymbol.HotspotPlace.CENTER);
            mInstructionsLayer.addItem(createMarkerItem(new GeoPoint(instruction.getPoints().getLat(0), instruction.getPoints().getLon(0)), markerSymbol));
        }
        mMapView.map().updateMap(true);
    }

    @Override
    public void removeInstructions(){
        mInstructionsLayer.removeAllItems();
        mMapView.map().updateMap(true);
    }
}
