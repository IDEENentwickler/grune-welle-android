package com.creadory.smove.ui.main;

import com.creadory.smove.ui.base.MvpView;
import com.creadory.smove.data.remote.util.NamedLatLong;

import java.util.Calendar;


public interface MainMvpView extends MvpView {

    void showSuggestions(NamedLatLong[] namedLatLongs);

    void hideKeyBoard();

    void showError();

    public void showFaButtonLocation();

    public void hideFaButtonLocation();

    public void showMainToolbar();

    public void showSearchResultToolbar(String searchResult);

    public void showSearchResultInfo(String name, String vicinity);

    public void hideSearchResultInfo();

    public void showRouteResultInfoLoading();

    public void showRouteResultInfoContent(double distanceInMeter, long timeInMillis);

    public void hideRouteResultInfo();

    public void hideAppToolbar();

    public void showAppToolbar();

    public void showNavigationInfoContainer();

    public void hideNavigationInfoContainer();

    public void setNavigationInfoUserSpeed(int speedKmH);

    public void setNavigationInfoRouteInfo(double distanceInMeter, long timeInMillis, Calendar arrivalTime);

    public void showNavigationInfoInstructions(int sign, String name, double distance);

    public void hideNavigationInfoInstructions();

    public void updateNavigationInfoInstructions(int sign, String name, double distance);

    public void showFasterNavigationInfoInstructions(int sign, String name, double distance, long timeMillisDiffFaster);

    public void hideFasterNavigationInfoInstructions();

    public void updateFasterNavigationInfoInstructions(int sign, String name, double distance);

    public void showSpeedRecArrowUp(int speed);

    public void showSpeedRecArrowDown(int speed);

    public void showSpeedRecCircleGreen(int speed);

    public void showSpeedRecCircleRed(int seconds);

    public void hideSpeedRec();
}
