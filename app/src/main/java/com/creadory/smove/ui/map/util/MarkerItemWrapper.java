package com.creadory.smove.ui.map.util;

import com.creadory.smove.util.RxUtil;

import org.oscim.layers.marker.MarkerItem;

import io.reactivex.disposables.Disposable;

/**
 * Created by Tim on 11.03.2017.
 */

public class MarkerItemWrapper {
    private MarkerItem mMarkerItem;
    private Disposable mDisposable;

    public MarkerItemWrapper(MarkerItem markerItem) {
        this(markerItem, null);
    }

    public MarkerItemWrapper(MarkerItem markerItem, Disposable disposable) {
        this.mDisposable = disposable;
        this.mMarkerItem = markerItem;
    }

    public void setDisposable(Disposable mDisposable) {
        this.mDisposable = mDisposable;
    }

    public MarkerItem getMarkerItem() {
        return mMarkerItem;
    }

    public void dispose(){
        RxUtil.dispose(mDisposable);
    }
}
