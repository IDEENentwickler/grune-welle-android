package com.creadory.smove.ui.main.util;

/**
 * Created by Tim on 02.04.2017.
 */

public class SpeedRecommendation {
    public enum HINT {
        UP, DOWN, OK, NO
    }

    private int displaySpeed;
    private HINT hint;
    private int waitingTime;

    public SpeedRecommendation(){
        displaySpeed = -1;
        waitingTime = -1;
        hint = HINT.NO;
    }

    public boolean hasSpeed(){
        return displaySpeed != -1;
    }

    public boolean hasWaitingTime(){
        return waitingTime != -1;
    }

    public int getDisplaySpeed() {
        return displaySpeed;
    }

    public void setDisplaySpeed(int displaySpeed) {
        this.displaySpeed = displaySpeed;
    }

    public HINT getHint() {
        return hint;
    }

    public void setHint(HINT hint) {
        this.hint = hint;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }
}
