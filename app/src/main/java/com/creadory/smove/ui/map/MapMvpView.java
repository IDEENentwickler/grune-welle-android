package com.creadory.smove.ui.map;

import android.location.Location;

import com.creadory.smove.core.trafficlights.Phase;
import com.creadory.smove.core.trafficlights.TrafficLightSignal;
import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.ui.base.MvpView;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.InstructionList;

import org.oscim.layers.marker.MarkerItem;

import java.util.List;

import gnu.trove.map.TIntObjectMap;

/**
 * Created by Tim on 02.03.2017.
 */

public interface MapMvpView extends MvpView {

    public void addTrafficLightSignalMarker(TrafficLightSignal tls);

    public void addTrafficLightSignalMarkers(List<TrafficLightSignal> trafficLightSignals);

    public void addTrafficLightSignalMarkers(TIntObjectMap<TIntObjectMap<TrafficLightSignal>> trafficLightSignals);

    public void updateTrafficLightSignalMarker(TrafficLightSignal trafficLightSignal, MarkerItem markerItem, Phase.State phaseState);

    public void removeAllTrafficLightSignalMarkers();

    public void disposeTrafficLightSignalObservers();

    public void setSearchResultMarker(LatLongPoint point);

    public void hideSearchResultMarker();

    public void showPath(PathWrapper response);

    public void removePath();

    public void showAlternativePath(PathWrapper response);

    public void removeAlternativePath();

    public void updateUserPosition(Location location);

    public void switchToStandardView();

    public void switchToNavigationView(Location location, boolean animate);

    public void followUser(boolean enable);

    public void setMapPosition(Location location, boolean animate);

    public void showOverview(Location location, LatLongPoint point, boolean animate);

    public void showInstructions(InstructionList instructions);

    public void removeInstructions();
}
