package com.creadory.smove.ui.main;

import android.content.Context;
import android.location.Location;
import android.os.SystemClock;
import android.support.v7.widget.SearchView;
import android.util.Log;

import com.creadory.smove.core.trafficlights.TimeRange;
import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.data.DataManager;
import com.creadory.smove.data.local.StateHelper;
import com.creadory.smove.data.map.util.CustomGHResponse;
import com.creadory.smove.data.map.util.InstructionWrapper;
import com.creadory.smove.data.map.util.LocationAndSpeed;
import com.creadory.smove.data.map.util.PathPosition;
import com.creadory.smove.data.map.util.TrafficLightSignalWrapper;
import com.creadory.smove.data.remote.util.NamedLatLong;
import com.creadory.smove.injection.ApplicationContext;
import com.creadory.smove.injection.ConfigPersistent;
import com.creadory.smove.ui.base.BasePresenter;
import com.creadory.smove.ui.main.util.SpeedRecommendation;
import com.creadory.smove.ui.map.MapFragment;
import com.creadory.smove.ui.map.MapPresenter;
import com.creadory.smove.util.LogUtil;
import com.creadory.smove.util.MathUtil;
import com.creadory.smove.util.RxSearch;
import com.creadory.smove.util.RxUtil;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.InstructionList;
import com.graphhopper.util.PointList;

import org.oscim.core.GeoPoint;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

@ConfigPersistent
public class MainPresenter extends BasePresenter<MainMvpView> {
    private static final String TAG = "MainPresenter";
    private static final int MAX_DISTANCE_TO_PATH_FOR_SWITCH_TO_NEW = 15; //in meter
    private static final int MAX_DISTANCE_TO_PATH_FOR_RECALC = 40; //in meter
    private static final int MIN_ELAPSED_TIME_TILL_NEXT_ALT_CALC = 1*60*1000; //1min in millis

    private MapPresenter mMapPresenter;
    private NamedLatLong[] mSuggestions;
    private NamedLatLong mActiveSearchResult;

    private CustomGHResponse mActivePathResp;
    private Disposable mActiveInstructionDisposable;
    private InstructionWrapper mActiveInstructionWrapper;

    private CustomGHResponse mActiveAlternativePathResp;
    private Disposable mActiveAlternativeInstructionDisposable;

    private Observable<LocationAndSpeed> mUserLocationObservable;
    private Disposable mRedWaitingTimeDisposable;

    private long lastAlternativeRouteCalculation = 0;

    //for testing
    private Context context;
    private LogUtil logUtil;

    @Inject
    public MainPresenter(@ApplicationContext Context context, DataManager dataManager, MapPresenter mapPresenter, LogUtil logUtil) {
        super(dataManager);
        mMapPresenter = mapPresenter;
        mMapPresenter.setMainPresenter(this);

        this.context = context;
        this.logUtil = logUtil;
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);

        goToStart();
        initUserLocation();
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    private void initUserLocation() {
        mUserLocationObservable = getDataManager().getLocationService().observable()
                .observeOn(Schedulers.io())
                .switchMap(new Function<LocationAndSpeed, ObservableSource<LocationAndSpeed>>() {
                    @Override
                    public ObservableSource<LocationAndSpeed> apply(@NonNull final LocationAndSpeed locationAndSpeed) throws Exception {
                        if (mActivePathResp != null) {  //do matching to path if available
                            PathPosition pathPosition = getDataManager().getPositioningHelper()
                                    .calcMatchedPositionBasedOnCurrentPath(mActivePathResp.getBest().getPoints(), locationAndSpeed.location, null);
                            locationAndSpeed.location.setBearing(pathPosition.bearing);
                            locationAndSpeed.location.setLatitude(pathPosition.matchedPosition.lat);
                            locationAndSpeed.location.setLongitude(pathPosition.matchedPosition.lon);

                            if(mActiveAlternativePathResp != null){
                                PathPosition altPathPosition = getDataManager().getPositioningHelper()
                                        .calcMatchedPositionBasedOnCurrentPath(mActiveAlternativePathResp.getBest().getPoints(), locationAndSpeed.location, null);

                                if(pathPosition.distanceToPath > MAX_DISTANCE_TO_PATH_FOR_SWITCH_TO_NEW && altPathPosition.distanceToPath < MAX_DISTANCE_TO_PATH_FOR_SWITCH_TO_NEW){
                                    //user took alternative path -> alternative path is now current path
                                    mActivePathResp = mActiveAlternativePathResp;
                                    mActiveInstructionWrapper = null;
                                    mActiveAlternativePathResp = null;
                                    RxUtil.dispose(mActiveAlternativeInstructionDisposable);
                                    RxUtil.dispose(mActiveInstructionDisposable);
                                    startInstructionUpdates(mActivePathResp.getBest().getInstructions(), false);
                                    locationAndSpeed.setNewRoute(true);
                                }else if(altPathPosition.distanceToPath > MAX_DISTANCE_TO_PATH_FOR_SWITCH_TO_NEW && pathPosition.distanceToPath < MAX_DISTANCE_TO_PATH_FOR_SWITCH_TO_NEW){
                                    //user continued on current path -> remove alternative path
                                    mActiveAlternativePathResp = null;
                                    RxUtil.dispose(mActiveAlternativeInstructionDisposable);
                                    locationAndSpeed.setNewRoute(true);
                                }
                            }

                            if(pathPosition.distanceToPath > MAX_DISTANCE_TO_PATH_FOR_RECALC){
                                reCalcRoute(mActiveSearchResult, locationAndSpeed.location);
                            }

                        }

                        logSpeedData(locationAndSpeed);

                        return new ObservableSource<LocationAndSpeed>() {
                            @Override
                            public void subscribe(Observer<? super LocationAndSpeed> observer) {
                                observer.onNext(locationAndSpeed);
                            }
                        };
                    }
                });

        Disposable disposable = mUserLocationObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<LocationAndSpeed>() {
                    @Override
                    public void accept(@NonNull LocationAndSpeed locationAndSpeed) throws Exception {
                        if(locationAndSpeed.isNewRoute()){
                            getMvpView().hideFasterNavigationInfoInstructions();
                            mMapPresenter.getMvpView().removeAlternativePath();
                            mMapPresenter.getMvpView().showPath(mActivePathResp.getBest());
                            mMapPresenter.getMvpView().removeAllTrafficLightSignalMarkers();
                            mMapPresenter.getMvpView().addTrafficLightSignalMarkers(getDataManager().getTrafficLightService()
                                    .getAllTrafficLightSignalsOnPath(new LatLongPoint(mActivePathResp.getBest().getWaypoints().getLat(0), mActivePathResp.getBest().getWaypoints().getLon(0)),
                                            mActivePathResp.getPaths().get(0).calcNodes(), getDataManager().getRoutingService().getGraphHopper()));
                        }
                        MapFragment basePresenter = (MapFragment) mMapPresenter.getMvpView();
                        if (basePresenter != null) {
                            basePresenter.updateUserPosition(locationAndSpeed.location);
                        }
                        getMvpView().setNavigationInfoUserSpeed((int)locationAndSpeed.speed);
                    }
                });
    }

    private void checkForAlternativeRoute() {
        logAltRouteEvents("check");
        long currentSystemTime = SystemClock.elapsedRealtime();
        if (mActivePathResp != null && mActiveInstructionWrapper != null &&
                ((currentSystemTime - lastAlternativeRouteCalculation) > MIN_ELAPSED_TIME_TILL_NEXT_ALT_CALC)) {     //only if last calculation was > MIN_ELAPSED_TIME_TILL_NEXT_ALT_CALC ago
            logAltRouteEvents("calculate");
            lastAlternativeRouteCalculation = currentSystemTime;
            final InstructionWrapper activeInstructionWrapper = mActiveInstructionWrapper;
            PointList wayPoints = mActivePathResp.getBest().getWaypoints();
            LatLongPoint to = new LatLongPoint(wayPoints.getLat(wayPoints.size() - 1), wayPoints.getLon(wayPoints.size() - 1));
            LatLongPoint from = activeInstructionWrapper.userPosition;

            getDataManager().getRoutingService().calcRoute(from.lat, from.lon, to.lat, to.lon)
                    .observeOn(Schedulers.io())
                    .flatMap(new Function<CustomGHResponse, SingleSource<CustomGHResponse>>() {
                        @Override
                        public SingleSource<CustomGHResponse> apply(@NonNull CustomGHResponse customGHResponse) throws Exception {
                            if(customGHResponse != null){
                                InstructionWrapper instructionWrapper = getDataManager().getInstructionsHelper()
                                        .find(customGHResponse.getBest().getInstructions(), activeInstructionWrapper.userPosition.lat, activeInstructionWrapper.userPosition.lon, 10);

                                double distanceDiff = Math.abs(instructionWrapper.distanceToFinish - activeInstructionWrapper.distanceToFinish);
                                long timeDiff = activeInstructionWrapper.timeToFinish - instructionWrapper.timeToFinish;

                                if(distanceDiff > 0.5) {
                                    logAltRouteEvents("calculated=" + (int)(timeDiff/1000));
                                }

                                if(timeDiff > 5000 && distanceDiff > 0.5){   //at least 10s (10000millis) faster than current route and not same length as current route-> alternative route found
                                    customGHResponse.setFaster(timeDiff);
                                }else{
                                    customGHResponse = null;
                                }
                            }

                            final CustomGHResponse activeAlternativePathResp = customGHResponse;
                            return new SingleSource<CustomGHResponse>() {
                                @Override
                                public void subscribe(SingleObserver<? super CustomGHResponse> observer) {
                                    observer.onSuccess(activeAlternativePathResp);
                                }
                            };
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<CustomGHResponse>() {
                        @Override
                        public void accept(@NonNull CustomGHResponse customGHResponse) throws Exception {
                            if(customGHResponse != null){   //faster route found
                                Log.d(TAG, "NEW alt route found");
                                logAltRouteEvents("NEWfound");
                                mActiveAlternativePathResp = customGHResponse;

                                //show navigation instructions for alternative route
                                PathWrapper best = customGHResponse.getBest();
                                getMvpView().showFasterNavigationInfoInstructions(best.getInstructions().get(1).getSign(), best.getInstructions().get(1).getName(),
                                        best.getInstructions().get(0).getDistance(), customGHResponse.getFaster());
                                mMapPresenter.getMvpView().showAlternativePath(best);
                                RxUtil.dispose(mActiveAlternativeInstructionDisposable);
                                mActiveAlternativeInstructionDisposable = startInstructionUpdates(best.getInstructions(), true);
                            }else{
                                Log.d(TAG, "NO alt route found");
                                logAltRouteEvents("NOfound");
                            }
                        }
                    });
        }
    }

    protected void initSearch(SearchView searchView) {
        RxSearch.fromSearchView(searchView)
                .debounce(300, TimeUnit.MILLISECONDS)   //send request only if at least 300 millis past since last character entered
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String s) throws Exception {
                        return s.length() > 2;  //send request only if at least 3 character entered
                    }
                })
                .observeOn(Schedulers.io())
                .switchMap(new Function<String, ObservableSource<NamedLatLong[]>>() {
                    @Override
                    public ObservableSource<NamedLatLong[]> apply(final @NonNull String s) throws Exception {
                        return getDataManager().getGeocodingService().searchQueryNearByLocation(s,
                                MathUtil.convertLocToLatLongPoint(getDataManager().getLocationService().getLastLocation()));
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<NamedLatLong[]>() {
                    @Override
                    public void accept(@NonNull NamedLatLong[] namedLatLongs) throws Exception {
                        mSuggestions = namedLatLongs;
                        getMvpView().showSuggestions(namedLatLongs);
                    }
                });
    }

    protected void searchResultSelected(int position) {
        NamedLatLong namedLatLong = mSuggestions[position];
        Log.d(TAG, "searchResultSelected: " + namedLatLong.name);
        getMvpView().hideKeyBoard();
        goToSearchResult(namedLatLong);
    }

    private Disposable startInstructionUpdates(final InstructionList instructionList, final boolean alternativeInstructions) {
        return mUserLocationObservable.observeOn(Schedulers.io())
                .switchMap(new Function<LocationAndSpeed, ObservableSource<InstructionWrapper>>() {
                    @Override
                    public ObservableSource<InstructionWrapper> apply(@NonNull LocationAndSpeed locationAndSpeed) throws Exception {
                        //find next instruction, max 10m away from position
                        final InstructionWrapper instructionWrapper = getDataManager().getInstructionsHelper()
                                .find(instructionList, locationAndSpeed.location.getLatitude(), locationAndSpeed.location.getLongitude(), 10);

                        return new ObservableSource<InstructionWrapper>() {
                            @Override
                            public void subscribe(Observer<? super InstructionWrapper> observer) {
                                if (instructionWrapper != null) {
                                    observer.onNext(instructionWrapper);
                                }
                            }
                        };
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<InstructionWrapper>() {
                    @Override
                    public void accept(@NonNull InstructionWrapper instructionWrapper) throws Exception {
                        if(alternativeInstructions){
                            getMvpView().updateFasterNavigationInfoInstructions(instructionWrapper.nextInstruction.getSign(), instructionWrapper.nextInstruction.getName(),
                                    instructionWrapper.distanceToNextInstruction);
                        }else {
                            mActiveInstructionWrapper = instructionWrapper;
                            getMvpView().updateNavigationInfoInstructions(instructionWrapper.nextInstruction.getSign(), instructionWrapper.nextInstruction.getName(),
                                    instructionWrapper.distanceToNextInstruction);
                            Calendar arrivalTime = getDataManager().getNtpTimeService().getCurrentTimeCalendar();
                            arrivalTime.add(Calendar.SECOND, (int) (instructionWrapper.timeToFinish / 1000));
                            getMvpView().setNavigationInfoRouteInfo(instructionWrapper.distanceToFinish, instructionWrapper.timeToFinish, arrivalTime);
                            logInstructions(instructionWrapper);
                        }
                    }
                });
    }

    private void startSpeedRecommendationUpdates() {
        Disposable disposable = mUserLocationObservable.observeOn(Schedulers.io())
                .switchMap(new Function<LocationAndSpeed, ObservableSource<SpeedRecommendation>>() {
                    @Override
                    public ObservableSource<SpeedRecommendation> apply(@NonNull LocationAndSpeed locationAndSpeed) throws Exception {
                        final SpeedRecommendation speedRecommendation = new SpeedRecommendation();

                        if (mActivePathResp != null) {   //if path available
                            TrafficLightSignalWrapper tlsWrapper = getDataManager().getTrafficLightService().getNextTrafficLightSignalOnPath(
                                    MathUtil.convertLocToLatLongPoint(getDataManager().getLocationService().getLastLocation()),
                                    mActivePathResp.getPaths().get(0).calcNodes(),
                                    getDataManager().getRoutingService().getGraphHopper());

                            if (tlsWrapper != null) {     //if trafficLightSignal on path
//                                Log.d(TAG, "distanceToTls: " + tlsWrapper.distanceToTls + "; timeToTls: " + tlsWrapper.timeToTls + "; currentMaxSpeed: " + tlsWrapper.currentMaxSpeed / 3.6);
                                ArrayList<TimeRange> timeRanges = getDataManager().getTrafficLightService().getTrafficLightController()
                                        .getTimeRangesForGreen(tlsWrapper.tls, getDataManager().getNtpTimeService().getCurrentTimeCalendar(),
                                                tlsWrapper.timeToTls, 2);

                                if (!timeRanges.isEmpty()) { //if ranges for next traffic light available
                                    double maxSpeed = -1;
                                    double minSpeed = -1;
                                    double displaySpeed = -1;
                                    TimeRange nextRange = timeRanges.get(0);    //just take first reachable range

                                    double currentMaxSpeedInMpS = tlsWrapper.currentMaxSpeed / 3.6;
                                    if (tlsWrapper.timeToTls >= nextRange.start && tlsWrapper.timeToTls <= nextRange.end) {   //phase=green when driving with max. possible speed
                                        long timeLeft = nextRange.end - Math.round(tlsWrapper.timeToTls);
                                        long timeDrivingWithCurrentMaxSpeed = (long) (tlsWrapper.distanceWithCurrentMaxSpeed / currentMaxSpeedInMpS);
                                        maxSpeed = nextRange.start > 0 ? (tlsWrapper.distanceToTls / nextRange.start) : Double.POSITIVE_INFINITY;
                                        minSpeed = tlsWrapper.distanceWithCurrentMaxSpeed / (timeDrivingWithCurrentMaxSpeed + timeLeft);
                                        displaySpeed = currentMaxSpeedInMpS;
                                    } else if (tlsWrapper.timeToTls < nextRange.start) {   //phase=green when driving with max. possible speed -> slow down to reach phase
                                        maxSpeed = tlsWrapper.distanceToTls / nextRange.start;
                                        minSpeed = tlsWrapper.distanceToTls / nextRange.end;
                                        displaySpeed = maxSpeed > currentMaxSpeedInMpS ? currentMaxSpeedInMpS : maxSpeed;
                                    }

                                    if (maxSpeed >= (currentMaxSpeedInMpS * 0.7) && locationAndSpeed.speed > 0) {    //only show speed recommendation if speed would be at least 80% of max. possible speed (speed limit)
//                                        Log.d(TAG, "GO -> maxSpeed=" + maxSpeed + ", minSpeed=" + minSpeed + ", displaySpeed=" + displaySpeed);
                                        maxSpeed *= 3.6;
                                        minSpeed *= 3.6;
                                        displaySpeed *= 3.6;

                                        //compare with current user speed
                                        //set hint
                                        if (displaySpeed < minSpeed) {
                                            minSpeed = displaySpeed;
                                        } else if (displaySpeed > maxSpeed) {
                                            maxSpeed = displaySpeed;
                                        }
                                        if (locationAndSpeed.speed > maxSpeed) {  //show arrow down
                                            speedRecommendation.setHint(SpeedRecommendation.HINT.DOWN);
                                        } else if (locationAndSpeed.speed < minSpeed) {
                                            speedRecommendation.setHint(SpeedRecommendation.HINT.UP);
                                        } else {
                                            speedRecommendation.setHint(SpeedRecommendation.HINT.OK);
                                        }

                                        //set recommended speed
                                        speedRecommendation.setDisplaySpeed((int) Math.round(displaySpeed));
                                    } else if (tlsWrapper.timeToTls < nextRange.start) {     //to fast or in front of traffic light -> show red light countdown
                                        speedRecommendation.setWaitingTime((int) nextRange.start);
                                    } else {    //to slow or next phase not reachable in speed limit -> show red light countdown
//                                        Log.d(TAG, "STOP: RED -> maxSpeed=" + maxSpeed + ", minSpeed=" + minSpeed + ", displaySpeed=" + displaySpeed);
                                        speedRecommendation.setWaitingTime((int) timeRanges.get(1).start);
                                    }
                                }
                            }
                        }

                        return new ObservableSource<SpeedRecommendation>() {
                            @Override
                            public void subscribe(Observer<? super SpeedRecommendation> observer) {
                                observer.onNext(speedRecommendation);
                            }
                        };
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<SpeedRecommendation>() {
                    @Override
                    public void accept(@NonNull SpeedRecommendation speedRecommendation) throws Exception {
                        RxUtil.dispose(mRedWaitingTimeDisposable);

                        if (speedRecommendation.hasSpeed()) {
                            switch (speedRecommendation.getHint()) {
                                case OK:
                                    getMvpView().showSpeedRecCircleGreen(speedRecommendation.getDisplaySpeed());
                                    break;
                                case DOWN:
                                    getMvpView().showSpeedRecArrowDown(speedRecommendation.getDisplaySpeed());
                                    break;
                                case UP:
                                    getMvpView().showSpeedRecArrowUp(speedRecommendation.getDisplaySpeed());
                                    break;
                            }
                            logSpeedRecommendation("" + speedRecommendation.getDisplaySpeed(), speedRecommendation.getHint(), "-");
                        } else if (speedRecommendation.hasWaitingTime()) {
                            getMvpView().showSpeedRecCircleRed(speedRecommendation.getWaitingTime());
                            logSpeedRecommendation("-", speedRecommendation.getHint(), ""+speedRecommendation.getWaitingTime());
                            showRedWaitingTime(speedRecommendation.getWaitingTime());
                            checkForAlternativeRoute();
                        } else {
                            getMvpView().hideSpeedRec();
                        }
                    }
                });
    }

    private void showRedWaitingTime(final int waitingTime) {
        mRedWaitingTimeDisposable = Single.timer(1, TimeUnit.SECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        int newWaitingTime = waitingTime - 1;
                        if (newWaitingTime <= 0) {
                            getMvpView().showSpeedRecCircleGreen(Integer.MAX_VALUE);
                        } else {
                            getMvpView().showSpeedRecCircleRed(newWaitingTime);
                            showRedWaitingTime(newWaitingTime);
                        }
                    }
                });
    }

    private void goToStart() {
        getDataManager().getStateHelper().setCurrentState(StateHelper.State.START);
        getMvpView().hideFaButtonLocation();
        mMapPresenter.getMvpView().setMapPosition(getDataManager().getLocationService().getLastLocation(), false);
        mMapPresenter.getMvpView().followUser(true);
    }

    private void goToSearch() {  //for now only reachable from SearchResult (on back)
        getDataManager().getStateHelper().setCurrentState(StateHelper.State.SEARCH);
        mActiveSearchResult = null;

        //show main toolbar (including search input) instead of SearchResult toolbar and hide search result bottom info
        getMvpView().showMainToolbar();
        getMvpView().hideSearchResultInfo();

        //show user position
        getMvpView().hideFaButtonLocation();
        mMapPresenter.getMvpView().hideSearchResultMarker();
        mMapPresenter.getMvpView().setMapPosition(getDataManager().getLocationService().getLastLocation(), false);
        mMapPresenter.getMvpView().followUser(true);
    }

    private void goToSearchResult(NamedLatLong searchResult) {
        getDataManager().getStateHelper().setCurrentState(StateHelper.State.SEARCH_RESULT);
        mActiveSearchResult = searchResult;

        //show search result toolbar
        getMvpView().showSearchResultToolbar(searchResult.name);

        //hide RouteResult (on back)
        getMvpView().hideRouteResultInfo();
        mMapPresenter.getMvpView().removePath();

        //show search result bottom info and calc button
        getMvpView().showSearchResultInfo(searchResult.name, searchResult.vicinity);

        //set marker and center map to it
        mMapPresenter.getMvpView().setSearchResultMarker(searchResult.latLong);
        mMapPresenter.getMvpView().followUser(false);
        getMvpView().hideFaButtonLocation();

        //show all traffic light signals
        mMapPresenter.getMvpView().removeAllTrafficLightSignalMarkers();
        if (getDataManager().getTrafficLightService().isReady()) {
            mMapPresenter.getMvpView().addTrafficLightSignalMarkers(getDataManager().getTrafficLightService().getTrafficLightController().getAllTrafficLightSignals());
        }
    }

    private void goToRouteResult(NamedLatLong searchResult) {
        getDataManager().getStateHelper().setCurrentState(StateHelper.State.ROUTER_RESULT);
        Location lastLocation = getDataManager().getLocationService().getLastLocation();

        //start route calculation and show route infos on finish
        getDataManager().getRoutingService().calcRoute(lastLocation.getLatitude(), lastLocation.getLongitude(), searchResult.latLong.lat, searchResult.latLong.lon)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CustomGHResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(CustomGHResponse resp) {
                        mActivePathResp = resp;
                        PathWrapper bestPath = resp.getBest();

                        //show route result info in bottom bar and fab to start navigation
                        getMvpView().showRouteResultInfoContent(bestPath.getDistance(), bestPath.getTime());

                        //set path in map view and show only traffic lights on it
                        mMapPresenter.getMvpView().showPath(bestPath);
                        mMapPresenter.getMvpView().addTrafficLightSignalMarkers(getDataManager().getTrafficLightService()
                                .getAllTrafficLightSignalsOnPath(new LatLongPoint(bestPath.getWaypoints().getLat(0), bestPath.getWaypoints().getLon(0)),
                                        resp.getPaths().get(0).calcNodes(), getDataManager().getRoutingService().getGraphHopper()));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("TAG", "calcRoute onError: " + e.getMessage());
                    }
                });

        //top bar is the same like in SearchResult, just change bottom bar
        getMvpView().hideSearchResultInfo();
        getMvpView().showRouteResultInfoLoading();

        //show overview without traffic lights
        mMapPresenter.getMvpView().removeAllTrafficLightSignalMarkers();
        mMapPresenter.getMvpView().showOverview(lastLocation, searchResult.latLong, true);
    }

    private void goToNavigation(CustomGHResponse resp) {
        if (resp != null) {
            getDataManager().getStateHelper().setCurrentState(StateHelper.State.NAVIGATION);
            PathWrapper bestPath = resp.getBest();

            //hide route result view and app toolbar
            getMvpView().hideRouteResultInfo();
            getMvpView().hideAppToolbar();

            //set bottom infos
            getMvpView().setNavigationInfoUserSpeed((int) getDataManager().getLocationService().getLastSpeed());
            Calendar currentTime = getDataManager().getNtpTimeService().getCurrentTimeCalendar();
            currentTime.add(Calendar.SECOND, (int) (bestPath.getTime() / 1000));
            getMvpView().setNavigationInfoRouteInfo(bestPath.getDistance(), bestPath.getTime(), currentTime);

            //show navigation container
            getMvpView().showNavigationInfoContainer();

            //switch to navigation indicator and animate to user position
            mMapPresenter.getMvpView().switchToNavigationView(getDataManager().getLocationService().getLastLocation(), true);

            //show instructions
//            mMapPresenter.getMvpView().showInstructions(bestPath.getInstructions());     //need other color
            getMvpView().showNavigationInfoInstructions(bestPath.getInstructions().get(1).getSign(), bestPath.getInstructions().get(1).getName(),
                    bestPath.getInstructions().get(0).getDistance());
            mActiveInstructionDisposable = startInstructionUpdates(bestPath.getInstructions(), false);

            //show speed recommendations
            startSpeedRecommendationUpdates();
        }
    }

    private volatile boolean runningReCalcPath = false;
    private void reCalcRoute(NamedLatLong searchResult, Location lastLocation){
        if(!runningReCalcPath) {
            RxUtil.dispose(mActiveInstructionDisposable);

            runningReCalcPath = true;
            getDataManager().getRoutingService().calcRoute(lastLocation.getLatitude(), lastLocation.getLongitude(), searchResult.latLong.lat, searchResult.latLong.lon)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<CustomGHResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onSuccess(CustomGHResponse resp) {
                            mActivePathResp = resp;
                            PathWrapper bestPath = resp.getBest();

                            //set path in map view and show only traffic lights on it
                            mMapPresenter.getMvpView().showPath(bestPath);
                            mMapPresenter.getMvpView().removeAllTrafficLightSignalMarkers();
                            mMapPresenter.getMvpView().addTrafficLightSignalMarkers(getDataManager().getTrafficLightService()
                                    .getAllTrafficLightSignalsOnPath(new LatLongPoint(bestPath.getWaypoints().getLat(0), bestPath.getWaypoints().getLon(0)),
                                            resp.getPaths().get(0).calcNodes(), getDataManager().getRoutingService().getGraphHopper()));

                            goToNavigation(resp);
                            runningReCalcPath = false;
                        }

                        @Override
                        public void onError(Throwable e) {
                            runningReCalcPath = false;
                            Log.d("TAG", "calcRoute onError: " + e.getMessage());
                        }
                    });
        }
    }

    private void showFabLocation() {
        if (getDataManager().getStateHelper().getCurrentState() == StateHelper.State.START ||
                getDataManager().getStateHelper().getCurrentState() == StateHelper.State.SEARCH ||
                getDataManager().getStateHelper().getCurrentState() == StateHelper.State.NAVIGATION) {
            getMvpView().showFaButtonLocation();
            mMapPresenter.getMvpView().followUser(false);
        }
    }

    /*****
     * MVP Presenter methods implementation, called from View (MainActivity)
     *****/

    public void onFaButtonLocationClicked() {
        if (getDataManager().getStateHelper().getCurrentState() == StateHelper.State.START ||
                getDataManager().getStateHelper().getCurrentState() == StateHelper.State.SEARCH) {
            getMvpView().hideFaButtonLocation();
            mMapPresenter.getMvpView().setMapPosition(getDataManager().getLocationService().getLastLocation(), true);
            mMapPresenter.getMvpView().followUser(true);
        } else if (getDataManager().getStateHelper().getCurrentState() == StateHelper.State.NAVIGATION) {
            getMvpView().hideFaButtonLocation();
            mMapPresenter.getMvpView().switchToNavigationView(getDataManager().getLocationService().getLastLocation(), true);
            mMapPresenter.getMvpView().followUser(true);
        }
    }

    public void onSearchResultToolbarBackPressed() {
        if (getDataManager().getStateHelper().getCurrentState() == StateHelper.State.SEARCH_RESULT) {
            goToSearch();
        } else if (getDataManager().getStateHelper().getCurrentState() == StateHelper.State.ROUTER_RESULT) {
            goToSearchResult(mActiveSearchResult);
        }
    }

    //return true if it should collapse
    public boolean onSearchActionCollapse() {
        if (getDataManager().getStateHelper().getCurrentState() == StateHelper.State.SEARCH_RESULT) {
            goToSearch();
            return false;
        } else if (getDataManager().getStateHelper().getCurrentState() == StateHelper.State.ROUTER_RESULT) {
            goToSearchResult(mActiveSearchResult);
            return false;
        } else if (getDataManager().getStateHelper().getCurrentState() == StateHelper.State.NAVIGATION) {
            return true;
        }
        goToStart();
        return true;
    }

    public void onFaButtonSearchResultClicked() {    //start calc route
        if (mActiveSearchResult != null) {
            goToRouteResult(mActiveSearchResult);
        }
    }

    public void onFaButtonRouteResultClicked() {     //start navigation
        if (mActivePathResp != null) {
            goToNavigation(mActivePathResp);
        }
    }

    public boolean onBackPressed() {
        if(getDataManager().getStateHelper().getCurrentState() == StateHelper.State.NAVIGATION){
            return true;
        }else {
            return false; //return false if not handled
        }
    }

    public void onOne() {
        getDataManager().getTrafficFlowService().startTrafficFlowUpdates();
    }

    public void onTwo() {
        NamedLatLong namedLatLong = new NamedLatLong(new LatLongPoint(52.479349, 13.482026), "Testziel", "Testziel Umgebung");
        goToSearchResult(namedLatLong);
    }

    public void onThree() {
        lastAlternativeRouteCalculation = 0;    //force alt route calculation
        checkForAlternativeRoute();
    }

    public void onFour() {
        writeAllDataToFile();
        logUtil.logUser(TAG, "WRITE DATA DONE");
    }

    /*****
     * Presenter communication, called from MapPresenter
     *****/

    public void onMapLongPress(GeoPoint p) {
        NamedLatLong namedLatLong = new NamedLatLong(new LatLongPoint(p.getLatitude(), p.getLongitude()), "Long Press Ziel", "Long Press Umgebung");
        goToSearchResult(namedLatLong);
    }

    public void onMapDoubleTap(GeoPoint p) {
        showFabLocation();
    }

    public void onMapActionMove() {
        showFabLocation();
    }

    /*****
     * Logging for test drives
     *****/
    private static SimpleDateFormat testDateFormat = new SimpleDateFormat("HH-mm-ss", Locale.GERMANY);
    private int speedCnt = 0;
    private String locationAndSpeedString = "time\tcnt\tspeed\tlat\tlon\n";
    private String altRouteEventsString = "time\tevent\n";
    private String instructionWrapperString = "time\tdistanceToFinish\ttimeToFinish\n";
    private String speedRecommendationString = "time\tspeed\thint\tredWaitingTime\n";

    private void logSpeedData(LocationAndSpeed locationAndSpeed) {
        if (locationAndSpeed.speed > 5) {
            speedCnt++;
            locationAndSpeedString += testDateFormat.format(getDataManager().getNtpTimeService().getCurrentTimeDate()) + "\t" + speedCnt + "\t" +
                    (int)locationAndSpeed.speed + "\t" + locationAndSpeed.location.getLatitude() + "\t" + locationAndSpeed.location.getLongitude() + "\n";
        }
    }

    private void logAltRouteEvents(String event){
        altRouteEventsString += testDateFormat.format(getDataManager().getNtpTimeService().getCurrentTimeDate()) + "\t" + event + "\n";
    }

    private void logInstructions(InstructionWrapper instructionWrapper){
        instructionWrapperString += testDateFormat.format(getDataManager().getNtpTimeService().getCurrentTimeDate()) + "\t" + instructionWrapper.distanceToFinish + "\t" + (int)(instructionWrapper.timeToFinish/1000) + "\n";
    }

    private void logSpeedRecommendation(String speed, SpeedRecommendation.HINT hint, String waitingTime){
        speedRecommendationString += testDateFormat.format(getDataManager().getNtpTimeService().getCurrentTimeDate()) + "\t" + speed + "\t" + hint.name() + "\t" + waitingTime + "\n";
    }

    private void writeAllDataToFile(){
        speedCnt = 0;
        writeToFile(locationAndSpeedString, "speed");
        writeToFile(altRouteEventsString, "alt");
        writeToFile(instructionWrapperString, "instruction");
        writeToFile(speedRecommendationString, "recomm");
    }

    //writes a string to the file fileName_"time".txt and saves file on Android's external storage
    private void writeToFile(String data, String fileName) {
        File path = context.getExternalFilesDir(null);
        String currentTime = testDateFormat.format(getDataManager().getNtpTimeService().getCurrentTimeDate());
        new File(path + "/" + currentTime).mkdir();
        File file = new File(path + "/" + currentTime, fileName + "_" + currentTime + ".txt");
        Log.d("FILEWRITE", "path: " + path);
        try {
            FileOutputStream stream = new FileOutputStream(file);
            try {
                stream.write(data.getBytes());
            } finally {
                stream.close();
            }
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
