package com.creadory.smove;

/**
 * Created by Tim on 27.01.2017.
 */
public class AppConstants {
    public static final class Algorithms {
        /**
         * Algorithms considering Traffic Lights
         */
        public static final String DIJKSTRA_TRAFFIC_LIGHT = "dijkstratrafficlight";
        public static final String ASTAR_TRAFFIC_LIGHT = "astartrafficlight";
    }

    public static final class AREA{
        public static final String ALL = "";
        public static final String TREPTOW_KOEPENICK = "Treptow";
    }

    public static final class HERE{
        public static final String APP_ID = "qybvtsSSlGXEO608FPSn";
        public static final String APP_CODE = "HG-Ymmwi6RLemVG4pVIumw";
        private static final String AREA_TOP_LEFT = "52.604181,13.190117";
        private static final String AREA_BOTTOM_RIGHT = "52.390150,13.723640";
        public static final String TRAFFIC_FLOW_URL =
                "https://traffic.cit.api.here.com/traffic/6.2/flow.json?" +
                "app_id=bUEQq7jYaJnLFoPGg470&app_code=rh8MfcQiargcDHem8SC7vQ" +
                "&responseattributes=sh,fc" +
                "&bbox=" + AREA_TOP_LEFT + ";" + AREA_BOTTOM_RIGHT;
    }

    public static final class MAP_DATA{
        public static final String DEFAULT_AREA = "berlin_v2_car2_811";
        public static final String DIRECTORY = "/graphhopper/maps/";
    }

    public static final class TIME{
        public final static String[] NTP_SERVERS = {"0.de.pool.ntp.org", "1.de.pool.ntp.org", "2.de.pool.ntp.org", "3.de.pool.ntp.org"};
        public static final int MINUTES_TO_UPDATE_NTP_TIME = 5;
    }

    public static final class TRAFFIC_FLOW{
        public final static String FLOW_SERVER_TEST = "http://192.168.0.55:8989";
        public final static String FLOW_SERVER = "http://smove.herokuapp.com";
    }
}
