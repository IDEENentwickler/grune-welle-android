package com.creadory.smove;

import android.app.Application;
import android.content.Context;

import com.creadory.smove.injection.component.ApplicationComponent;
import com.creadory.smove.injection.component.DaggerApplicationComponent;
import com.creadory.smove.injection.module.ApplicationModule;

public class AndroidApplication extends Application {

    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public static AndroidApplication get(Context context) {
        return (AndroidApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }
}
