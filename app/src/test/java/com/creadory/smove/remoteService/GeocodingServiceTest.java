package com.creadory.smove.remoteService;

import com.creadory.smove.core.util.LatLongPoint;
import com.creadory.smove.data.remote.GeocodingService;
import com.creadory.smove.data.remote.util.NamedLatLong;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class GeocodingServiceTest {
    @Test
    public void testSearch() throws Exception {
        GeocodingService suggestions = new GeocodingService();
        NamedLatLong[] results = suggestions.searchNearBy(new LatLongPoint(52.5023, 13.427), "kollw");
        assertEquals(GeocodingService.LIMIT, results.length);

        NamedLatLong kollwitzplatz = results[1];
        assertEquals("Kollwitzplatz", kollwitzplatz.name);
        assertEquals(new LatLongPoint(52.53628, 13.41803), kollwitzplatz.latLong);
    }
}
